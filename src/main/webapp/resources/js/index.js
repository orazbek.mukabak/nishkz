
const includeContent = (path, viewPort) => {
    path = path.split(' ').join('%20');
    viewPort = "#" + viewPort;
    $(viewPort).load(path);
};

window.onload = function() {
    //$("#divFooterContentHeaderJsp").load("display-footer");
    $("#banner_index_jsp").load("display-banner");
    includeContent('course/display-all-courses', 'all_courses_jsp');
    
    $('.navbar-nav>li>a').on('click', function(){
        $('.navbar-collapse').collapse('hide');
    });

    $(document).ready(function () {
        $(document).click(function (event) {
            var click = $(event.target);
            var _open = $(".navbar-collapse").hasClass("show");
            if (_open === true && !click.hasClass("navbar-toggler")) {
                $(".navbar-toggler").click();
            }
        });
    });
};

function divideContent() {
    var varDivideContentHtml = "<div class='row content'>";
    varDivideContentHtml += "<div class='col-sm-3 col-lg-2' id='left_content'></div>";
    varDivideContentHtml += "<div class='col-sm-9 col-lg-10' id='right_content'></div></div>";
    document.getElementById('container_fluid').innerHTML = varDivideContentHtml;
}
