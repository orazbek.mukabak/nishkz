
function insertLessonContent(path, tabId) {
	modifyTabButtons(tabId);
	includeContent(path, 'lesson-content-view-course-jsp');
}

function modifyTabButtons (tabId) {
	document.querySelector('.icon-bar .active').className = '';
	document.getElementById(tabId).classList.add('active');
}


