var CropImage = (function() {
    function uploadFile(result, innerId, username) {
        var file = result.src;
        var filename = document.getElementById("innerId").value + ".jpg";
        var storageRef = firebase.storage().ref('/images/profile/' + filename);
        var uploadTask = storageRef.put(file);
        var subscribe = uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED);
        subscribe({
            'next': function(snapshot) {
            },
            'error': function(error) {
            },
            'complete': function() {
                uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                    $("#btnSavePictureEditPictureJsp").hide();
                    window.location.replace("my-profile");
                });
            }
        });
    }

    function demoUpload(innerId, username) {
        var $uploadCrop;
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.upload-demo').addClass('ready');
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function() {
                        console.log('jQuery bind complete');
                    });
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 230,
                height: 230
                //type: 'circle'
            },
            boundary: {
                width: 230,
                height: 230
            }
        });
        
        $('#inputPictureEditPictureJsp').on('change', function () {
            readFile(this);
            //$("#btnViewPictureEditPictureJsp").hide();
            $("#btnSavePictureEditPictureJsp").show();
        });
        
        $('#btnSavePictureEditPictureJsp').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'blob',
                size: 'viewport',
                format: 'jpeg',
		quality: 1
            }).then(function (resp) {
                uploadFile({
                    src: resp
                }, innerId, username);
            });
        });
    }

    function bindNavigation () {
        var $body = $('body');
        $('nav a').on('click', function (ev) {
            var lnk = $(ev.currentTarget),
                href = lnk.attr('href'),
                targetTop = $('a[name=' + href.substring(1) + ']').offset().top;
            $body.animate({ scrollTop: targetTop });
            ev.preventDefault();
        });
    }

    function init(innerId, username) {
        bindNavigation();
        demoUpload(innerId, username);
    }

    return {
        init: init
    };
    })();

    (function () {
      var method;
      var noop = function () { };
      var methods = [
          'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
          'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
          'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
          'timeStamp', 'trace', 'warn'
      ];
      var length = methods.length;
      var console = (window.console = window.console || {});

      while (length--) {
        method = methods[length];
        if (!console[method]) {
            console[method] = noop;
        }
      }

      if (Function.prototype.bind) {
        window.log = Function.prototype.bind.call(console.log, console);
      }
      else {
        window.log = function() { 
          Function.prototype.apply.call(console.log, console, arguments);
        };
      }
})();