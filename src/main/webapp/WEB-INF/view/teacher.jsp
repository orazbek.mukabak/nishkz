<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Teacher</title>
    <link rel="stylesheet" href="resources/css/teacher.css" />
</head>
<body>

  <div class='outer-wrapper-teacher'>
      <div class='inner-wrapper-teacher'>
        <div class='title-teacher' id='title_teacher' data-aos='fade-right'>
          Бұл курсты кім жіргізеді
        </div>
      </div>
  </div>

  <div class='outer-wrapper-teacher'>
      <div class='inner-wrapper-teacher'>
        <div class='image-part-teacher' data-aos='fade-up'>
          <img src='resources/images/website/teacher.jpg'>
        </div>
        <div class='text-part-teacher' id='text_part_teacher'>
          <h1 data-aos='fade-right'>Бақты Қуатұлы</h1>
          <div data-aos='fade-up'>
            <div class='paragraph-text-part-teacher' id='main_text_part_teacher'></div>

            <div class='paragraph-text-part-teacher' id='features_text_part_teacher'>
              <div></div>
              <div></div>
              <div></div>
            </div>

            <div class='paragraph-text-part-teacher' id='aboutme_text_part_teacher'>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>

            <div class='paragraph-text-part-teacher' id='end_text_part_teacher'>
            </div>
          </div>
        </div>
      </div>
    <div class='background-outer-wrapper-teacher'>
    </div>
  </div>

  <script type="text/javascript">
    const lanKazTeacher = {
        "sectionTitleAuthor": "Бұл курсты кім жіргізеді",
        "teacherName": "Тлеуғали Бақты",
        
        "teacherInfoMain": "Сәлеметсіздер ме! Менің атым Тлеуғали Бақты, мен 6 жылдық тәжірибесі бар, мамандандырылған мектеп-лицейлері мен жоғары оқу орындарына түсуге дайындайтын кәсіби тренермін.",
        
        "teacherInfoFeature1": "+ Менің оқушыларым еліміздің ең мықты мектептеріне экзаменнен жоғары бал алып түскен: НЗМ, РФММ, БИЛ.",
        "teacherInfoFeature2": "+ Оқушылар ҰБТ-дан ең жоғары ұпай жинап грантқа ие болды. Әлемнің және Қазақстанның үздік университеттеріне түсті.",
        "teacherInfoFeature3": "+ Әлемнің түпкір-түпкірінен келіп немесе онлайн білім алған оқушыларым бар: Қазақстаннан, Германиядан, АҚШ-тан, Франциядан және т. б.",

        "teacherInfoAboutMeMain": "Ұстаз жайлы аз-кем ақпарат:",
        "teacherInfoAboutMe1": "+ Алматыдағы РФМШ мектебін Алтын белгімен бітірген",
        "teacherInfoAboutMe2": "+ Назарбаев Университетінің Құрылыс Инженерия бакалавриятын ең үздік үштікте бола отырып тәмамдаған",
        "teacherInfoAboutMe3": "+ 3 тілде еркін сөйлеп, білім бере алатын заманауи тренер",

        "teacherInfoEnd": "Біздің курстарды таңдағаныңызға қуаныштымын. Дұрыс дайындық платформасын таңдау – қалаған оқуға сәтті және уақтылы өтудің кепілдігін береді."
    }

    const lanRuTeacher = {
        "sectionTitleAuthor": "Кто ведет этот курс",
        "teacherName": "Тлеугали Бакты",
        
        "teacherInfoMain": "Здравствуйте! Меня зовут Тлеугали Бакты, и я профессиональный тренер по поступлению в специализированные школы и топовые университеты с 6-летним стажем.",
        
        "teacherInfoFeature1": "+ Мои ученики успешно поступают в лучшие школы страны: НИШ, РФМШ, КТЛ.",
        "teacherInfoFeature2": "+ Абитуренты набирают высшие баллы на ЕНТ в своих школах и поступают на гранты, а также поступают в топовые вузы мира и Казахстана",
        "teacherInfoFeature3": "+ Ученики родом из Казахстана, Германии, США, Франции и других стран по всему миру",

        "teacherInfoAboutMeMain": "Немного фактов обо мне:",
        "teacherInfoAboutMe1": "+ Закончил школу РФМШ г.Алматы с Алтын Белги",
        "teacherInfoAboutMe2": "+ Окончил бакалавриат Гражданской и Строительной инженерии Назарбаев Университета, будучи топ-3 студентов курса",
        "teacherInfoAboutMe3": "+ Свободно говорю на 3 языках с возможностью преподавать на каждом из них",

        "teacherInfoEnd": "Я рад, что вы выбираете наши курсы. Правильный выбор платформы - залог успешного и своевременного поступления в выбранную вами школу"
    }

    let currentLangTeacher = lanRuTeacher;

    if (getLanguage() === 'kaz') {
      currentLangTeacher = lanKazTeacher;
    }
    
    document.querySelectorAll("#title_teacher")[0].innerHTML = currentLangTeacher.sectionTitleAuthor;
    document.querySelectorAll("#text_part_teacher > h1")[0].innerHTML = currentLangTeacher.teacherName;

    
    document.querySelectorAll("#main_text_part_teacher")[0].innerHTML = currentLangTeacher.teacherInfoMain;
    
    document.querySelectorAll("#features_text_part_teacher > div")[0].innerHTML = currentLangTeacher.teacherInfoFeature1;
    document.querySelectorAll("#features_text_part_teacher > div")[1].innerHTML = currentLangTeacher.teacherInfoFeature2;
    document.querySelectorAll("#features_text_part_teacher > div")[2].innerHTML = currentLangTeacher.teacherInfoFeature3;

    document.querySelectorAll("#aboutme_text_part_teacher > div")[0].innerHTML = currentLangTeacher.teacherInfoAboutMeMain;
    document.querySelectorAll("#aboutme_text_part_teacher > div")[1].innerHTML = currentLangTeacher.teacherInfoAboutMe1;
    document.querySelectorAll("#aboutme_text_part_teacher > div")[2].innerHTML = currentLangTeacher.teacherInfoAboutMe2;
    document.querySelectorAll("#aboutme_text_part_teacher > div")[3].innerHTML = currentLangTeacher.teacherInfoAboutMe3;

    document.querySelectorAll("#end_text_part_teacher")[0].innerHTML = currentLangTeacher.teacherInfoEnd;

  </script>

</body>
</html>