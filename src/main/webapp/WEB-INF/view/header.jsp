<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nish.KZ</title>
    <link id="dynamic-favicon" rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/website/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/header.css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-175940099-1">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-175940099-1');
    </script>
</head>
<body>
<%@ include file="contactus.jsp" %>
<!-- <div class="alert alert-success alert-dismissible fade show custom-alert" role="alert" data-aos="flip-up">
    <div>
      <strong id='discount_value_header'>50% жеңілдік!</strong>
      <span id='discount_text_header'>15-Қыркүйекке дейін жеңілдікпен жазылып үлгеріңіз!</span>
    </div>

    <div>
        <strong>+7 777 111 85 41</strong>.
        <div class='buy-course-header' id='buy_course_header' onclick='showSmallContactForm()'>
        Курсқа жазылу
        </div>
    </div>
      <button type="button" class="close close-custom" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
</div> -->
<nav id="top-bar" class="navbar navbar-expand-lg navbar-light">
    
    <div class='header-left-part'>
        <div class="main-logo">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">
            </a>
        </div>

        <div class="phone-and-flags">
            <div class="phone-header">
                Tel: +7 777 111 85 41
            </div>

            <div class='wrapper-language-header'>
                <div class='language-flag-header' onclick="setLanguage('ru')">
                    <img src="${pageContext.request.contextPath}/resources/images/website/russia.svg">
                </div>
                <div class='language-flag-header' onclick="setLanguage('kaz')">
                    <img src="${pageContext.request.contextPath}/resources/images/website/kazakhstan.svg">
                </div>
            </div>
        </div>
    </div>

    <div class='header-right-part'>
        <sec:authorize access="!isAuthenticated()">
            <a class="signin-btn-header" id='signin_btn_header' href="${pageContext.request.contextPath}/signin?u=null&p=null">Кіру</a>
            <a class="signin-btn-header" id='signup_btn_header' href="${pageContext.request.contextPath}/register/signup">Тіркелу</a>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse navbar-custom" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" id="courses_menu_header" href="${pageContext.request.contextPath}/course/view-all">Курстар</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="my-courses_menu_header" href="${pageContext.request.contextPath}/course/view-my-courses">Менің курстарым</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id='my_profile_menu_header' href="${pageContext.request.contextPath}/profile/my-profile">Менің профилім</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  id='logout_menu_header' href="${pageContext.request.contextPath}/logout">Шығу</a>
                </li>
                <li class="nav-item">
                    <i class="nav-link" id="balance"></i>
                </li>
            </ul>
        </div>
        </sec:authorize>
    </div>
</nav>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

<script>

    const includeContent = (path, viewPort) => {
        path = path.split(' ').join('%20');
        viewPort = "#" + viewPort;
        $(viewPort).load(path);
    };

    function showSmallContactForm () {
        const contactUsOuter = document.getElementById('contact_us_outer');
        contactUsOuter.style.visibility = "visible";
        contactUsOuter.style.opacity = 1;  
    }

    function setLanguage(language) {
        localStorage.setItem("nishkz_lan", language);
        location.reload();
    }

    function getLanguage() {
        return localStorage.getItem("nishkz_lan");
    }


    const lanKazHeader = {
        "login": "Кіру",
        "register": "Тіркелу",
        "courses": "Курстар",
        "myProfile": "Менің профилім",
        "logout": "Шығу",
        "discountVlaue": "50% жеңілдік!",
        "discountText": "15-Қыркүйекке дейін жеңілдікпен жазылып үлгеріңіз! ",
        "buyCourse": "Курсқа жазылу"
    }

    const lanRuHeader = {
        "login": "Войти",
        "register": "Регистрация",
        "courses": "Курсы",
        "myProfile": "Мой профиль",
        "logout": "Выход",
        "discountVlaue": "Скидка 50%!",
        "discountText": "Получите подписку со скидкой до 15 сентября! ",
        "buyCourse": "Записаться на курс"
    }

    let currentLangHeader = lanRuHeader;

    if (getLanguage() === 'kaz') {
      currentLangHeader = lanKazHeader;
    }

    const login = document.querySelectorAll("#signin_btn_header")[0];
    if (login) {
        login.innerHTML = currentLangHeader.login;
    }

    const register = document.querySelectorAll("#signup_btn_header")[0];
    if (register) {
        register.innerHTML = currentLangHeader.register;
    }

    const courses = document.querySelectorAll("#courses_menu_header")[0];
    if (courses) {
        courses.innerHTML = currentLangHeader.courses;
    }

    const myProfile = document.querySelectorAll("#my_profile_menu_header")[0];
    if (myProfile) {
        myProfile.innerHTML = currentLangHeader.myProfile;
    }

    const logout = document.querySelectorAll("#logout_menu_header")[0];
    if (logout) {
        logout.innerHTML = currentLangHeader.logout;
    }

     //document.querySelectorAll("#discount_value_header")[0].innerHTML = currentLangHeader.discountVlaue;
     //document.querySelectorAll("#discount_text_header")[0].innerHTML = currentLangHeader.discountText;
     //document.querySelectorAll("#buy_course_header")[0].innerHTML = currentLangHeader.buyCourse;
     

</script>
</body>
</html>