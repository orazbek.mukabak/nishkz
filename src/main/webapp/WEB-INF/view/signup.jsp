<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тіркелу беті...</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/signup.css" />
</head>
<body>
    <div class="register-page-signup-jsp">
        <div class="register-wrap-signup-jsp">
            <div class="register-title-signup-jsp" id='register_title_signup'>
                NISH.KZ-ке Тіркеліп, Сапалы Білім Алуды Баста!
            </div>
            <div class="register-content-signup-jsp">
                <form:form action="${pageContext.request.contextPath}/register/signing" modelAttribute="authUser" onsubmit="return checkPasswordSignupJsp(this);">
                    <div>
                        <c:if test="${registrationError != null}">
                            <script>
                                var error = '${registrationError}';
                                if(error === 'ERROR_USER_ALREADY_EXISTS') {
                                    error = 'Извините, ваш номер телефона уже зарегистрирован';
                                } else if(error === 'ERROR_EMPTY_USERNAME') {
                                    error = 'Запишите свой номер телефона';
                                } else {
                                    error = 'Пароль должен быть длиной от 6 до 32';
                                }
                                Swal.fire({
                                    title: 'Пожалуйста, попробуйте еще раз ...',
                                    text: error,
                                    confirmButtonColor: '#4B2D73'
                                });
                            </script>
                        </c:if>                                         
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-user"></i>
                        <form:input id="register_phone_signup_jsp" path="userName" placeholder="Телефон" class="form-control" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password_signup_jsp" path="password" placeholder="Пароль (минимум 6 мест)" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password2_signup_jsp" path="" placeholder="Повторите пароль" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>

                    <button type="submit" class="btn register-btn-signup-jsp" id='register_btn_signup'>Тіркелу</button>
                    <div class="register-login-wrapper-signup-jsp">
                        <span id='already_registered'>Сіз тіркеліп қойғансыз ба?</span>
                        <a id='login_signup' href="${pageContext.request.contextPath}/signin?u=null&p=null">Кіру</a>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/lib/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">

        const lanKazSignUp = {
            "registerNishAccount": "Nish.kz-ке Тіркеліп, НИШ-қа дайындалуды Баста!",
            "register": "Тіркелу",
            "already_registered": "Сіз тіркеліп қойғансыз ба?",
            "login_signup": "Кіру",
            "passwordRequirement": "Пароль тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек",
        }

        const lanRuSignUp = {
            "registerNishAccount": "Зарегистрируйтесь на Nish.kz и начните подготовку в НИШ!",
            "register": "Зарегистрироваться",
            "already_registered": "Уже зарегистрированы?",
            "login_signup": "Вход",
            "passwordRequirement": "Пароль должен содержать только латинские буквы и цифры. Длина пароля должна быть от 6 до 32",
        }

        let currentLangSignUp = lanRuSignUp;

        if (getLanguage() === 'kaz') {
          currentLangSignUp = lanKazSignUp;
        }

        document.querySelectorAll("#register_title_signup")[0].innerHTML = currentLangSignUp.registerNishAccount;
        document.querySelectorAll("#register_btn_signup")[0].innerHTML = currentLangSignUp.register;
        document.querySelectorAll("#already_registered")[0].innerHTML = currentLangSignUp.already_registered;
        document.querySelectorAll("#login_signup")[0].innerHTML = currentLangSignUp.login_signup;

        document.querySelectorAll("#password_signup_jsp")[0].title = currentLangSignUp.passwordRequirement;
        document.querySelectorAll("#password2_signup_jsp")[0].title = currentLangSignUp.passwordRequirement;
        


        $(function(){
          $("#register_phone_signup_jsp").mask("8 (999) 999-99-99");
        });

        function checkPasswordSignupJsp(form) {
          if(form.password_signup_jsp.value !== form.password2_signup_jsp.value) {
                Swal.fire({
                    title: 'Попробуйте еще раз ...',
                    text: 'Пароль неверный',
                    confirmButtonColor: '#4B2D73'
                });
              form.password_signup_jsp.focus();
              return false;
            }
          return true;
        }

    </script>
</body>
</html>