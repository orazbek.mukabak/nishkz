<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Барлық курстарды көру</title>
    </head>
    <body>
         <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
            <div>
                <a class="nav-link dropdown-toggle"  style="float: right;" href="" role="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="javascript: viewTestResultsViewLessonJsp(${lesson.id});"><i class="fas fa-list-ol"></i> Тест нәтижелерін қарау</a>
                    <a class="dropdown-item" href="javascript: editLessonViewLessonJsp(${lesson.id});"><i class="fas fa-pen"></i> Сабақты өңдеу</a>
                    <a class="dropdown-item" href="javascript: deleteLessonViewLessonJsp(${lesson.id});"><i class="fas fa-trash-alt"></i> Сабақты жою</a>
                </div>
            </div>
        </sec:authorize>
        <div id="lesson-content-view-course-jsp">
        </div>
        <div class="icon-bar">
            <a id="tab-video" class="active" href="javascript: insertLessonContent('course/display-view-lesson-video/${lesson.id}', 'tab-video');"><i class="fas fa-video"></i><br><i class="menu-text-view-course">Видео</i></a>
            <a id="tab-summary" href="javascript: insertLessonContent('course/display-view-lesson-summary/${lesson.id}', 'tab-summary');"><i class="fas fa-book"></i><br><i class="menu-text-view-course">Мазмұны</i></a>
            <a id="tab-quiz" href="javascript: insertLessonContent('quiz/display-view-lesson-quiz/${lesson.id}','tab-quiz');"><i class="fas fa-edit"></i><br><i class="menu-text-view-course">Тест</i></a>
            <a id="tab-discussion" href="javascript: insertLessonContent('forum/display-view-forum/?lessonId=${lesson.id}','tab-discussion');"><i class="fas fa-comments"></i><br><i class="menu-text-view-course">Талқылау</i></a> 
        </div>

        <script src="resources/js/view-lesson.js"></script>
        <script>
            function viewTestResultsViewLessonJsp(lessonId) {
                includeContent('quiz/view-lesson-quiz-results/' + lessonId, 'right_content');
            }
            
            function editLessonViewLessonJsp(id) {
                includeContent('course/view-edit-lesson/' + id, 'right_content');
            }

            function deleteLessonViewLessonJsp(id) {
                Swal.fire({
                    title: 'Осы сабақты жойғыңыз келеді ма?',
                    text: "Барлық видеолар, тесттер, талқылаулыр түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/course/delete-lesson/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Сабақ сәтті жойылды.',
                                    'success'
                                );
                                divideContent();
                                includeContent('course/view-all-lessons/${course.id}', 'left_content');
                            });
                    }
                });
            }
            
            function alertForDiscussion () {
                Swal.fire({
                    title: 'Бұл арада техникалық жұмыстар жүріп жатыр... Талқылау беті жақында толықтай іске қосылады...',
                    showClass: {
                      popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                      popup: 'animate__animated animate__fadeOutUp'
                    }
                });
            }
            
            if('${lesson.status}' === 'c') {
                <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                    insertLessonContent('course/display-view-lesson-video/${lesson.id}', 'tab-video');
                </sec:authorize>
                <sec:authorize access="!hasAuthority('TUTOR') or !hasAuthority('MANAGER') and !isAuthenticated()">
                    includeContent("course/display-view-lesson-locked/${lesson.id}", "right_content");
                </sec:authorize>
            } else {
                insertLessonContent('course/display-view-lesson-video/${lesson.id}', 'tab-video');
            }
        </script>
    </body>
</html>
