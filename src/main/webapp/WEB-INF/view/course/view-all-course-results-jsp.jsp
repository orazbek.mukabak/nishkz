<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Барлық курс нәтижелерін қарау</title>
    </head>
    <body>
        <div id="testDiv">
            Барлық курс нәтижелерін қарау
        </div>
        <script>
            function getAllCoursesViewAllCourseResultsJsp() {
                fetch('${pageContext.request.contextPath}/course/get-all-courses/')
                    .then(response => response.json())
                    .then(data => {

                        let testVar = "";
                        for(var i = 0; i < data.length; i++) {
                            testVar += "<br><br><br>COURSE ID: " + data[i].id + ", CATEGORY: " + data[i].categoryId + ", DESCRIPTION: " + data[i].description + ", GRADE: " + data[i].grade + ", SUBJECT: " + data[i].subject + ", PRICE: " + data[i].price + ", PRIORITY: " + data[i].priority + ", USERNAME: " + data[i].username + ", PRICE: " + data[i].status + "<a href=\"javascript: divideContent(); includeContent('course/view-course-result/" + data[i].id + "', 'container_fluid');\" class='btn'>VIEW TOP TABLE</a>";
                        }

                        document.getElementById('testDiv').innerHTML = testVar;


                    });
            }
            getAllCoursesViewAllCourseResultsJsp();
        </script>
    </body>
</html>
