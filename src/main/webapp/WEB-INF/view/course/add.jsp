<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Жаңа сабақ қосу</title>
        <style>
            .tableView td {
                padding: 10px;
            }
            .tableInnerView td {
                padding: 5px;
                font-family: Verdana;
                font-size: 12px;
                color: #24303F;
            }
            .sceditor-container wysiwygMode ltr {
                width: 100% important;
            }
        </style>
    </head>
    <body>
        <br/>
            <div style="width: inherit; border-bottom: 1px solid #f1f1f1;">
            <table class="tableView">
                <tr>
                    <td>
                        <label style="font-size: 12px; font-weight: bolder; font-family: Verdana; color: #24303F;">Жаңа курс қосу:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="tableInnerView">
                            <tr>
                                <td>Категория: </td>
                                <td>
                                    <select id="categoryAddCourseJsp" name="category" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Категорияны талдаңыз...</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Сынып: </td>
                                <td>
                                    <select id="gradeAddCourseJsp" name="grade" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Сыныпты талдаңыз...</option>
                                        <option value="1">1 - сынып</option>
                                        <option value="2">2 - сынып</option>
                                        <option value="3">3 - сынып</option>
                                        <option value="4">4 - сынып</option>
                                        <option value="5">5 - сынып</option>
                                        <option value="6">6 - сынып</option>
                                        <option value="7">7 - сынып</option>
                                        <option value="8">8 - сынып</option>
                                        <option value="9">9 - сынып</option>
                                        <option value="10">10 - сынып</option>
                                        <option value="11">11 - сынып</option>
                                        <option value="0">Басқасы</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Пән: </td>
                                <td>
                                    <input id="subjectAddCourseJsp" name="subject" type="text" class="form-control input-sm" placeholder="Пәннің атауы..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Тілі: </td>
                                <td>
                                    <select id="languageAddCourseJsp" name="language" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Тілін талдаңыз...</option>
                                        <option value="Қазақша">Қазақша</option>
                                        <option value="Русский">Русский</option>
                                        <option value="Engish">Engish</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Курс туралы: </td>
                                <td>
                                    <input id="descriptionAddCourseJsp" name="description" type="text" class="form-control input-sm" placeholder="Курс туралы..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityAddCourseJsp" name="priority" type="number" step="0.01" class="form-control input-sm" value="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Бағасы: </td>
                                <td>
                                    <input id="priceAddCourseJsp" name="price" type="number" class="form-control input-sm" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td>Жеңілдік атауы: </td>
                                <td>
                                    <input id="discountTitleAddCourseJsp" name="discountTitle" type="text" class="form-control input-sm" value="" placeholder="Жеңілдіктің атауы..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Жеңілдік мөлшері: </td>
                                <td>
                                    <input id="discountValueAddCourseJsp" name="discountValue" type="number" class="form-control input-sm" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td>Видео ID: </td>
                                <td>
                                    <input id="videoIdAddCourseJsp" name="videoId" type="text" class="form-control input-sm" value="" placeholder="Видео ID..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ашылу уақыты: </td>
                                <td>
                                    <input id="startDateTimeAddCourseJsp" name="startDateTime" type="datetime-local" class="form-control input-sm" value="2020-01-01T00:00" />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 1: </td>
                                <td>
                                    <input id="feature1AddCourseJsp" name="feature1" type="text" class="form-control input-sm" value="" placeholder="1 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 2: </td>
                                <td>
                                    <input id="feature2AddCourseJsp" name="feature2" type="text" class="form-control input-sm" value="" placeholder="2 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 3: </td>
                                <td>
                                    <input id="feature3AddCourseJsp" name="feature3" type="text" class="form-control input-sm" value="" placeholder="3 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 4: </td>
                                <td>
                                    <input id="feature4AddCourseJsp" name="feature4" type="text" class="form-control input-sm" value="" placeholder="4 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 5: </td>
                                <td>
                                    <input id="feature5AddCourseJsp" name="feature5" type="text" class="form-control input-sm" value="" placeholder="5 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 6: </td>
                                <td>
                                    <input id="feature6AddCourseJsp" name="feature6" type="text" class="form-control input-sm" value="" placeholder="6 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 7: </td>
                                <td>
                                    <input id="feature7AddCourseJsp" name="feature7" type="text" class="form-control input-sm" value="" placeholder="7 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 8: </td>
                                <td>
                                    <input id="feature8AddCourseJsp" name="feature8" type="text" class="form-control input-sm" value="" placeholder="8 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 9: </td>
                                <td>
                                    <input id="feature9AddCourseJsp" name="feature9" type="text" class="form-control input-sm" value="" placeholder="9 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 10: </td>
                                <td>
                                    <input id="feature10AddCourseJsp" name="feature10" type="text" class="form-control input-sm" value="" placeholder="10 - ерекшелік..." />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return saveAddCourseJsp();'><i class="fas fa-save"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return cancelAddCourseJsp();'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                </tr>
            </table>
        </div>
        <%@ include file="../footer.jsp" %>
        <script>
            function cancelAddCourseJsp() {
                window.location.replace("${pageContext.request.contextPath}/course/view-all");
            }
            
            function saveAddCourseJsp() {
                var categoryId = document.getElementById('categoryAddCourseJsp').value;
                var grade = document.getElementById('gradeAddCourseJsp').value;
                var subject = document.getElementById('subjectAddCourseJsp').value;
                var language = document.getElementById('languageAddCourseJsp').value;
                var description = document.getElementById('descriptionAddCourseJsp').value;
                var price = document.getElementById('priceAddCourseJsp').value;
                var discountTitle = document.getElementById('discountTitleAddCourseJsp').value;
                var discountValue = document.getElementById('discountValueAddCourseJsp').value;
                var videoId = document.getElementById('videoIdAddCourseJsp').value;
                var startDateTime = document.getElementById('startDateTimeAddCourseJsp').value;
                var feature1 = document.getElementById('feature1AddCourseJsp').value;
                var feature2 = document.getElementById('feature2AddCourseJsp').value;
                var feature3 = document.getElementById('feature3AddCourseJsp').value;
                var feature4 = document.getElementById('feature4AddCourseJsp').value;
                var feature5 = document.getElementById('feature5AddCourseJsp').value;
                var feature6 = document.getElementById('feature6AddCourseJsp').value;
                var feature7 = document.getElementById('feature7AddCourseJsp').value;
                var feature8 = document.getElementById('feature8AddCourseJsp').value;
                var feature9 = document.getElementById('feature9AddCourseJsp').value;
                var feature10 = document.getElementById('feature10AddCourseJsp').value;
                var priority = document.getElementById('priorityAddCourseJsp').value;
                var username = "${username}";
                
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {
                    categoryId,
                    description,
                    grade,
                    subject,
                    language,
                    price,
                    discountTitle,
                    discountValue,
                    videoId,
                    feature1,
                    feature2,
                    feature3,
                    feature4,
                    feature5,
                    feature6,
                    feature7,
                    feature8,
                    feature9,
                    feature10,
                    priority,
                    username
                }
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                }
                
                fetch('${pageContext.request.contextPath}/course/add-course?startDateTime=' + startDateTime, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        window.location.replace("${pageContext.request.contextPath}/course/view-all");
                    });
            }
            
            function getAllCategoriesAddCourseJsp() {
                var category = document.getElementById('categoryAddCourseJsp');
                fetch('${pageContext.request.contextPath}/course/get-all-categories/')
                    .then(response => response.json())
                    .then(data => {
                        for(var i = 0; i < data.length; i++) {
                            var option = document.createElement("option");
                            option.value = data[i].id;
                            option.text = data[i].title;
                            category.appendChild(option);
                        }
                    });
            }
            getAllCategoriesAddCourseJsp();
        </script>
    </body>
</html>