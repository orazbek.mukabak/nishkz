<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Барлық сабақтарды көру</title>
        <link rel="stylesheet" href="resources/css/view-course.css" />
    </head>
    <body>
        <div id="all_lessons_view_all_lessons_jsp" class="lessons-titles-menu">
        </div>
        <div onclick='showLessonsMenu()' id="show_lessons_menu">
            <i class="fas fa-list"></i> Тақырыптар <i class="fas fa-caret-right"></i>
        </div>
    
        <script>
            function showLessonsMenu () {
                const lessonTitlesMenu = $("#all_lessons_view_all_lessons_jsp");
                const isMenuActive = lessonTitlesMenu.hasClass("lessons_menu_active");
                if (isMenuActive) {
                    lessonTitlesMenu.removeClass("lessons_menu_active");
                } else {
                    lessonTitlesMenu.addClass("lessons_menu_active");
                }
            }

            function closeLessonsMenu () {
                const lessonTitlesMenu = $("#all_lessons_view_all_lessons_jsp");
                const isMenuActive = lessonTitlesMenu.hasClass("lessons_menu_active");
                if (isMenuActive) {
                    lessonTitlesMenu.removeClass("lessons_menu_active");
                }
            }

            function highlightCurrentLessonTitle (index, title) {
                document.querySelector('.menu-item-active-view-all-lessons-jsp').className = 'menu-item-view-all-lessons-jsp';
                document.getElementById("lesson_titles_menu_item_"+index).classList.add('menu-item-active-view-all-lessons-jsp');
                document.getElementById("show_lessons_menu").innerHTML = "<i class=\"fas fa-list\"></i> Тақырыптар <i class=\"fas fa-caret-right\"></i> " + title;
            }

            function showLessonAndCloseMenu(id, index, title) {
                includeContent("course/view-lesson/" + id, "right_content");
                closeLessonsMenu();
                highlightCurrentLessonTitle(index, title);
            }

            function getLessonsByCourseIdViewAllLessonsJsp(courseId) {
                fetch('${pageContext.request.contextPath}/course/get-lessons-by-course-id/?id=' + courseId)
                    .then(response => response.json())
                    .then(data => {

                        const menuLessonItem = (title, id, priority, index) => {
                            const className = (index === 0) ? "menu-item-view-all-lessons-jsp menu-item-active-view-all-lessons-jsp" : "menu-item-view-all-lessons-jsp";

                            const imageSrc = data[i].status === "a" ? "resources/images/website/board.svg" : "resources/images/website/password.svg";

                            let temp = "<div id='lesson_titles_menu_item_"+index+"' onclick='return showLessonAndCloseMenu(\"" + id + "\",\"" + index + "\",\"" + title + "\");' class='"+className+"'><img src='"+imageSrc+"' /> ";
                            <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                                temp += "[" + priority + "] ";
                            </sec:authorize>
                            temp += title + "</div>";
                            return temp;
                        };

                        let menuList = "";
                        for(var i = 0; i < data.length; i++) {
                            menuList += menuLessonItem(data[i].title, data[i].id, data[i].priority, i);
                        }
                        <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                            menuList += "<div onclick='return includeContent(\"course/display-add-lesson/${course.id}\", \"right_content\");' class='menu-item-view-all-lessons-jsp'><img src='resources/images/website/add.svg' /> Жаңа сабақ қосу...</div>";
                        </sec:authorize>
                        menuList += "<div onclick='closeLessonsMenu()'' class='lessons-titles-menu-close'><i class='far fa-times-circle'></i></div>";
                        document.getElementById('all_lessons_view_all_lessons_jsp').innerHTML = menuList;

                        includeContent('course/view-lesson/' + data[0].id, 'right_content');
                    });
            }
            getLessonsByCourseIdViewAllLessonsJsp("${course.id}");
        </script>
    </body>
</html>
