<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Қысқаша видео</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view-lesson-video.css" />        
    </head>
    <body>
        <div class="view-lesson-video-playlist" id="view_lesson_video_playlist">
        </div>
    </body>
    <script>
        function deleteVideo(id) {
            Swal.fire({
                title: 'Осы видеоны жойғыңыз келеді ма?',
                text: "Осы видео түбегейлі жойылатын болады!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4B2D73',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Йә',
                cancelButtonText: 'Жоқ'
            }).then((result) => {
                if(result.value) {
                    fetch('${pageContext.request.contextPath}/course/delete-video/' + id)
                        .then(response => {
                            Swal.fire(
                                'Жойылды!',
                                'Видео сәтті жойылды.',
                                'success'
                            );
                            includeContent('${pageContext.request.contextPath}/course/view-lesson-video/${lesson.id}', 'page-video');
                        });
                }
            });
        }

        function postVideo(videoId, title, url, thumbnailUrl, duration, priority) {
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            const raw = {videoId, title, url, thumbnailUrl, duration, priority};

            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(raw)
            };

            fetch('${pageContext.request.contextPath}/course/add-video?lessonId=${lesson.id}', requestOptions)
                .then(response => response.text())
                .then(result => {
                    includeContent('${pageContext.request.contextPath}/course/view-lesson-video/${lesson.id}', 'page-video');
                });
            
        }
        
        function getVideoDetails (result) {
            
            fetch("https://vimeo.com/api/oembed.json?url=" + result[1])
                .then(response => response.json())
                .then(data => {
                    postVideo(data.video_id, result[0], result[1], data.thumbnail_url, data.duration, result[2]);
                });
        }
        
        function displayAddVideoForm() {
            Swal.mixin({
            input: 'text',
            confirmButtonText: 'Келесі',
            showCancelButton: true,
            progressSteps: ['1', '2', '3']
          }).queue([
            {
              title: 'Видеоның атауы:'
            },
            'Видеоның URL әдірісі:',
            'Видеоның орны:'
          ]).then((result) => {
            if (result.value) {
              Swal.fire("Видео сәтті қосылды!");
              getVideoDetails(result.value);
            }
          });
        }

        function getVideoDuration (SECONDS) {
            var date = new Date(null);
            date.setSeconds(SECONDS); // specify value for SECONDS here
            var result = date.toISOString().substr(11, 8);

            return result;
        }
        
        function getVideos() {
            fetch('${pageContext.request.contextPath}/course/get-videos-by-lesson-id/?lessonId=${lesson.id}')
                .then(response => response.json())
                .then(data => {
                    let playList = "";
                    if(data.length > 0) {
                        for(var i = 0; i < data.length; i++) {
                            const videoTitle = data[i].title;
                            let className = 'view-lesson-video-playlist-item';

                            playList += "<div class='video-item-wrapper-view-lesson-video'>"
                                playList += "<div id='view-lesson-video-playlist-item-"+i+"' class='"+className+"' onclick=\"return sweetAlertPlayMainVideo(" + data[i].videoId + ");\">";
                                playList += "<img src='" + data[i].thumbnailUrl + "' />";
                                playList += "<div class='video-info-view-lesson-video'>";
                                    playList += "<h4>" + data[i].title + "</h4>";
                                    playList += "<h5>" + getVideoDuration(data[i].duration) + "</h5>";
                                playList += "</div>";
                                playList += "</div>";
                                <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                                    playList += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                                    playList += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                                    //playList += "<a class='dropdown-item' href='javascript: editQuestionViewQuizJsp(" + id + ");'><i class='fas fa-pen'></i> Сұрақты өңдеу</a>";
                                    playList += "<a class='dropdown-item' href='javascript: deleteVideo(" + data[i].id + ");'><i class='fas fa-trash-alt'></i> Видеоны жою</a>";
                                    playList += "</div></div>";
                                </sec:authorize>
                            playList += "</div>"
                        }
                    }
                    <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                        playList += "<div class='add-video-view-lesson-video' onclick='return displayAddVideoForm();'>+ Add new video</div>";
                    </sec:authorize>
                    document.getElementById('view_lesson_video_playlist').innerHTML = playList;
                });
        }

        function drawLiveBtn() {
            let varHtml = "";

            varHtml += "<a class='live-video-btn-view-lesson' href='${lesson.liveUrl}'>";
                varHtml += "Тікелей эфир";
            varHtml += "</a>";


            document.getElementById('view_lesson_video_playlist').innerHTML = varHtml;
        }

        function sweetAlertPlayMainVideo(videoId) {
            Swal.fire({
                customClass: 'sweetAlertMainVideo',
                html: "<iframe src='//player.vimeo.com/video/" + videoId + "?&autoplay=1&title=0&byline=0' width='640px' height='360px' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>",  
                showConfirmButton: false
            });
        }

        if ("${lesson.liveUrl}") {
            drawLiveBtn();
        } else {
            getVideos();
        }
    </script>
</html>
