<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Барлық курстарды көру</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view-all.css" />
    </head>
    <body>
        
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb custom-breadcrumb">
              <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/">Бас бет</a></li>
              <li class="breadcrumb-item active" aria-current="page">Курстар</li>
            </ol>
        </nav>
        
        <div id="divViewAllCoursesJsp">
        </div>
        <%@ include file="../footer.jsp" %>
        <script>

            function updateCourseStatusViewAllCoursesJsp(id, status) {
                fetch('${pageContext.request.contextPath}/course/update-course-status/' + id + '?status=' + status)
                    .then(response => {
                        includeContent("course/display-all-courses", "container_fluid");
                    });
            }
            
            function deleteCourseViewAllCoursesJsp(id) {
                Swal.fire({
                    title: 'Осы курсты жойғыңыз келеді ма?',
                    text: "Барлық сабақтар, видеолар, тесттер, талқылаулыр түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/course/delete-course/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Курс сәтті жойылды.',
                                    'success'
                                );
                                window.location.replace("${pageContext.request.contextPath}/course/view-all");
                            });
                    }
                });
            }

            function adminPanelViewAllCourses (coursesId) {
                varHtml = "";
                <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                varHtml += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                varHtml += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                varHtml += "<a class='dropdown-item' href='${pageContext.request.contextPath}/course/edit/" + coursesId + "'><i class='fas fa-edit'></i> Курсты өңдеу</a>";
                varHtml += "<a class='dropdown-item' href='${pageContext.request.contextPath}/course/edit-picture/" + coursesId + "'><i class='fas fa-image'></i> Суретін өңдеу</a>";
                varHtml += "<a class='dropdown-item' href=\"javascript: includeContent('course/view-course-result/" + coursesId + "', 'container_fluid');\"><i class='fas fa-balance-scale-right'></i> Курс нәтижесі</a>";
                <sec:authorize access="hasAuthority('MANAGER')">
                    varHtml += "<a class='dropdown-item' href='javascript: updateCourseStatusViewAllCoursesJsp(" + coursesId + ", \"c\");'><i class='fas fa-pause-circle'></i> Курсты тоқтату</a>";
                </sec:authorize>
                varHtml += "<a class='dropdown-item' href='javascript: deleteCourseViewAllCoursesJsp(" + coursesId + ");'><i class='fas fa-trash-alt'></i> Курсты жою</a></div></div>";
                </sec:authorize>

                return varHtml;
            }

            function drawCousesInCategory_ViewAllCoursesJsp(category,coursesList) {
                let varHtml = "";
                for(let i = 0; i < coursesList.length; i++) {
                    if (coursesList[i].categoryId === category.id) {
                            varHtml += "<div class='card-view-course'>";

                                
                                
                                varHtml += "<img src='https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fcourses%2F" + coursesList[i].id + ".jpg?alt=media' onerror=\"this.onerror=null; this.src='https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fcourses%2Ferror.jpg?alt=media'\" />";

                                varHtml += "<div class='card-view-course-info'>";
                                    varHtml += "<h5>" + coursesList[i].subject + "</h5>";
                                    varHtml += "<h6>" + coursesList[i].grade + "-сынып</h6>";
                                    varHtml += "<a href=\"${pageContext.request.contextPath}/course/view/" + coursesList[i].id + "\" class='btn'>Курсқа кіру</a>";
                                    varHtml += "<p>" + coursesList[i].description + "</p>";
                                varHtml += "</div>";

                                varHtml += adminPanelViewAllCourses(coursesList[i].id);
                            varHtml += "</div>";
                    }
                }

                return varHtml;
                        
            }

            function drawCategory_ViewAllCoursesJsp(category, coursesList) {
                let varHtml = "";

                varHtml += "<div class='category-wrapper-view-all-courses-jsp'>";
/*                    varHtml += "<div class='category-title-view-all-courses-jsp'>";
                        varHtml += category.title;
                    varHtml += "</div>";*/
                    varHtml += "<div class='category-courses-view-all-courses-jsp'>";
                        varHtml += drawCousesInCategory_ViewAllCoursesJsp(category,coursesList);
                    varHtml += "</div>";
                varHtml += "</div>";

                return varHtml;
            }

            function drawAddCourse_ViewAllCoursesJsp() {
                varHtml = "";

                <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                    varHtml += "<div class='card-view-course'>";

                        varHtml += "<img src='https://firebasestorage.googleapis.com/v0/b/ozatonline/o/images%2Fcourse%2Fadd.png?alt=media' />";
                        
                        varHtml += "<div class='card-view-course-info'>";
                            varHtml += "<h5>Құрметті ұстаз,</h5>";
                            varHtml += "<p>OZAT.Online платформасында өз курсыңызды ашыңыз...  </p>";
                            varHtml += "<a class='btn' href='${pageContext.request.contextPath}/course/add'>Add New Course</a>";
                        varHtml += "</div>";
                    varHtml += "</div>";
                </sec:authorize>

                return varHtml;
            }

            function drawAllCourses_ViewAllCoursesJsp(categoryList, coursesList) {
                let varHtml = "";

                for (let i=0; i < categoryList.length; i++) {
                    varHtml += drawCategory_ViewAllCoursesJsp(categoryList[i], coursesList);
                }

                varHtml += drawAddCourse_ViewAllCoursesJsp();

                document.getElementById('divViewAllCoursesJsp').innerHTML = varHtml;
            }
            
            function getAllCoursesViewAllCoursesJsp(categoryList) {
                $.ajax({
                    type: 'get',
                    url: "${pageContext.request.contextPath}/course/get-all-courses/",
                    data: {},
                    cache: false,
                    success: function(data) {
                        drawAllCourses_ViewAllCoursesJsp(categoryList, data);
                    },
                    error: function(data, status, error) {
                    }
                });
            }
            
            function uploadFileSweetAlert() {
                swal({
                    input: 'file',
                    inputAttributes: {
                        name:"upload[]",
                        id:"fileToUpload"
                    },
                    showCloseButton: true,
                    showCancelButton: true
                }).then(function() {
                    var formData = new FormData();
                    formData.append('fileToUpload', $('#fileToUpload')[0]);
                    $.ajax({type:"POST",url:"script\\php\\upload.php",data: formData,processData: false,contentType: false,headers:{"Content-Type":"multipart/form-data"},async:false});
                })
            }

            function compareByPriority( a, b ) {
              if ( a.priority < b.priority ){
                return -1;
              }
              if ( a.priority > b.priority ){
                return 1;
              }
              return 0;
            }

            
            function getAllCategoriesViewAllCoursesJsp() {
                fetch('${pageContext.request.contextPath}/course/get-all-categories/')
                    .then(response => response.json())
                    .then(data => {
                        getAllCoursesViewAllCoursesJsp(data.sort(compareByPriority));
                    });
            }
            getAllCategoriesViewAllCoursesJsp();
        </script>
    </body>
</html>
