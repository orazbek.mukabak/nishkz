<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/croppie.css" />
        <title>Курс суретін өзгерту</title>
    </head>
    <body>
        <div>
            <input type="file" id="fileEditCoursePictureJsp" accept="image/jpg" />
            <br>
            <button id="sendEditCoursePictureJsp">Upload</button>
            <p id="uploadingEditCoursePictureJsp"></p>
            <progress value="0" max="100" id="progressEditCoursePictureJsp"></progress>
        </div>
        <%@ include file="../footer.jsp" %>
        <script src="${pageContext.request.contextPath}/resources/js/firebase-app.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/firebase-storage.js"></script>
        <script>
            var firebaseConfig = {
                apiKey: "AIzaSyBWHMIqx_yDT7hDiC3fB9D9tgO7M4zlG_w",
                authDomain: "nishkz.firebaseapp.com",
                storageBucket: "nishkz"
            };
            firebase.initializeApp(firebaseConfig);

            var files = [];
            document.getElementById("fileEditCoursePictureJsp").addEventListener("change", function(e) {
                files = e.target.files;
            });

            document.getElementById("sendEditCoursePictureJsp").addEventListener("click", function() {
                if(files.length !== 0) {
                    let storage = firebase.storage().ref('/images/courses/${course.id}.jpg'); 
                    let upload = storage.put(files[0]);
                    upload.on("state_changed", function progress(snapshot) {
                        let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        document.getElementById("progressEditCoursePictureJsp").value = percentage;
                    },
                    function error() {
                        Swal.fire({
                            position: 'center',
                            title: 'Кешіріңіз, белгісіз қателік орын алды. +7 (775) 899-77-55 ұялы байланыс нөміріне хабарласуыңызды өтінеміз.',
                            confirmButtonColor: '#4B2D73'
                        });
                    },
                    function complete() {
                        Swal.fire({
                            position: 'center',
                            title: 'Сурет сәтті жүктелді...',
                            confirmButtonColor: '#4B2D73'
                        }).then((result) => {
                            if(result.value) {
                                location.reload();
                            }
                        });
                    });

                } else {
                    Swal.fire({
                        position: 'center',
                        title: 'Кешіріңіз, ешқандай сурет табылмады...',
                        confirmButtonColor: '#4B2D73'
                    });
                }
            });
        </script>
    </body>
</html>
