<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Жаңа сабақ қосу</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/lib/sceditor/default.min.css" id="theme-style" />
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/sceditor.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/jquery.sceditor.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/monocons.js"></script>
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/xhtml.js"></script>
        <style>
            .tableView td {
                padding: 10px;
            }
            .tableInnerView td {
                padding: 5px;
                font-family: Verdana;
                font-size: 12px;
                color: #24303F;
            }
            .sceditor-container wysiwygMode ltr {
                width: 100% important;
            }
        </style>
    </head>
    <body>
        <br/>
            <div style="width: inherit; border-bottom: 1px solid #f1f1f1;">
            <table class="tableView">
                <tr>
                    <td>
                        <label style="font-size: 12px; font-weight: bolder; font-family: Verdana; color: #24303F;"><span class="glyphicon glyphicon-exclamation-sign"></span> Жаңа сабақ қосу:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="tableInnerView">
                            <tr>
                                <td>Сабақтың тақырыбы: </td>
                                <td><input id="titleAddLessonJsp" name="title" type="text" class="form-control input-sm" placeholder="тақырып..." /></td>
                            </tr>
                            <tr>
                                <td>Live URL: </td>
                                <td><input id="liveUrlAddLessonJsp" name="liveUrl" type="text" class="form-control input-sm" placeholder="Youtube Live URL..." /></td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityAddLessonJsp" name="priority" type="number" step="0.01" class="form-control input-sm" value="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Ашылу уақыты: </td>
                                <td>
                                    <input id="startDateTimeAddLessonJsp" name="startDateTime" type="datetime-local" class="form-control input-sm" value="2020-01-01T00:00" />
                                </td>
                            </tr>
                            <tr>
                                <td>Тест тапсырмасы: </td>
                                <td>
                                    <select id="quizAllowedAddLessonJsp" name="quizAllowed" class="form-control input-sm">
                                        <option value="0" selected>Жоқ, тест тапсырмасы болмайды</option>
                                        <option value="1">Йә, тест тапсырмасы болады</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Статусы: </td>
                                <td>
                                    <select id="statusAddLessonJsp" name="status" class="form-control input-sm">
                                        <option value="c" selected>Құлыпталған</option>
                                        <option value="a">Ашық</option>
                                        <option value="s">Тегін</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea id="summaryAddLessonJsp" name="summary" class="form-control input-sm" style="min-height: 300px; width: 100%;">Сабақтың мазмұны...</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return saveAddLessonJsp();'><i class="fas fa-save"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return  window.location.replace("${pageContext.request.contextPath}/course/view/${course.id}");'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                </tr>
            </table>
        </div>
        <script>
            var varSceditor = document.getElementById('summaryAddLessonJsp');
            sceditor.create(varSceditor, {
                width: "100%",
                    format: 'xhtml',
                    icons: 'monocons',
                    style: '${pageContext.request.contextPath}/resources/lib/sceditor/content.default.min.css'
            });
  
            function saveAddLessonJsp() {
                var title = document.getElementById('titleAddLessonJsp').value;
                var liveUrl = document.getElementById('liveUrlAddLessonJsp').value;
                var summary = encodeURI(sceditor.instance(varSceditor).val());
                var priority = document.getElementById('priorityAddLessonJsp').value;
                var startDateTime = document.getElementById('startDateTimeAddLessonJsp').value;
                var quizAllowed = document.getElementById('quizAllowedAddLessonJsp').value;
                var status = document.getElementById('statusAddLessonJsp').value;
                
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {
                    title,
                    liveUrl,
                    summary,
                    priority,
                    quizAllowed,
                    status
                };
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };
                
                fetch('${pageContext.request.contextPath}/course/add-lesson?courseId=${course.id}&startDateTime=' + startDateTime, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        window.location.replace("${pageContext.request.contextPath}/course/view/${course.id}");
                    });
            }
        </script>
    </body>
</html>