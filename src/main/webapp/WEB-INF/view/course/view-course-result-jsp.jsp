<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Курс нәтижесін қарау</title>
        <link rel="stylesheet" href="resources/css/view-course-results.css" />
    </head>
    <body>
        <div id="outer_wrapper_course_results_jsp">
            <div id="inner_wrapper_course_results_jsp">
            </div>
        </div>
        <script>
            function getLessonsByCourseId(courseId, sortedArray) {
                fetch('${pageContext.request.contextPath}/course/get-lessons-by-course-id/?id=' + courseId)
                    .then(response => response.json())
                    .then(data => {
                        drawResults_ViewCourseResultsJsp(sortedArray, data);
                    });
            }

            function getLessonResult_ViewCourseResultsJsp(result, lessonId) {

                for (let i=0; i < result.testResults.length; i++) {
                    if (result.testResults[i].lessonId === lessonId) {
                        return result.testResults[i];
                    }
                }

                return null;
            }

            function drawSingleRow_ViewCourseResultsJsp(result, lessonsList, index) {

                htmlStr = "";
                
                let rowClassName = 'row-view-course-results-jsp';
                
                rowClassName += (index % 2) ? ' light-row-view-course-results-jsp' : ' dark-row-view-course-results-jsp';

                htmlStr += "<div class='"+rowClassName+"'>";
                    htmlStr += "<div class='username-row-view-course-results-jsp'>";
                        htmlStr += result.username;
                    htmlStr += "</div>";

                    

                    for (let i=0; i < lessonsList.length; i++) {
                        htmlStr += "<div class='lesson-score-row-view-course-results-jsp'>";
                            const lessonResult = getLessonResult_ViewCourseResultsJsp(result,lessonsList[i].id);

                            if (lessonResult !== null) {
                                htmlStr += "<div>";
                                    htmlStr += lessonResult.correctAnswers;
                                htmlStr += "</div>";
                            }
                             
                        htmlStr += "</div>";
                    }

                    htmlStr += "<div class='totalCourseScore-row-view-course-results-jsp'>";
                        htmlStr += result.totalCourseScore;
                    htmlStr += "</div>";

                htmlStr += "</div>";

                return htmlStr;
            }

            function drawHeaderRow_ViewCourseResultsJsp(lessonsList) {

                htmlStr = "";

                htmlStr += "<div class='row-view-course-results-jsp'>";
                    htmlStr += "<div class='username-row-view-course-results-jsp'>";
                        htmlStr += "Логин";
                    htmlStr += "</div>";

                    htmlStr += "<div class='totalCourseScore-row-view-course-results-jsp'>";
                        htmlStr += "Жалпы балл";
                    htmlStr += "</div>";

                    for (let i=0; i < lessonsList.length; i++) {
                        htmlStr += "<div class='lesson-score-row-view-course-results-jsp'>";
                            htmlStr += lessonsList[i].title;
                        htmlStr += "</div>";
                    }
                htmlStr += "</div>";

                return htmlStr;
            }

            function drawTitle_ViewCourseResultsJsp () {
                var htmlStr = "";

                htmlStr += "<div class='ranking-view-course-results-jsp'>";
                    htmlStr += "<img src='resources/images/website/trophy.svg'>"
                    htmlStr += "Рейтинг";
                htmlStr += "</div>";
                htmlStr += "<div class='course-title-view-course-results-jsp'>";
                    htmlStr += "${course.subject}";
                htmlStr += "</div>";

                return htmlStr;
            }

            function drawResults_ViewCourseResultsJsp (results, lessonsList) {

                let htmlStr = "";
                //htmlStr += drawHeaderRow_ViewCourseResultsJsp(lessonsList);

                htmlStr += drawTitle_ViewCourseResultsJsp();

                for (let i=0; i < results.length; i++) {
                    htmlStr += drawSingleRow_ViewCourseResultsJsp(results[i], lessonsList, i);
                }

                document.getElementById('inner_wrapper_course_results_jsp').innerHTML = htmlStr;
            }



            function sortByTotalScore( a, b ) {
              if ( a.totalCourseScore < b.totalCourseScore ){
                return 1;
              }
              if ( a.totalCourseScore > b.totalCourseScore ){
                return -1;
              }
              return 0;
            }

            function restructureArray(inputArray) {
                let newArray = [];

                for (let i=0; i < inputArray.length; i++) {
                    const username = inputArray[i].username;

                    let inTheNewArray = false;
                    for (let j=0; j < newArray.length; j++) {
                        if (newArray[j].username === username) {
                            inTheNewArray = true;
                            newArray[j].totalCourseScore += inputArray[i].correctAnswers;
                            newArray[j].testResults.push(inputArray[i]);
                        }
                    }

                    if (!inTheNewArray) {
                        newArray.push({"username" : username, "totalCourseScore": inputArray[i].correctAnswers, testResults: [inputArray[i]]});
                    }
                }

                const sortedArray = newArray.sort(sortByTotalScore);
                console.log(sortedArray);

                getLessonsByCourseId("${course.id}", sortedArray);
            }
            
            function getParticipantsByCourseId(courseId) {
                fetch('${pageContext.request.contextPath}/quiz/get-participants-by-course-id/?courseId=' + courseId)
                    .then(response => response.json())
                    .then(data => {
                        restructureArray(data);
                    });
            }


            getParticipantsByCourseId("${course.id}");
        </script>
    </body>
</html>
