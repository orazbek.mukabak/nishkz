<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Қысқаша видео</title>        
    </head>
    <body>
         <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
            <div>
                <a class="nav-link dropdown-toggle"  style="float: right;" href="" role="button" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <!--a class="dropdown-item" href="javascript: viewTestResultsViewLessonSummary(${lesson.id});"><i class="fas fa-list-ol"></i> Тест нәтижелерін қарау</a-->
                    <a class="dropdown-item" href="javascript: editLessonViewLessonSummary(${lesson.id});"><i class="fas fa-pen"></i> Сабақты өңдеу</a>
                    <a class="dropdown-item" href="javascript: deleteLessonViewLessonSummary(${lesson.id});"><i class="fas fa-trash-alt"></i> Сабақты жою</a>
                </div>
            </div>
        </sec:authorize>
        <div class="view-lesson-summary-jsp" id="content-view-lesson-summary-jsp">
        </div>
        <script>
            var encoded = "${lesson.summary}";
            var decodedSummary = decodeURI(encoded);
            document.getElementById('content-view-lesson-summary-jsp').innerHTML = decodedSummary;
            
            function deleteLessonViewLessonSummary(id) {
                Swal.fire({
                    title: 'Осы сабақты жойғыңыз келеді ма?',
                    text: "Барлық видеолар, тесттер, талқылаулыр түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/course/delete-lesson/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Сабақ сәтті жойылды.',
                                    'success'
                                );
                                window.location.replace("${pageContext.request.contextPath}/course/view/${lesson.course.id}");
                            });
                    }
                });
            }
            
            function editLessonViewLessonSummary(id) {
                includeContent('${pageContext.request.contextPath}/course/view-edit-lesson/' + id, 'page-summary');
            }
            
            function sweetAlertPlaySolutionSummary(videoId) {
                Swal.fire({
                    customClass: 'sweetAlertSolution',
                    html: "<iframe src='//player.vimeo.com/video/" + videoId + "?&autoplay=1&title=0&byline=0' width='640px' height='360px' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>",  
                    showConfirmButton: false
                });
            }
        </script>
    </body>
</html>
