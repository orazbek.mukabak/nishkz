<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Template for the new android layout">
        <meta name="author" content="Andrew Henry">
        <link rel="icon" href="#">
        <title>Bootstrap Nav Template</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view-lesson.css">
    </head>
    <body>
      <div class="container-fluid">
          
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb custom-breadcrumb">
                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/">Бас бет</a></li>
                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/course/view-all">Курстар</a></li>
                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/course/view/${course.id}">${course.subject}</a></li>
                <li class="breadcrumb-item" aria-current="page">${lesson.title}</li>
            </ol>
        </nav>
          
         <div id="page-lessons" class="active">
         </div>
          
          <div id="page-video" class="inactive">
         </div>

         <div id="page-summary" class="inactive">
         </div>

         <div id="page-quiz" class="inactive">
         </div>
      </div>

      <!-- Bottom Nav Bar -->
      <footer class="footer-menu">
         <div id="buttonGroup" class="btn-group selectors" role="group" aria-label="Basic example">
            <button id="lessons" type="button" class="btn btn-secondary button-active">
               <div class="selector-holder">
                  <i class="fas fa-th-list"></i>
                  <span id="lessons_view_lesson">Тақырыптар</span>
               </div>
            </button>
            <button id="video" type="button" class="btn btn-secondary button-inactive" onclick="includeContent('${pageContext.request.contextPath}/course/view-lesson-video/${lesson.id}', 'page-video');">
               <div class="selector-holder">
                  <i class="fab fa-youtube"></i>
                  <span id="video_view_lesson">Сабақ</span>
               </div>
            </button>
               <button id="summary" type="button" class="btn btn-secondary button-inactive" onclick="includeContent('${pageContext.request.contextPath}/course/view-lesson-summary/${lesson.id}', 'page-summary');">
               <div class="selector-holder">
                  <i class="fas fa-book-reader"></i>
                  <span id="summary_view_lesson">Өздік жұмыс</span>
               </div>
            </button>
            <button id="quiz" type="button" class="btn btn-secondary button-inactive" onclick="viewQuizViewLesson();">
               <div class="selector-holder">
                  <i class="fas fa-spell-check"></i>
                  <span id="quiz_view_lesson">Тест</span>
               </div>
            </button>
         </div>
      </footer>
      <script src="${pageContext.request.contextPath}/resources/js/view-lesson.js"></script>
      <script>

        const lanKazViewLesson = {
            "lessons": "Тақырыптар",
            "video": "Сабақ",
            "summary": "Өздік жұмыс",
            "quiz": "Тест",
            "dearStudent": "Құрметті оқушы,",
            "noLoginTestMessage": "Тестілеу бөліміне өту үшін сізге сайтқа кіру керек..."
        }

        const lanRuViewLesson = {
            "lessons": "Темы",
            "video": "Урок",
            "summary": "Самостоятельная работа",
            "quiz": "Тест",
            "dearStudent": "Дорогой студент,",
            "noLoginTestMessage": "Для перехода в раздел тестирования необходимо авторизоваться на сайте ..."
        }

        let currentLangViewLesson = lanRuViewLesson;

        if (getLanguage() === 'kaz') {
          currentLangViewLesson = lanKazViewLesson;
        }

        document.querySelectorAll("#lessons_view_lesson")[0].innerHTML = currentLangViewLesson.lessons;
        document.querySelectorAll("#video_view_lesson")[0].innerHTML = currentLangViewLesson.video;
        document.querySelectorAll("#summary_view_lesson")[0].innerHTML = currentLangViewLesson.summary;
        document.querySelectorAll("#quiz_view_lesson")[0].innerHTML = currentLangViewLesson.quiz;

        function viewQuizViewLesson() {
            if("${username}".length === 0) {
                Swal.fire({
                    position: 'center',
                    title: currentLangViewLesson.dearStudent,
                    text:  currentLangViewLesson.noLoginTestMessage,
                    confirmButtonColor: '#4B2D73',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                    window.location.replace("${pageContext.request.contextPath}/signin?u=null&p=null");
                });
            } else {
                includeContent('${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${lesson.id}', 'page-quiz');
            }
        }

        function handleLessonItemClickForNewUser (lessonId, lessonStatus) {
          if (lessonStatus !== 's') {
            const sweetAlertMessage = "Сіз бұл тақырыпты көре алмайсыз. <br> Сатып алу үшін +7 778 001-10-29 номеріне хабарласыңыз. <br> Егер сіз сатып алған болсаңыз, өз аккаунтыңызбен сайтқа кіруіңізді өтінеміз.";
            Swal.fire(
                'Курсты сатып алу',
                sweetAlertMessage,
                'info'
            );
          }
        }
        
        function drawSingleLessonTitle_ViewLesson (lesson) {
          let varHtml = "";
          let imgSrc = 'movies.svg';

          if (lesson.status === 'c') {
            imgSrc = 'lock.svg';
          }

          if ("${status}" === 'STATUS.NEWUSER') {
            let itemClassNameForNewUser = "not-paid-lesson-item-view-lesson";

            if (lesson.status === 's') {
              itemClassNameForNewUser += " free-lesson-item";
            }

            if ("${lesson.id}" == lesson.id) {
              itemClassNameForNewUser += " active-lesson-item";
            }

            varHtml += "<div class='"+itemClassNameForNewUser+"' onclick='handleLessonItemClickForNewUser("+lesson.id+", \""+lesson.status+"\")'>";
              varHtml += "<img src='${pageContext.request.contextPath}/resources/images/website/"+imgSrc+"'/>";
              varHtml += "<span>"; 
                varHtml += lesson.title;
              varHtml += "</span>";

              if (lesson.status === 's') {
                varHtml += "<span class='preview-lesson-item-view-lesson'>"; 
                  varHtml += '(Пробный урок)';
                varHtml += "</span>";
              }

            varHtml += "</div>";
          } else {
            let className = "lesson-item-view-lesson";
          
            if ("${lesson.id}" == lesson.id) {
              className += " active-lesson-item";
            }

            if (lesson.status === 'c') {
                
                let lockedLessonString = "<a class='locked-lesson-item-view-lesson' href='#'>";
                
                <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER')">
                  lockedLessonString = "<a class='"+className+"' href='${pageContext.request.contextPath}/course/view-lesson/" + lesson.id + "'>";
                </sec:authorize>
                
                varHtml += lockedLessonString;
            } else {
              varHtml += "<a class='"+className+"' href='${pageContext.request.contextPath}/course/view-lesson/" + lesson.id + "'>";
            }
            

              varHtml += "<img src='${pageContext.request.contextPath}/resources/images/website/"+imgSrc+"'/>";
              varHtml += "<span>"; 
                varHtml += lesson.title;
              varHtml += "</span>";

              if (lesson.status === 's') {
                varHtml += "<span class='preview-lesson-item-view-lesson'>"; 
                  varHtml += '(Пробный урок)';
                varHtml += "</span>";
              }

              let startTime = "";
              if (lesson.startDateTime.date){
                  startTime += lesson.startDateTime.date.year+"-"+lesson.startDateTime.date.month+"-"+lesson.startDateTime.date.day+" "+lesson.startDateTime.time.hour+":"+lesson.startDateTime.time.minute;
              } else {
                  startTime += lesson.startDateTime.year+"-"+lesson.startDateTime.monthValue+"-"+lesson.startDateTime.dayOfMonth+" "+lesson.startDateTime.hour+":"+lesson.startDateTime.minute;
              }

              if (lesson.status === 'c') {
                varHtml += "<span class='start-time-lesson-item-view-lesson'>"; 
                  varHtml += '(' + startTime + ')';
                varHtml += "</span>";
              }
            varHtml += "</a>";
          }
          

            

          return varHtml;
        }

        function getLessonsByCourseIdViewPage(courseId) {
            fetch('${pageContext.request.contextPath}/course/get-lessons-by-course-id/?id=' + courseId)
                .then(response => response.json())
                .then(data => {
                    let varHtml = "";
                    for(var i = 0; i < data.length; i++) {
                      varHtml += drawSingleLessonTitle_ViewLesson(data[i]);
                    }
                    <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                        varHtml += "<div onclick='return includeContent(\"${pageContext.request.contextPath}/course/display-add-lesson/" + courseId + "\", \"page-lessons\");' class='add-lesson-view-lesson'> + Add new lesson</div>";
                    </sec:authorize>
                    document.getElementById('page-lessons').innerHTML = varHtml;

                });
        }
        getLessonsByCourseIdViewPage("${course.id}");
        includeContent('${pageContext.request.contextPath}/course/view-lesson-video/${lesson.id}', 'page-video');

      </script>
    </body>
</html>
