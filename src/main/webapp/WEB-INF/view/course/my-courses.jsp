<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Барлық курстарды көру</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view-all.css" />
    </head>
    <body>
        <div id="divMyCourses">
        </div>
        <%@ include file="../footer.jsp" %>
        <script>
            function getMyCourses() {
                fetch('${pageContext.request.contextPath}/course/get-my-courses')
                    .then(response => response.json())
                    .then(data => {
                        console.log(data);
                    });
            }
            getMyCourses();
        </script>
    </body>
</html>
