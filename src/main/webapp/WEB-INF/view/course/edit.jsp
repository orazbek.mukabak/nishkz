<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Жаңа сабақ қосу</title>
        <style>
            .tableView td {
                padding: 10px;
            }
            .tableInnerView td {
                padding: 5px;
                font-family: Verdana;
                font-size: 12px;
                color: #24303F;
            }
            .sceditor-container wysiwygMode ltr {
                width: 100% important;
            }
        </style>
    </head>
    <body>
        <br/>
            <div style="width: inherit; border-bottom: 1px solid #f1f1f1;">
            <table class="tableView">
                <tr>
                    <td>
                        <label style="font-size: 12px; font-weight: bolder; font-family: Verdana; color: #24303F;">Жаңа курс қосу:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="tableInnerView">
                            <tr>
                                <td>Категория: </td>
                                <td>
                                    <select id="categoryEditCourseJsp" name="category" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Категорияны талдаңыз...</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Сынып: </td>
                                <td>
                                    <select id="gradeEditCourseJsp" name="grade" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Сыныпты талдаңыз...</option>
                                        <option id="grade1EditCourseJsp" value="1">1 - сынып</option>
                                        <option id="grade2EditCourseJsp" value="2">2 - сынып</option>
                                        <option id="grade3EditCourseJsp" value="3">3 - сынып</option>
                                        <option id="grade4EditCourseJsp" value="4">4 - сынып</option>
                                        <option id="grade5EditCourseJsp" value="5">5 - сынып</option>
                                        <option id="grade6EditCourseJsp" value="6">6 - сынып</option>
                                        <option id="grade7EditCourseJsp" value="7">7 - сынып</option>
                                        <option id="grade8EditCourseJsp" value="8">8 - сынып</option>
                                        <option id="grade9EditCourseJsp" value="9">9 - сынып</option>
                                        <option id="grade10EditCourseJsp" value="10">10 - сынып</option>
                                        <option id="grade11EditCourseJsp" value="11">11 - сынып</option>
                                        <option id="grade0EditCourseJsp" value="0">Басқасы</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Пән: </td>
                                <td>
                                    <input id="subjectEditCourseJsp" name="subject" type="text" class="form-control input-sm" value="${course.subject}" placeholder="Пәннің атауы..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Тілі: </td>
                                <td>
                                    <select id="languageEditCourseJsp" name="language" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Тілін талдаңыз...</option>
                                        <option id="languageҚазақшаEditCourseJsp" value="Қазақша">Қазақша</option>
                                        <option id="languageРусскийEditCourseJsp" value="Русский">Русский</option>
                                        <option id="languageEngishEditCourseJsp" value="Engish">Engish</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Курс туралы: </td>
                                <td>
                                    <input id="descriptionEditCourseJsp" name="description" type="text" class="form-control input-sm" value="${course.description}" placeholder="Курс туралы..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityEditCourseJsp" name="priority" type="number" step="0.01" class="form-control input-sm" value="${course.priority}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Бағасы: </td>
                                <td>
                                    <input id="priceEditCourseJsp" name="price" type="number" class="form-control input-sm" value="${course.price}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Жеңілдік атауы: </td>
                                <td>
                                    <input id="discountTitleEditCourseJsp" name="discountTitle" type="text" class="form-control input-sm" value="${course.discountTitle}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Жеңілдік мөлшері: </td>
                                <td>
                                    <input id="discountValueEditCourseJsp" name="discountValue" type="number" class="form-control input-sm" value="${course.discountValue}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Видео ID: </td>
                                <td>
                                    <input id="videoIdEditCourseJsp" name="videoId" type="text" class="form-control input-sm" value="${course.videoId}" placeholder="Видео ID..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ашылу уақыты: </td>
                                <td>
                                    <input id="startDateTimeEditCourseJsp" name="startDateTime" type="datetime-local" class="form-control input-sm" value="${course.startDateTime}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 1: </td>
                                <td>
                                    <input id="feature1EditCourseJsp" name="feature1" type="text" class="form-control input-sm" value="${course.feature1}" placeholder="1 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 2: </td>
                                <td>
                                    <input id="feature2EditCourseJsp" name="feature2" type="text" class="form-control input-sm" value="${course.feature2}" placeholder="2 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 3: </td>
                                <td>
                                    <input id="feature3EditCourseJsp" name="feature3" type="text" class="form-control input-sm" value="${course.feature3}" placeholder="3 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 4: </td>
                                <td>
                                    <input id="feature4EditCourseJsp" name="feature4" type="text" class="form-control input-sm" value="${course.feature4}" placeholder="4 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 5: </td>
                                <td>
                                    <input id="feature5EditCourseJsp" name="feature5" type="text" class="form-control input-sm" value="${course.feature5}" placeholder="5 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 6: </td>
                                <td>
                                    <input id="feature6EditCourseJsp" name="feature6" type="text" class="form-control input-sm" value="${course.feature6}" placeholder="6 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 7: </td>
                                <td>
                                    <input id="feature7EditCourseJsp" name="feature7" type="text" class="form-control input-sm" value="${course.feature7}" placeholder="7 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 8: </td>
                                <td>
                                    <input id="feature8EditCourseJsp" name="feature8" type="text" class="form-control input-sm" value="${course.feature8}" placeholder="8 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 9: </td>
                                <td>
                                    <input id="feature9EditCourseJsp" name="feature9" type="text" class="form-control input-sm" value="${course.feature9}" placeholder="9 - ерекшелік..." />
                                </td>
                            </tr>
                            <tr>
                                <td>Ерекшелік 10: </td>
                                <td>
                                    <input id="feature10EditCourseJsp" name="feature10" type="text" class="form-control input-sm" value="${course.feature10}" placeholder="10 - ерекшелік..." />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return saveEditCourseJsp();'><i class="fas fa-save"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return cancelEditCourseJsp();'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                </tr>
            </table>
        </div>
        <%@ include file="../footer.jsp" %>
        <script>
            function cancelEditCourseJsp() {
                window.location.replace("${pageContext.request.contextPath}/course/view-all");
            }
            
            function saveEditCourseJsp() {
                var id = "${course.id}";
                var categoryId = document.getElementById('categoryEditCourseJsp').value;
                var grade = document.getElementById('gradeEditCourseJsp').value;
                var subject = document.getElementById('subjectEditCourseJsp').value;
                var language = document.getElementById('languageEditCourseJsp').value;
                var description = document.getElementById('descriptionEditCourseJsp').value;
                var price = document.getElementById('priceEditCourseJsp').value;
                var discountTitle = document.getElementById('discountTitleEditCourseJsp').value;
                var discountValue = document.getElementById('discountValueEditCourseJsp').value;
                var videoId = document.getElementById('videoIdEditCourseJsp').value;
                var startDateTime = document.getElementById('startDateTimeEditCourseJsp').value;
                var feature1 = document.getElementById('feature1EditCourseJsp').value;
                var feature2 = document.getElementById('feature2EditCourseJsp').value;
                var feature3 = document.getElementById('feature3EditCourseJsp').value;
                var feature4 = document.getElementById('feature4EditCourseJsp').value;
                var feature5 = document.getElementById('feature5EditCourseJsp').value;
                var feature6 = document.getElementById('feature6EditCourseJsp').value;
                var feature7 = document.getElementById('feature7EditCourseJsp').value;
                var feature8 = document.getElementById('feature8EditCourseJsp').value;
                var feature9 = document.getElementById('feature9EditCourseJsp').value;
                var feature10 = document.getElementById('feature10EditCourseJsp').value;
                var priority = document.getElementById('priorityEditCourseJsp').value;
                var username = "${username}";
                
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {
                    id,
                    categoryId,
                    description,
                    grade,
                    subject,
                    language,
                    price,
                    discountTitle,
                    discountValue,
                    videoId,
                    feature1,
                    feature2,
                    feature3,
                    feature4,
                    feature5,
                    feature6,
                    feature7,
                    feature8,
                    feature9,
                    feature10,
                    priority,
                    username
                }
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                }
                
                fetch('${pageContext.request.contextPath}/course/edit-course?startDateTime=' + startDateTime, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        window.location.replace("${pageContext.request.contextPath}/course/view-all");
                    });
            }
            
            function getAllCategoriesEditCourseJsp() {
                var category = document.getElementById('categoryEditCourseJsp');
                fetch('${pageContext.request.contextPath}/course/get-all-categories/')
                    .then(response => response.json())
                    .then(data => {
                        for(var i = 0; i < data.length; i++) {
                            var option = document.createElement("option");
                            option.id = "category" + data[i].id + "EditCourseJsp";
                            option.value = data[i].id;
                            option.text = data[i].title;
                            category.appendChild(option);
                        }
                        document.getElementById("category${course.categoryId}EditCourseJsp").selected = "true";
                    });
            }
            getAllCategoriesEditCourseJsp();
            
            document.getElementById("language${course.language}EditCourseJsp").selected = "true";
            document.getElementById("grade${course.grade}EditCourseJsp").selected = "true";
        </script>
    </body>
</html>