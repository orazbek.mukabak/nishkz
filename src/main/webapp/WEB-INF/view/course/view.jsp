<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<%@ include file="../header.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Курсты көру</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"/>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    </head>
    <body>
        <%@ include file="../contactus.jsp" %>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb custom-breadcrumb">
                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/">Бас бет</a></li>
                <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/course/view-all">Курстар</a></li>
                <li class="breadcrumb-item" aria-current="page">${course.subject}</li>
            </ol>
        </nav>
        <div class='outer-wrapper-main-view-course'>
            <div class='inner-wrapper-main-view-course'>
                <div class='left-part-main-view-course' data-aos="fade-right">
                    <h2>${course.grade}-<span id='class_view_course'>сынып</span></h2>
                    <h1>${course.subject}</h1>
                    <p>${course.description}</p>
                    <div id='course_price_viewJsp'>
                    </div>
                    <div class='buttons-view-course'>
                        <div id='contact_us' class='buy-btn-view-course'>
                            <div class='icon-buy-lesson-view-course'>
                                <img src='${pageContext.request.contextPath}/resources/images/website/buy.svg'>
                            </div>
                            <span id='buy_course_view_course'>Курсқа жазылу</span>
                        </div>
                        <div class='start-lesson-btn-view-course' onclick='return window.location = "${pageContext.request.contextPath}/course/view-special/${course.id}";'>
                            <div class='icon-start-lesson-view-course'>
                                <img src='${pageContext.request.contextPath}/resources/images/website/play.svg'>
                            </div>
                            <span id="start_course_view_course">Сабақты бастау</span>
                        </div>
                    </div>
                </div>

                <div class='right-part-main-view-course' data-aos="fade-up">
                    <div class='wrapper-course-image-view-course'>
                        <img src="https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fcourses%2F${course.id}.jpg?alt=media"/>
                    </div>
                </div>
            </div>
        </div>

        <div class='outer-wrapper-intro-video-view-course'>
            <div class='inner-wrapper-intro-video-view-course'>
                <div class='wrapper-intro-video-view-course' data-aos="fade-right">
                    <iframe src="//player.vimeo.com/video/${course.videoId}?&autoplay=1&title=0&byline=0" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class='title-intro-video-view-course' data-aos="fade-up">
                    <span id='intro_to_course_view_course'>Курсқа кіріспе</span>
                </div>
            </div>
        </div>


        <div class='outer-wrapper-features-view-course'>
            <div class='inner-wrapper-features-view-course' id='course_features_view'>
            </div>
        </div>

        <div class='outer-wrapper-lessons-view-course'>
            <div class='inner-wrapper-lessons-view-course' data-aos="fade-up">
                <div class='title-lessons-view-course'>
                    <span id='lessons_of_course_view_course'>Осы курстағы тақырыптар</span>
                </div>
                <div class='lessons-view-course' id='lessons_view_course'>
                </div>
            </div>
        </div>

        <%@ include file="../footer.jsp" %>
        <script>

            const lanKazViewCourse = {
                "class": "сынып",
                "price": "Бағасы",
                "buyCourse": "Курсқа жазылу",
                "startLesson": "Сабақты бастау",
                "introToCourse": "Курсқа кіріспе",
                "lessonInThisCourse": "Осы курстағы тақырыптар"
            }

            const lanRuViewCourse = {
                "class": "класс",
                "price": "Цена",
                "buyCourse": "Записаться на курс",
                "startLesson": "Начать урок",
                "introToCourse": "Введение в курс",
                "lessonInThisCourse": "Темы в этом курсе"
            }

            let currentLangViewCourse = lanRuViewCourse;

            if (getLanguage() === 'kaz') {
              currentLangViewCourse = lanKazViewCourse;
            }

            document.querySelectorAll("#class_view_course")[0].innerHTML = currentLangViewCourse.class;
            document.querySelectorAll("#buy_course_view_course")[0].innerHTML = currentLangViewCourse.buyCourse;
            document.querySelectorAll("#start_course_view_course")[0].innerHTML = currentLangViewCourse.startLesson;
            document.querySelectorAll("#intro_to_course_view_course")[0].innerHTML = currentLangViewCourse.introToCourse;
            document.querySelectorAll("#lessons_of_course_view_course")[0].innerHTML = currentLangViewCourse.lessonInThisCourse;


            

            if ("${status}" === 'STATUS.PAIDUSER') {
                document.querySelectorAll("#contact_us")[0].style.display = 'none';
                document.querySelectorAll("#course_price_viewJsp")[0].style.display = 'none';
            }

            
            function addCoursePrice_ViewJsp () {

                let tempHtml = "";

                if ("${course.discountValue}" == 0) {
                    tempHtml += "<div class='price-main-view-course'>";
                        tempHtml += "<span id='price_view_course'>";
                            tempHtml += currentLangViewCourse.price + " - ";
                        tempHtml += "</span>";
                        tempHtml += "<span>"
                            tempHtml += "${course.price}";
                        tempHtml += "</span>";
                    tempHtml += "</div>";
                } else {
                    let coursePrice = "${course.price}" * (100 - "${course.discountValue}") / 100;

                    tempHtml += "<div class='price-main-view-course'>";
                        tempHtml += "<span id='price_view_course'>";
                            tempHtml += currentLangViewCourse.price + " - ";
                        tempHtml += "</span>";
                        tempHtml += "<span>"
                            tempHtml += coursePrice;
                        tempHtml += "</span>";
                        tempHtml += "<span class='price-crossed-main-course'>"
                            tempHtml += "${course.price}";
                        tempHtml += "</span>";
                    tempHtml += "</div>";
                }


                document.getElementById('course_price_viewJsp').innerHTML = tempHtml;
            }

            function addCourseFeatures_ViewJsp () {
                let courseFeatures = [];
                let tempHtml = "";
                
                courseFeatures.push("${course.feature1}");
                courseFeatures.push("${course.feature2}");
                courseFeatures.push("${course.feature3}");
                courseFeatures.push("${course.feature4}");
                courseFeatures.push("${course.feature5}");
                courseFeatures.push("${course.feature6}");
                courseFeatures.push("${course.feature7}");
                courseFeatures.push("${course.feature8}");
                courseFeatures.push("${course.feature9}");
                courseFeatures.push("${course.feature10}");
                
                
                for (let i=0; i < courseFeatures.length; i++){
                    if (courseFeatures[i].length > 0) {
                        tempHtml += "<div class='feature-view-course'>";
                            tempHtml += "<img src='${pageContext.request.contextPath}/resources/images/website/tick.svg'/>"
                            tempHtml += "<div>";
                                tempHtml += courseFeatures[i];
                            tempHtml += "</div>";
                        tempHtml += "</div>";
                    }
                }
                
                document.getElementById('course_features_view').innerHTML = tempHtml;
            }


            function getLessonsByCourseIdViewPage(courseId) {
                fetch('${pageContext.request.contextPath}/course/get-lessons-by-course-id/?id=' + courseId)
                    .then(response => response.json())
                    .then(data => {
                        let varHtml = "";
                        for(var i = 0; i < data.length; i++) {
                            varHtml += "<div class='lesson-item-course-view'>" + data[i].title + "</div>";
                        }
                        document.getElementById('lessons_view_course').innerHTML = varHtml;
                    });
            }


            $( document ).ready(function() {
                addCourseFeatures_ViewJsp();
                getLessonsByCourseIdViewPage("${course.id}");
                addCoursePrice_ViewJsp();
                AOS.init();
            });
        </script>
    </body>
</html>