<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Сабақты өңдеу</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/edit-lesson.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/lib/sceditor/default.min.css" id="theme-style" />
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/sceditor.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/jquery.sceditor.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/monocons.js"></script>
        <script src="${pageContext.request.contextPath}/resources/lib/sceditor/xhtml.js"></script>
    </head>
    <body>
        <br/>
            <div>
            <table class="tableView">
                <tr>
                    <td>
                        <label style="font-size: 12px; font-weight: bolder; font-family: Verdana; color: #24303F;"><i class="fas fa-pen"></i> Сабақты өңдеу:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="tableInnerView">
                            <tr>
                                <td>Сабақтың тақырыбы: </td>
                                <td><input id="titleEditLessonJsp" name="title" type="text" class="form-control input-sm" placeholder="тақырып..." value="${lesson.title}" /></td>
                            </tr>
                            <tr>
                                <td>Live URL: </td>
                                <td><input id="liveUrlEditLessonJsp" name="liveUrl" type="text" class="form-control input-sm" placeholder="Youtube Live URL..." value="${lesson.liveUrl}" /></td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityEditLessonJsp" name="priority" type="number" step="0.01" class="form-control input-sm" value="${lesson.priority}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Ашылу уақыты: </td>
                                <td>
                                    <input id="startDateTimeEditLessonJsp" name="startDateTime" type="datetime-local" class="form-control input-sm" value="${lesson.startDateTime}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Тест тапсырмасы: </td>
                                <td>
                                    <select id="quizAllowedEditLessonJsp" name="quizAllowed" class="form-control input-sm">
                                        <option id="quizAllowedNoEditLessonJsp" value="0" selected>Жоқ, тест тапсырмасы болмайды</option>
                                        <option id="quizAllowedYesEditLessonJsp" value="1">Йә, тест тапсырмасы болады</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Статусы: </td>
                                <td>
                                    <select id="statusEditLessonJsp" name="status" class="form-control input-sm">
                                        <option id="statuscEditLessonJsp" value="c">Құлыпталған</option>
                                        <option id="statusaEditLessonJsp" value="a">Ашық</option>
                                        <option id="statussEditLessonJsp" value="s">Тегін</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea id="summaryEditLessonJsp" name="summary" class="form-control input-sm" style="min-height: 300px; width: 100%;">Сабақтың мазмұны...</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm" style="background-color: #4B2D73; border-color: #4B2D73;" onclick='return saveEditLessonJsp();'><i class="fas fa-save"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm" style="background-color: #4B2D73; border-color: #4B2D73;" onclick='return  window.location.replace("${pageContext.request.contextPath}/course/view-lesson/${lesson.id}");'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                </tr>
            </table>
        </div>
        <script>
            function quizAllowedSelector() {
                let quizAllowedValue = "${lesson.quizAllowed}";
                if(quizAllowedValue === "1") {
                    document.getElementById("quizAllowedYesEditLessonJsp").selected = "true";
                }
            }
            quizAllowedSelector();
            
            document.getElementById('summaryEditLessonJsp').value = decodeURI("${lesson.summary}");
            var varSceditor = document.getElementById('summaryEditLessonJsp');
            sceditor.create(varSceditor, {
                width: "100%",
                    format: 'xhtml',
                    icons: 'monocons',
                    style: '${pageContext.request.contextPath}/resources/lib/sceditor/content.default.min.css'
            });
        
            function saveEditLessonJsp() {
                var id = "${lesson.id}";
                var title = document.getElementById('titleEditLessonJsp').value;
                var liveUrl = document.getElementById('liveUrlEditLessonJsp').value;
                var summary = encodeURI(sceditor.instance(varSceditor).val());
                var priority = document.getElementById('priorityEditLessonJsp').value;
                var startDateTime = document.getElementById('startDateTimeEditLessonJsp').value;
                var quizAllowed = document.getElementById('quizAllowedEditLessonJsp').value;
                var status = document.getElementById('statusEditLessonJsp').value;
                
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {
                    id,
                    title,
                    liveUrl,
                    summary,
                    priority,
                    quizAllowed,
                    status
                }
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                }
                
                fetch('${pageContext.request.contextPath}/course/edit-lesson?startDateTime=' + startDateTime, requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        window.location.replace("${pageContext.request.contextPath}/course/view-lesson/" + id);
                    });
            }
            
            document.getElementById("status${lesson.status}EditLessonJsp").selected = "true";
        </script>
    </body>
</html>