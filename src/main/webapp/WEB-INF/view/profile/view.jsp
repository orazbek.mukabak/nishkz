<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Қолданушы туралы</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/profile.css" />
    </head>
    <body>
        <%@ include file="../header.jsp" %>
        <div class="profile-right-part">
            <h1>Жеке мәліметтерім</h1>
            <div class="profile-main-content-container">
                <div id="profile_user_image_container" class="profile-user-image-container">
                    <img id="imgViewPictureJsp" src="https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fprofile%2F${profile.innerId}.jpg?alt=media&time=<%=System.currentTimeMillis()%>" onerror="this.onerror=null; this.src='https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fprofile%2Ferror.jpg?alt=media'" />

                    <button class="btn edit-picture-text" onclick='return includeContent("${pageContext.request.contextPath}/profile/edit-picture/${profile.username}", "profile_user_image_container");'>
                        Өңдеу
                    </button>
                </div>
                <div id="profile_user_info_container" class="profile-user-info-container">
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Аккаунт №:
                        </div>
                        <div class="profile-user-info-item-value">
                            ${profile.innerId}
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Тегі:
                        </div>
                        <div class="profile-user-info-item-value">
                            ${profile.lastName}
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Аты:
                        </div>
                        <div class="profile-user-info-item-value">
                            ${profile.firstName}
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Ұялы телефон:
                        </div>
                        <div class="profile-user-info-item-value">
                            ${profile.username}
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Электронды пошта:
                        </div>
                        <div class="profile-user-info-item-value">
                            ${profile.email}
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Туылған күні:
                        </div>
                        <div class="profile-user-info-item-value">
                            <fmt:formatDate type="date" value="${profile.birthDate}" />
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Тіркелген күні:
                        </div>
                        <div class="profile-user-info-item-value">
                            <fmt:formatDate type="date" value="${profile.regDate}" />
                        </div>
                    </div>
                    <div class="profile-user-info-item">
                        <div class="profile-user-info-item-name">
                            Өзім туралы:
                        </div>
                        <div class="profile-user-info-item-value">
                            ${profile.aboutMe}
                        </div>
                    </div>
                    <button class="btn edit-info-text" onclick='return includeContent("${pageContext.request.contextPath}/profile/edit-basic-info/${profile.username}", "profile_user_info_container");'>
                        Өңдеу
                    </button>
                </div>
            </div>
        </div>
        <%@ include file="../footer.jsp" %>
        <script>
            function selectGenderSymbol() {
                if(${profile.gender}===true) {
                    document.getElementById("genderViewBasicInfoJsp").src = "${pageContext.request.contextPath}/resources/images/profile/male.svg";
                }
            };
            //selectGenderSymbol();
        </script>
    </body>
</html>
