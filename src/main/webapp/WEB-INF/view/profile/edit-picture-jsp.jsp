<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/croppie.css" />
        <title>Менің суретім</title>
    </head>
    <body>
        <div class="upload-demo-wrap">
            <div id="upload-demo"></div>
        </div>
        <input class="file file-btn" id="inputPictureEditPictureJsp" type="file" accept="image/*">
        <input type="text" id="innerId" value="${innerId}" style="display: none;"/>
        <button class="btn edit-picture-save" id="btnSavePictureEditPictureJsp"> Сақтау</button>
        

        <script src="${pageContext.request.contextPath}/resources/js/firebase-app.js"></script>
        <script>
            var firebaseConfig = {
                apiKey: "AIzaSyBWHMIqx_yDT7hDiC3fB9D9tgO7M4zlG_w",
                authDomain: "nishkz.firebaseapp.com",
                storageBucket: "nishkz"
            };
            firebase.initializeApp(firebaseConfig);
        </script>
        <script src="${pageContext.request.contextPath}/resources/js/firebase-storage.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>');</script>
        <script src="${pageContext.request.contextPath}/resources/js/croppie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/exif.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/js.upload.catalogue.image.js"></script>
        <script>
            CropImage.init("${innerId}", "${username}");
        </script>
    </body>
</html>