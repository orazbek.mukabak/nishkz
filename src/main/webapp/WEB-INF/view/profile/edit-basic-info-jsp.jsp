<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Қолданушы туралы</title>
    </head>
    <body>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Тегі:
            </div>
            <div class="profile-user-info-item-value">
                <input id="lastNameEditBasicInfoJsp" name="lastName" type="text" class="form-control input-sm" value="${profile.lastName}" />
            </div>
        </div>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Аты:
            </div>
            <div class="profile-user-info-item-value">
                <input id="firstNameEditBasicInfoJsp" name="firstName" type="text" class="form-control input-sm" value="${profile.firstName}" />
            </div>
        </div>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Электронды пошта:
            </div>
            <div class="profile-user-info-item-value">
                <input id="emailEditBasicInfoJsp" name="email" type="email" class="form-control input-sm" value="${profile.email}" />
            </div>
        </div>
<!--         <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Жынысы:
            </div>
            <div class="profile-user-info-item-value">
                <select id="genderEditBasicInfoJsp" name="gender" class="form-control input-sm">
                    <option value="0" selected>Әйел</option>
                    <option value="1">Ер</option>
                </select>
            </div>
        </div> -->
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Туылған күні:
            </div>
            <div class="profile-user-info-item-value">
                <input id="birthDateEditBasicInfoJsp" name="birthDate" type="date" class="form-control input-sm" value="<fmt:formatDate pattern='yyyy-MM-dd' value='${profile.birthDate}' />">
            </div>
        </div>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Өзім туралы:
            </div>
            <div class="profile-user-info-item-value">
                <textarea id="aboutMeEditBasicInfoJsp" name="aboutMe" class="form-control input-sm" rows="5" placeholder="Өзің туралы қысқаша... хоббиың, ерекшелігің т.б." maxlength="400">${profile.aboutMe}</textarea>
            </div>
        </div>
        <button class="btn edit-info-save" onclick='return saveBasicInfoEditBasicInfoJsp();'>Сақтау</button>
        <script>
            function saveBasicInfoEditBasicInfoJsp() {
                var lastName = document.getElementById("lastNameEditBasicInfoJsp").value;
                var firstName = document.getElementById("firstNameEditBasicInfoJsp").value;
                /*var gender = document.getElementById("genderEditBasicInfoJsp").value;*/
                var gender = true;
                var email = document.getElementById("emailEditBasicInfoJsp").value;
                var birthDate = document.getElementById("birthDateEditBasicInfoJsp").value;
                var aboutMe = document.getElementById("aboutMeEditBasicInfoJsp").value;
                var username = "${profile.username}";
                
                fetch("${pageContext.request.contextPath}/profile/save-basic-info/" + username + "?lastName=" + lastName + "&firstName=" + firstName + "&gender=" + gender + "&birthDate=" + birthDate + "&email=" + email + "&aboutMe=" + aboutMe)
                    .then(data => {
                        window.location.replace("${pageContext.request.contextPath}/profile/my-profile");
                    });
              
            }
/*            function selectGenderBox() {
                if(${profile.gender}===true) {
                    document.getElementById("genderEditBasicInfoJsp").selectedIndex = 1;
                }
            };
            selectGenderBox();*/
        </script>
    </body>
</html>