<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Footer</title>
</head>
<body>
<!-- Footer -->
<footer class="page-footer font-small unique-color-dark">

  <div style="background-color: #6351ce;"  data-aos="fade-right">
    <div class="container">

      <!-- Grid row-->
      <div class="row py-4 d-flex align-items-center">

        <!-- Grid column -->
        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
          <h6 class="mb-0" id='social_main_text_footer'>Әлеуметтік желіде бізге қосылыңыз!</h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-6 col-lg-7 text-center text-md-right">

          <!-- Facebook 
          <a class="fb-ic">
            <i class="fab fa-facebook-f white-text mr-4"> </i>
          </a>
           Twitter 
          <a class="tw-ic">
            <i class="fab fa-twitter white-text mr-4"> </i>
          </a>
           Google +
          <a class="gplus-ic">
            <i class="fab fa-google-plus-g white-text mr-4"> </i>
          </a>
          Linkedin 
          <a class="li-ic">
            <i class="fab fa-linkedin-in white-text mr-4"> </i>
          </a>
          Instagram
          -->
          <a class="ins-ic">
            <i class="fab fa-instagram white-text"> </i>
          </a>
        

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row-->

    </div>
  </div>

  <!-- Footer Links -->
  <div class="container text-center text-md-left mt-5" data-aos="fade-up">

    <!-- Grid row -->
    <div class="row mt-3">

      <!-- Grid column -->
      <div class="col-md-4 col-lg-5 col-xl-4 mx-auto mb-5">

        <!-- Content -->
        <h6 class="text-uppercase font-weight-bold">Sirius Education</h6>
        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p id='about_platform_footer'>Nish.kz - оқушыларды Назарбаев Зияткерлік Мектептеріне түсуге дайындауға арналған арнайы платформа.</p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-5 col-lg-4 col-xl-4 mx-auto mb-md-0 mb-5">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold" id="contact_footer">Байланыс</h6>
        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p id='address_footer'>
          <i class="fas fa-home mr-3"></i>Қазақстан, Нур-Султан қаласы</p>
        <p>
          <i class="fas fa-envelope mr-3"></i>nish.kz.manager@gmail.com</p>
        <p>
          <i class="fas fa-phone mr-3"></i>+7 777 111 85 41</p>
        <p>
          <i class="fas fa-print mr-3"></i>+7 777 111 85 41</p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© Nish.kz, 2020 
  </div>
  <!-- Copyright -->



</footer>
<!-- Footer -->

<script type="text/javascript">
    const lanKazFooter = {
        "socialMain": "Әлеуметтік желіде бізге қосылыңыз!",
        "aboutPlatform": "Nish.kz - оқушыларды Назарбаев Зияткерлік Мектептеріне түсуге дайындауға арналған арнайы платформа.",
        "contact":"Байланыс",
        "address":"Қазақстан Республикасы, Нур-Султан қаласы"

    }

    const lanRuFooter = {
        "socialMain": "Присоединяйтесь к нам в социальных сетях!",
        "aboutPlatform": "Nish.kz - это специальная площадка для подготовки учеников к поступлению в Назарбаев Интеллектуальные Школы.",
        "contact":"Контакты",
        "address":"Республика Казахстан, город Нур-Султан"
    }

    let currentLangFooter = lanRuFooter;

    if (getLanguage() === 'kaz') {
      currentLangFooter = lanKazFooter;
    }

    document.querySelectorAll("#social_main_text_footer")[0].innerHTML = currentLangFooter.socialMain;
    document.querySelectorAll("#about_platform_footer")[0].innerHTML = currentLangFooter.aboutPlatform;
    document.querySelectorAll("#contact_footer")[0].innerHTML = currentLangFooter.contact;
    document.querySelectorAll("#address_footer")[0].innerHTML = currentLangFooter.address;

  </script>
</body>
</html>