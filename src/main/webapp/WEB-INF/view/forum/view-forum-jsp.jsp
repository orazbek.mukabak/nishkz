<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Талқылау</title>
        <link rel="stylesheet" href="resources/css/view-forum.css" />      
    </head>
    <body>
        <div class="forum-outer-div">
            <div class="forum-wrapper" id="forum_wrapper">
                <img class="loading-image-forum" src="resources/images/website/loading.gif"/>
            </div>
            <div class="forum-input-wrapper" id="forum_input_wrapper">
                <textarea id="forum_input_text" rows = "3" name = "forum-input-text" placeholder="Жаңа сұрақ қою..."></textarea>
                <div id="forum_comment_send_btn" onclick="saveTopicViewForumJsp('${pageContext.request.contextPath}/forum/add-topic', '${username}', '${lesson.id}')">Жіберу</div>
            </div>
        </div>
    </body>
    <script>
        function drawSingleComment(comment){
            let htmlStr = "";
            htmlStr += "<div class='comment-block' id='comment_block_"+comment.id+"'>";
                htmlStr += "<div class='comment-left-part'>";
                    htmlStr += "<img src='https://firebasestorage.googleapis.com/v0/b/school-online-2020/o/images%2Fprofile%2F"+comment.innerId+".png?alt=media' onerror=\"this.onerror=null; this.src='https://firebasestorage.googleapis.com/v0/b/school-online-2020/o/images%2Fprofile%2Fprofile_error_50.png?alt=media'\" />";
                htmlStr += "</div>";
                htmlStr += "<div class='comment-right-part'>";
                    htmlStr += "<h4>";
                        htmlStr += comment.firstName + " " + comment.lastName;
                    htmlStr += "</h4>";
                    htmlStr += "<h6>";
                        if (comment.createdDate.date){
                            htmlStr += comment.createdDate.date.year+"-"+comment.createdDate.date.month+"-"+comment.createdDate.date.day+" "+comment.createdDate.time.hour+":"+comment.createdDate.time.minute+":"+comment.createdDate.time.second;
                        } else {
                            htmlStr += comment.createdDate.year+"-"+comment.createdDate.monthValue+"-"+comment.createdDate.dayOfMonth+" "+comment.createdDate.hour+":"+comment.createdDate.minute+":"+comment.createdDate.second;
                        }
                    htmlStr += "</h6>";
                    htmlStr += "<p>";
                        htmlStr += comment.text;
                    htmlStr += "</p>";
                    
                    if (comment.replyCounter > 0) {
                        htmlStr += "<h5 onclick='getRepliesViewForumJsp("+comment.id+")'>";
                            htmlStr += "- " + comment.replyCounter + " жауапты көру";
                        htmlStr += "</h5>";
                    }
                    <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                        htmlStr += "<div class='delete-comment-div' onclick='deleteTopicViewForumJsp("+comment.id+")'>";
                            htmlStr += "×";
                        htmlStr += "</div>";
                    </sec:authorize>
                    
                htmlStr += "</div>";
            htmlStr += "</div>";

            return htmlStr;
        }

        function drawSingleReply(reply){
            let htmlStr = "";
            htmlStr += "<div class='reply-block'>";
                htmlStr += "<div class='reply-left-part'>";
                    htmlStr += "<img src='https://firebasestorage.googleapis.com/v0/b/school-online-2020/o/images%2Fprofile%2F"+reply.innerId+".png?alt=media' onerror=\"this.onerror=null; this.src='https://firebasestorage.googleapis.com/v0/b/school-online-2020/o/images%2Fprofile%2Fprofile_error_50.png?alt=media'\" />";
                htmlStr += "</div>";
                htmlStr += "<div class='reply-right-part'>";
                    htmlStr += "<h4>";
                        htmlStr += reply.firstName + " " + reply.lastName +":";
                    htmlStr += "</h4>";
                    htmlStr += "<p>";
                        htmlStr += reply.text;
                    htmlStr += "</p>";
                htmlStr += "</div>";
            htmlStr += "</div>";

            return htmlStr;
        } 

        function drawComments(comments) {
            let htmlStr = "";
            for (let i=0; i < comments.length; i++){
                htmlStr += drawSingleComment(comments[i]);
            }

            document.getElementById('forum_wrapper').innerHTML = htmlStr;
        }

        function drawReplies(replies, topicid) {
            let htmlStr = "";
            for (let i=0; i < replies.length; i++){
                htmlStr += drawSingleReply(replies[i]);
            }

            const currentCommentBlock = document.getElementById('comment_block_'+topicid);
            currentCommentBlock.getElementsByTagName('h5')[0].innerHTML = "";
            const p = currentCommentBlock.getElementsByTagName('p')[0];
            p.insertAdjacentHTML("afterend", htmlStr);
        }

         function saveTopicViewForumJsp(url, username, lessonId) {
            const text = document.getElementById('forum_input_text').value;

            
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            
            const raw = {
                text,
                lessonId,
                username
            }
            
            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(raw)
            }
            
            fetch(url, requestOptions)
                .then(response => response.text())
                .then(result => {
                    includeContent('forum/display-view-forum/?lessonId='+lessonId, 'lesson-content-view-course-jsp');
                })
                .catch(error => console.log('error', error));
        }

        function deleteTopicViewForumJsp(id) {
            Swal.fire({
                title: 'Осы пікірді жойғыңыз келеді ма?',
                text: "Осы пікір түбегейлі жойылатын болады!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4B2D73',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Йә',
                cancelButtonText: 'Жоқ'
            }).then((result) => {
                if(result.value) {
                    fetch('${pageContext.request.contextPath}/forum/delete-topic/' + id)
                        .then(function(response) {
                            return response.text();
                        })
                        .then(function(text) {
                            if(text.includes('STATUS.DELETED')) {
                                Swal.fire(
                                    'Жойылды!',
                                    'Пікір сәтті жойылды.',
                                    'success'
                                );
                                deleteCommentBlock(id);
                            }
                        });
                }
            });

        }

        function deleteCommentBlock(id) {
            $( '#comment_block_'+id ).remove();
        }
        
        function deleteReplyViewForumJsp(id) {
            fetch('${pageContext.request.contextPath}/forum/delete-reply/' + id)
            .then(response => {
                includeContent('forum/display-view-forum/?lessonId=${lesson.id}', 'lesson-content-view-course-jsp');
            });
        }
            
        function getTopicsViewForumJsp() {
            fetch("${pageContext.request.contextPath}/forum/get-topics-by-lesson-id/?lessonId=${lesson.id}")
                .then(response => response.json())
                .then(data => {
                    drawComments(data);
                });
        }

        function getRepliesViewForumJsp(topicId) {
            fetch("${pageContext.request.contextPath}/forum/get-replies-by-topic-id/?topicId="+topicId)
                .then(response => response.json())
                .then(data => {
                    drawReplies(data, topicId);
                });
        }
        getTopicsViewForumJsp();
        
        function updateNamesViewForumJsp(result) {
            var firstName = "";
            var lastName = "";
            if(result[0].length > 0) {
                lastName = result[0];
            } else {
                lastName = "Белгісіз";
            }
            if(result[1].length > 0) {
                firstName = result[1];
            } else {
                firstName = "Белгісіз";
            }
            fetch('${pageContext.request.contextPath}/profile/update-names/${username}?firstName=' + firstName + '&lastName=' + lastName)
                //.then(response => response.json())
                .then(response => {
                    includeContent('forum/display-view-forum/?lessonId=${lesson.id}', 'lesson-content-view-course-jsp');
                });
        }
        
        function checkProfileViewForumJsp() {
            const firstName = "${profile.firstName}";
            const lastName = "${profile.lastName}";
            if(firstName.length === 0 || lastName.length === 0) {
                Swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Келесі',
                    progressSteps: ['1', '2']
                }).queue([
                    {
                      title: 'Тегіңізді жазыңыз:'
                    },
                    'Атыңызды жазыңыз:'
                ]).then((result) => {
                    if(result.value) {
                        Swal.fire({
                        title: 'Сәтті өңделді!',
                        timer: 1500
                      });
                      updateNamesViewForumJsp(result.value);
                    }
                });
            }
        }
        checkProfileViewForumJsp();
    </script>
</html>
