<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Тіркелу беті...</title>
    <link id="dynamic-favicon" rel="shortcut icon" href="../resources/images/website/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="../resources/css/signup.css" />
    <link rel="stylesheet" href="../resources/css/main.css" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
</head>
<body>
    <div class="register-header-signup-jsp">
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">
            <img src='../resources/images/website/logo.svg'/>
        </a>
        <sec:authorize access="!isAuthenticated()" style="display: flex;width: 100%">
        <a class="signin-btn-header"  href="${pageContext.request.contextPath}/signin?u=null&p=null">Кіру</a>
        <a class="signup-btn-header"  href="${pageContext.request.contextPath}/register/signup">Тіркелу</a>
    </sec:authorize>
    </div>
    <div class="register-page-signup-jsp">
        <div class="register-wrap-signup-jsp">
            <div class="register-title-signup-jsp">
                Құпия Сөзді Қалпына Келтіру!
            </div>
            <div class="register-content-signup-jsp">
                <form:form action="${pageContext.request.contextPath}/register/recoverying" modelAttribute="authUser" onsubmit="return checkPasswordRecoveryJsp(this);">
                    <div>
                        <c:if test="${registrationError != null}">
                            <script>
                                var error = '${registrationError}';
                                if(error === 'ERROR_USER_DO_NOT_EXISTS') {
                                    error = 'Кешіріңіз, телефон нөміріңіз осы сайтта тіркелген.';
                                } else if(error === 'ERROR_EMPTY_USERNAME') {
                                    error = 'Кешіріңіз, телефон нөміріңізді жазыңыз.';
                                } else if(error === 'ERROR_EMPTY_PASSWORD') {
                                    error = 'Кешіріңіз, жаңа құпия сөзді жазыңыз.';
                                } else {
                                    error = 'Кешіріңіз, жаңа құпия сөз 6-32 ұзындығында болуы керек.';
                                }
                                Swal.fire({
                                    title: 'Қайта тырысып көріңіз...',
                                    text: error,
                                    confirmButtonColor: '#4B2D73'
                                });
                            </script>
                        </c:if>                                         
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-user"></i>
                        <form:input id="register_phone_signup_jsp" path="userName" placeholder="Телефон" class="form-control" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password_recovery_jsp" path="password" placeholder="Жаңа құпия сөз (минимум 6 орын)" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password2_recovery_jsp" path="" placeholder="Жаңа құпия сөзді қайталау" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <button type="submit" class="btn register-btn-signup-jsp">SMS-код жіберу</button>
                </form:form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/lib/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        $(function(){
          $("#register_phone_signup_jsp").mask("8 (999) 999-99-99");
        });
        
        function checkPasswordRecoveryJsp(form) {
          if(form.password_recovery_jsp.value !== form.password2_recovery_jsp.value) {
                Swal.fire({
                    title: 'Қайта тырысып көріңіз...',
                    text: 'Құпия сөз дұрыс жазылмады.',
                    confirmButtonColor: '#4B2D73'
                });
              form.password_recovery_jsp.focus();
              return false;
            }
          return true;
        }
    </script>
</body>
</html>