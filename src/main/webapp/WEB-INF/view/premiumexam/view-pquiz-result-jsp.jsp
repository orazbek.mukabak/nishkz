<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестілеу жүйесін көру</title>
        <link rel="stylesheet" href="resources/css/view-pquiz.css" />
    </head>
    <body>
        <div id="divViewPquizJsp">
            <div id='pquiz_left_div' class='pquiz-left-div'>
            </div>
            <div id='pquiz_inner_div' class='pquiz-inner-div'>
            </div>
            <div id='pquiz_boxes_div' class='pquiz-boxes-div'>
            </div>
        </div>

        <script>
            function deletePParticipantViewPQuizResultJsp() {
                Swal.fire({
                    title: 'Осы тесттен алған бағаңызды жойғыңыз келеді ма?',
                    text: "Осы тесттен алған бағаңыз түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/premiumexam/delete-pparticipant/${pParticipant.id}')
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Алған бағаңыз сәтті жойылды.',
                                    'success'
                                );
                                includeContent('premiumexam/view-pexam/${pQuiz.pExam.id}', 'container_fluid');
                            });
                    }
                });
            }
            
            var pQuizState = {
                currentQuestion: 1,
                pQuizLength:0,
                data: null,
            }


            function showNextPQuiz() {
                if (pQuizState.currentQuestion < pQuizState.pQuizLength){
                    pQuizState.currentQuestion += 1;
                } else {
                    pQuizState.currentQuestion = 1;
                }
                drawQuestionsViewPQuizJsp(pQuizState.data[pQuizState.currentQuestion - 1]);
            }

            function showPreviousPQuiz() {
                if (pQuizState.currentQuestion > 1){
                    pQuizState.currentQuestion -= 1;
                } else {
                    pQuizState.currentQuestion = pQuizState.pQuizLength;
                }
                drawQuestionsViewPQuizJsp(pQuizState.data[pQuizState.currentQuestion - 1]);
            }

            function drawQuestionsViewPQuizJsp(dataItem) {
                let htmlStr = "";
                    htmlStr += drawQuestionHeaderViewPQuizJsp(dataItem);
                    htmlStr += drawAnswersViewPQuizJsp(dataItem);
                    htmlStr += "<div class='pquiz-previous-next-btn-wrap'>";
                        htmlStr += "<div class='pquiz-prev-btn' onclick='showPreviousPQuiz()'>Арытқа</div>";
                        htmlStr += "<div class='pquiz-next-btn' onclick='showNextPQuiz()'>Келесі</div>";
                    htmlStr += "</div>";

                document.getElementById('pquiz_inner_div').innerHTML = htmlStr;
                document.getElementById('pquiz_boxes_div').innerHTML = drawBoxesViewPQuizJsp();
                MathJax.Hub.Queue(["Typeset",MathJax.Hub,"pquiz-inner-div"]);
            }

            function drawLeftPartViewPQuizJsp(data) {
                let htmlStr = "";
                htmlStr += "<div class='pquiz-about'>";
                htmlStr += "<div class='pquiz-pExam-title'>${pQuiz.pExam.title}</div>";
                htmlStr += "<div class='pquiz-pExam-description'>${pQuiz.pExam.description}</div>";
                htmlStr += "<div class='pquiz-priority'>Нұсқа: ${pQuiz.priority}</div>";
                htmlStr += "<div class='pquiz-duration'>Уақыты: ${pQuiz.duration} мин.</div>";
                htmlStr += "</div>";
                htmlStr += "<div class='pquiz-submit-btn' onclick='deletePParticipantViewPQuizResultJsp();'>Нәтижені жою</div>";
                htmlStr += drawScoreViewPQuizJsp(data);
                document.getElementById('pquiz_left_div').innerHTML = htmlStr;
            }

            function drawScoreViewPQuizJsp(data) {
                let score = 0;
                for (let i=0; i<data.length; i++){
                    score += data[i].score;
                }

                let htmlStr = "";
                    htmlStr += "<div class='pquiz-score-wrap'>";
                        htmlStr += "<div class='pquiz-score'>Дұрыс жауаптар саны: "+score+"</div>";
                    htmlStr += "</div>";

                return htmlStr;
            }

            
            function drawQuestionHeaderViewPQuizJsp(questionData) {

                const className = questionData.score === 1 ? "pquiz-result-header-correct" : "pquiz-result-header-wrong";
                let htmlStr = "";
                    
                    htmlStr += "<div class='"+className+"'>";
                        htmlStr += pQuizState.currentQuestion + "-сұрақ";
                    htmlStr += "</div>";
                    
                    htmlStr += "<div class='pquiz-question-description'>";
                        htmlStr += "<h3>";
                            htmlStr += questionData.text;
                        htmlStr += "</h3>";
                    htmlStr += "</div>";

                return htmlStr;
            }

            function drawAnswersViewPQuizJsp(questionData) {
                let htmlStr = "";

                htmlStr += "<div class='pquiz-question-answers'>";
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerA, "a", questionData);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerB, "b", questionData);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerC, "c", questionData);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerD, "d", questionData);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerE, "e", questionData);
                    htmlStr += "</div>";
                return htmlStr;
            }

            function drawSingleAnswerPQuizJsp(answerData, value, questionData) {
                const checked = (questionData.studentAnswer === value) ? "checked" : "";
                const className = (questionData.correctAnswer === value) ? "pquiz-result-answer-item pquiz-correct-answer-item" : "pquiz-result-answer-item";
                let htmlStr = "";
                    htmlStr += "<div class='"+className+"'>";
                        htmlStr += "<input type='radio' name='answer' disabled "+checked+">";
                        htmlStr += answerData;
                    htmlStr += "</div>";

                return htmlStr;
            }


            function drawBoxesViewPQuizJsp() {
                let htmlStr = "";
                for (let i=0; i < pQuizState.data.length; i++) {
                    htmlStr += drawBoxItemViewPQuizJsp(i);
                }

                return htmlStr;
            }

            function drawBoxItemViewPQuizJsp(index) {

                const pQuestionId = pQuizState.data[index].id;

                let className = 'pquiz-box-item';

                if (index === pQuizState.currentQuestion - 1){
                    className += ' pquiz-box-item-active';
                }

                if (pQuizState.data[index].score === 1){
                    className += ' pquiz-box-item-correct';
                } else {
                    className += ' pquiz-box-item-wrong';
                }



                let htmlStr = "";
                    htmlStr += "<div onclick='jumpToSelectedPQuiz("+index+")' class='"+className+"'>";
                        htmlStr += index+1;
                    htmlStr += "</div>";

                return htmlStr;
            }

            function jumpToSelectedPQuiz(index){
                pQuizState.currentQuestion = index + 1;
                drawQuestionsViewPQuizJsp(pQuizState.data[pQuizState.currentQuestion - 1]);
            }

            function getAllPQuestionsViewPQuizJsp() {
                fetch("${pageContext.request.contextPath}/premiumexam/get-results-by-pparticipant-id/?pParticipantId=${pParticipant.id}")
                    .then(response => response.json())
                    .then(data => {
                        if (data.length > 0){
                            pQuizState.pQuizLength = data.length;
                            pQuizState.data = data;
                            drawQuestionsViewPQuizJsp(data[0]);
                            drawLeftPartViewPQuizJsp(data);
                        }
                    });
            }
            getAllPQuestionsViewPQuizJsp();
        </script>
    </body>
</html>
