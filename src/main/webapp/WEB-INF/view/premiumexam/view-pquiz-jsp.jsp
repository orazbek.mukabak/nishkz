<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестілеу жүйесін көру</title>
        <link rel="stylesheet" href="resources/css/view-pquiz.css" />
    </head>
    <body>
        <div id="divViewPquizJsp">
            <div id='pquiz_left_div' class='pquiz-left-div'>
            </div>
            <div id='pquiz_inner_div' class='pquiz-inner-div'>
            </div>
            <div id='pquiz_boxes_div' class='pquiz-boxes-div'>
            </div>
        </div>

        <script>
            function deletePQuestionViewPQuizJsp(id) {
                Swal.fire({
                    title: 'Осы сұрақты жойғыңыз келеді ма?',
                    text: "Осы сұрақ түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/premiumexam/delete-pquestion/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Сұрақ сәтті жойылды.',
                                    'success'
                                );
                                includeContent('premiumexam/view-pquiz/${pQuiz.id}', 'container_fluid');
                            });
                    }
                });
            }

            function addPQuestionViewPQuizJsp() {
                includeContent('premiumexam/display-add-pquestion/${pQuiz.id}', 'pquiz_inner_div');

                let htmlStr = "<div class='pquiz-about'>";
                htmlStr += "<div class='pquiz-pExam-title'>${pQuiz.pExam.title}</div>";
                htmlStr += "<div class='pquiz-pExam-description'>${pQuiz.pExam.description}</div>";
                htmlStr += "<div class='pquiz-priority'>Нұсқа: ${pQuiz.priority}</div>";
                htmlStr += "<div class='pquiz-duration'>Уақыты: ${pQuiz.duration} мин.</div>";
                htmlStr += "</div>";
                document.getElementById('pquiz_left_div').innerHTML = htmlStr;
            }
            
            var pQuizState = {
                currentQuestion: 1,
                pQuizLength:0,
                data: null,
                answers: []
            }

            function getAnswerIndex(idOfQuestion) {
                let answerIndex = -1;
                for (let i=0; i < pQuizState.answers.length; i++){
                    if (pQuizState.answers[i].pQuestionId === idOfQuestion){
                        answerIndex = i;
                        break;
                    }
                }
                return answerIndex;
            }

            function highlightSelectedAnswer(pQuestionId) {

                const answerIndex = getAnswerIndex(pQuestionId);

                if (answerIndex !== -1){
                    const answer = pQuizState.answers[answerIndex].answer;
                    $('#pquiz-question-answer-'+answer+' input[type=radio]').prop("checked", true);
                }
                
            }

            function selectAnswer(answer, idOfQuestion) {
                $('#pquiz-question-answer-'+answer+' input[type=radio]').prop("checked", true);

                const answerIndex = getAnswerIndex(idOfQuestion);
                if (answerIndex === -1){
                    pQuizState.answers.push({pQuestionId: idOfQuestion, answer:answer});
                } else {
                    pQuizState.answers[answerIndex].answer = answer;
                }

                let htmlStr = drawBoxesViewPQuizJsp();

                document.getElementById('pquiz_boxes_div').innerHTML = htmlStr;
            }

            function showNextPQuiz() {
                if (pQuizState.currentQuestion < pQuizState.pQuizLength){
                    pQuizState.currentQuestion += 1;
                } else {
                    pQuizState.currentQuestion = 1;
                }
                drawQuestionsViewPQuizJsp(pQuizState.data[pQuizState.currentQuestion - 1]);
                highlightSelectedAnswer();
            }

            function showPreviousPQuiz() {
                if (pQuizState.currentQuestion > 1){
                    pQuizState.currentQuestion -= 1;
                } else {
                    pQuizState.currentQuestion = pQuizState.pQuizLength;
                }
                drawQuestionsViewPQuizJsp(pQuizState.data[pQuizState.currentQuestion - 1]);
            }

            function drawQuestionsViewPQuizJsp(dataItem) {
                let htmlStr = "";
                <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                    htmlStr += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                    htmlStr += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                    htmlStr += "<a class='dropdown-item' href='javascript: deletePQuestionViewPQuizJsp(" + dataItem.id + ");'><i class='fas fa-trash-alt'></i> Сұрақты жою</a></div></div>";
                </sec:authorize>
                htmlStr += drawQuestionHeaderViewPQuizJsp(dataItem);
                htmlStr += drawAnswersViewPQuizJsp(dataItem);
                htmlStr += "<div class='pquiz-previous-next-btn-wrap'>";
                htmlStr += "<div class='pquiz-prev-btn' onclick='showPreviousPQuiz()'>Алдыңғы</div>";
                htmlStr += "<div class='pquiz-next-btn' onclick='showNextPQuiz()'>Келесі</div>";
                htmlStr += "</div>";

                document.getElementById('pquiz_inner_div').innerHTML = htmlStr;
                document.getElementById('pquiz_boxes_div').innerHTML = drawBoxesViewPQuizJsp();

                highlightSelectedAnswer(dataItem.id);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub,"pquiz-inner-div"]);
            }

            function drawLeftPartViewPQuizJsp() {
                let htmlStr = "";
                htmlStr += "<div class='pquiz-about'>";
                htmlStr += "<div class='pquiz-pExam-title'>${pQuiz.pExam.title}</div>";
                htmlStr += "<div class='pquiz-pExam-description'>${pQuiz.pExam.description}</div>";
                htmlStr += "<div class='pquiz-priority'>Нұсқа: ${pQuiz.priority}</div>";
                htmlStr += "<div class='pquiz-duration'>Уақыты: ${pQuiz.duration} мин.</div>";
                htmlStr += "</div>";
                htmlStr += "<div class='pquiz-submit-btn' onclick='submitPQuiz()'>Тапсыру</div>";
                document.getElementById('pquiz_left_div').innerHTML = htmlStr;
            }
            
            function submitPQuiz() {
                Swal.fire({
                    title: 'Тестілеуді аяқтау...',
                    text: "Тестілеуді аяқтағыңыз келеді ма?",
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Иә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/premiumexam/add-results?pQuizId=${pQuiz.id}&pParticipantId=${pParticipant.id}', {
                            method: 'post',
                            headers: {
                                'Accept': 'application/json, text/plain, */*',
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(pQuizState.answers)
                        })
                        .then(response => response.text())
                        .then(result => {
                            includeContent('premiumexam/view-pquiz-result/${pQuiz.id}', 'container_fluid');
                        });
                    }
                });
            }
            
            function drawQuestionHeaderViewPQuizJsp(questionData) {
                let htmlStr = "";
                    
                    htmlStr += "<div class='pquiz-question-header'>";
                        htmlStr += pQuizState.currentQuestion + "-сұрақ";
                    htmlStr += "</div>";
                    
                    htmlStr += "<div class='pquiz-question-description'>";
                        htmlStr += "<h3>";
                            htmlStr += questionData.text;
                        htmlStr += "</h3>";
                    htmlStr += "</div>";

                return htmlStr;
            }

            function drawAnswersViewPQuizJsp(questionData) {
                let htmlStr = "";

                htmlStr += "<div class='pquiz-question-answers'>";
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerA, "a", questionData.id);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerB, "b", questionData.id);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerC, "c", questionData.id);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerD, "d", questionData.id);
                    htmlStr += drawSingleAnswerPQuizJsp(questionData.answerE, "e", questionData.id);
                    htmlStr += "</div>";
                return htmlStr;
            }

            function drawSingleAnswerPQuizJsp(asnwerData, value, idOfQuestion) {
                let htmlStr = "";
                    htmlStr += "<div class='pquiz-question-answer-item' id='pquiz-question-answer-"+value+"' onclick='return selectAnswer(\""+value+"\","+idOfQuestion+")'>";
                        htmlStr += "<input type='radio' name='answer'>";
                        htmlStr += asnwerData;
                    htmlStr += "</div>";

                return htmlStr;
            }


            function drawBoxesViewPQuizJsp() {
                let htmlStr = "";
                for (let i=0; i < pQuizState.data.length; i++) {
                    htmlStr += drawBoxItemViewPQuizJsp(i);
                }
                htmlStr += "<div onclick='addPQuestionViewPQuizJsp()' class='pquiz-box-item'>+</div>";
                return htmlStr;
            }

            function drawBoxItemViewPQuizJsp(index) {

                const pQuestionId = pQuizState.data[index].id;
                const answerIndex = getAnswerIndex(pQuestionId);

                let className = 'pquiz-box-item';

                if (index === pQuizState.currentQuestion - 1){
                    className += ' pquiz-box-item-active';
                }

                if (answerIndex !== -1){
                    className += ' pquiz-box-item-answered';
                }

                let htmlStr = "";
                    htmlStr += "<div onclick='jumpToSelectedPQuiz("+index+")' class='"+className+"'>";
                        htmlStr += index+1;
                    htmlStr += "</div>";

                return htmlStr;
            }

            function jumpToSelectedPQuiz(index){
                pQuizState.currentQuestion = index + 1;
                drawQuestionsViewPQuizJsp(pQuizState.data[pQuizState.currentQuestion - 1]);
                highlightSelectedAnswer();
            }

            function getAllPQuestionsViewPQuizJsp() {
                fetch("${pageContext.request.contextPath}/premiumexam/get-pquestions-by-pquiz-id/?pquizId=${pQuiz.id}")
                    .then(response => response.json())
                    .then(data => {
                        if (data.length > 0){
                            pQuizState.pQuizLength = data.length;
                            pQuizState.data = data;
                            drawQuestionsViewPQuizJsp(data[0]);
                            drawLeftPartViewPQuizJsp();
                        } else {
                            document.getElementById('pquiz_boxes_div').innerHTML = "<div onclick='addPQuestionViewPQuizJsp()' class='pquiz-box-item'>+</div>";
                        }
                    });
            }
            getAllPQuestionsViewPQuizJsp();
        </script>
    </body>
</html>
