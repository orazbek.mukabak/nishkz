<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Жаңа сұрақ қосу</title>
    </head>
    <body>
        <div>
            <table>
                <tr>
                    <td>
                        <textarea id="textAddPQuestionJsp" class="form-control input-sm" name="text" rows="5" placeholder="Сұрақтың мәтіні..." maxlength="2048"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>A жауабы: </td>
                                <td><input id="answerAAddPQuestionJsp" name="answerA" type="text" class="form-control input-sm" placeholder="A жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>B жауабы: </td>
                                <td><input id="answerBAddPQuestionJsp" name="answerB" type="text" class="form-control input-sm" placeholder="B жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>C жауабы: </td>
                                <td><input id="answerCAddPQuestionJsp" name="answerC" type="text" class="form-control input-sm" placeholder="C жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>D жауабы: </td>
                                <td><input id="answerDAddPQuestionJsp" name="answerD" type="text" class="form-control input-sm" placeholder="D жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>E жауабы: </td>
                                <td><input id="answerEAddPQuestionJsp" name="answerE" type="text" class="form-control input-sm" placeholder="E жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>Дұрыс жауабы: </td>
                                <td>
                                    <select id="correctAnswerAddPQuestionJsp" name="correctAnswer" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Дұрыс жауабын талдаңыз...</option>
                                        <option value="a">A</option>
                                        <option value="b">B</option>
                                        <option value="c">C</option>
                                        <option value="d">D</option>
                                        <option value="e">E</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityAddPQuestionJsp" name="priority" type="number" step="0.01" class="form-control input-sm" value="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Қиындығы: </td>
                                <td>
                                    <select id="levelAddPQuestionJsp" name="level" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Қиындық дәрежесін талдаңыз...</option>
                                        <option value="1">Оңай</option>
                                        <option value="2">Орташа</option>
                                        <option value="3">Қиын</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Шығару видеосы: </td>
                                <td><input id="urlAddPQuestionJsp" name="solution" type="text" class="form-control input-sm" placeholder="Есепті шығау видеосы (URL)..." /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm" style="background-color: #4B2D73; border-color: #4B2D73;" onclick='return saveAddPQuestionJsp();'>Қосу</button>
                    </td>
                </tr>
            </table>
        </div>
        <script>
            function postAddPQuestionJsp(text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, url, videoId) {
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {
                    text,
                    answerA,
                    answerB,
                    answerC,
                    answerD,
                    answerE,
                    correctAnswer,
                    priority,
                    level,
                    url,
                    videoId
                };
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };
                
                fetch('${pageContext.request.contextPath}/premiumexam/add-pquestion?pQuizId=${pQuiz.id}', requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        includeContent('premiumexam/view-pquiz/${pQuiz.id}', 'container_fluid');
                    });
            }
            
            function saveAddPQuestionJsp () {
                var text = document.getElementById('textAddPQuestionJsp').value;
                var answerA = document.getElementById('answerAAddPQuestionJsp').value;
                var answerB = document.getElementById('answerBAddPQuestionJsp').value;
                var answerC = document.getElementById('answerCAddPQuestionJsp').value;
                var answerD = document.getElementById('answerDAddPQuestionJsp').value;
                var answerE = document.getElementById('answerEAddPQuestionJsp').value;
                var correctAnswer = document.getElementById('correctAnswerAddPQuestionJsp').value;
                var priority = document.getElementById('priorityAddPQuestionJsp').value;
                var level = document.getElementById('levelAddPQuestionJsp').value;
                var url = document.getElementById('urlAddPQuestionJsp').value;
                fetch("https://vimeo.com/api/oembed.json?url=" + url)
                    .then(response => response.json())
                    .then(data => {
                        postAddPQuestionJsp(text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, url, data.video_id);
                    });
            }
        </script>
    </body>
</html>