<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестілеу жүйесін көру</title>
        <link rel="stylesheet" href="resources/css/view-pexam.css" />
    </head>
    <body>
        <div id="divViewPexamJsp">
        </div>
        <script>
            function deleteVariantViewPExamJsp(id) {
                Swal.fire({
                    title: 'Осы нұсқаны жойғыңыз келеді ма?',
                    text: "Осы нұсқадағы барлық сұрақтар түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/premiumexam/delete-variant/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Нұсқа сәтті жойылды.',
                                    'success'
                                );
                                includeContent("premiumexam/view-pexam/${pExam.id}", "container_fluid");
                            });
                    }
                });
            }
            
            function postVariantViewPExamJsp(data) {
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                var priority = data[0];
                var duration = data[1];
                const raw = {priority, duration};
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };

                fetch('${pageContext.request.contextPath}/premiumexam/add-variant?pExamId=${pExam.id}', requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        includeContent('premiumexam/view-pexam/${pExam.id}', 'container_fluid');
                    });

            }
        
            function displayAddVariantFormViewPExamJsp() {
                Swal.mixin({
                input: 'text',
                confirmButtonText: 'Келесі',
                showCancelButton: true,
                progressSteps: ['1', '2']
              }).queue([
                {
                  title: 'Тесттің нұсқалық нөмірі:'
                },
                'Тесттің уақыты (минут):'
              ]).then((result) => {
                if (result.value) {
                  Swal.fire("Тест нұсқасы сәтті қосылды!");
                  postVariantViewPExamJsp(result.value);
                }
              });
            }
        
            function startPQuizViewPExamJsp(id) {
                Swal.fire({
                    title: 'Тестілеуді бастау...',
                    text: "Сіз тестілеуді бастағыңыз келеді ма?",
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Иә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch("${pageContext.request.contextPath}/premiumexam/start-pquiz/" + id)
                            .then(function(response) {
                                return response.text();
                            })
                            .then(function(text) {
                                includeContent('premiumexam/view-pquiz/' + id, 'container_fluid');
                            });
                    }
                });
            }
            
            function getAllPQuizzesViewPExamJsp() {
                fetch("${pageContext.request.contextPath}/premiumexam/get-pquizzes-by-pexam-id/?pexamId=${pExam.id}")
                    .then(response => response.json())
                    .then(data => {
                        const pexamPanel = (id, duration, priority, index) => {
                            let temp = "<div class='card-view-pexam'>";
                            <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                                temp += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                                temp += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                                temp += "<a class='dropdown-item' href='javascript: deleteVariantViewPExamJsp(" + id + ");'><i class='fas fa-trash-alt'></i> Нұсқаны жою</a></div></div>";
                            </sec:authorize>
                            temp += "<div class='card-view-pexam-info'>";
                            temp += ++index + " - нұсқа: ${pExam.title}</h5>";
                            temp += "<p>Берілетін уақыт: " + duration + " минут</p>";
                            <sec:authorize access="isAuthenticated()">
                                temp += "<div id='div_pquiz_button_" + id + "'></div>";
                                fetch("${pageContext.request.contextPath}/premiumexam/get-pparticipant-status/?pQuizId=" + id)
                                    .then(function(response) {
                                        return response.text();
                                    })
                                    .then(function(text) {
                                        if(text.includes('STATUS.NOT_FOUND')) {
                                            document.getElementById("div_pquiz_button_" + id).innerHTML = "<a href=\"javascript: startPQuizViewPExamJsp(" + id + ");\" class='btn'>Бастау</a>";
                                        } else if(text.includes('STATUS.NOT_SUBMITTED')) {
                                            document.getElementById("div_pquiz_button_" + id).innerHTML = "<a href=\"javascript: startPQuizViewPExamJsp(" + id + ");\" class='btn'>NOT_SUBMITTED</a>";
                                        } else if(text.includes('STATUS.PROCESSING')) {
                                            document.getElementById("div_pquiz_button_" + id).innerHTML = "<a href=\"javascript: includeContent('premiumexam/view-pquiz/" + id + "', 'container_fluid');\" class='btn'>Жалғастыру</a>";
                                        } else if(text.includes('STATUS.SUBMITTED')) {
                                            document.getElementById("div_pquiz_button_" + id).innerHTML = "<a href=\"javascript: includeContent('premiumexam/view-pquiz-result/" + id + "', 'container_fluid');\" class='btn'>Нәтижені қарау</a>";
                                        } else {
                                            document.getElementById("div_pquiz_button_" + id).innerHTML = "<a href='#' class='btn'>UNKNOWN_ERROR</a>";
                                        }
                                    });
                            </sec:authorize>
                            <sec:authorize access="!isAuthenticated()">
                                temp += "<a href='${pageContext.request.contextPath}/signin?u=null&p=null' class='btn'>Бастау</a>";
                            </sec:authorize>
                            temp += "</div>";
                            temp += "</div>";
                            return temp;
                        };

                        let pexamCardHtml = "";
                        for(var i = 0; i < data.length; i++) {
                            pexamCardHtml += pexamPanel(data[i].id, data[i].duration, data[i].priority, i);
                        }
                        <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                            pexamCardHtml += "<div class='card-view-pexam'>";
                            pexamCardHtml += "<div class='card-view-pexam-info'>";
                            pexamCardHtml += "<h5>Жаңа нұсқа: ${pExam.title}</h5>";
                            pexamCardHtml += "<p>...</p>";
                            pexamCardHtml += "<a href='javascript: displayAddVariantFormViewPExamJsp();' class='btn'>Жаңа нұсқа қосу</a>";
                            pexamCardHtml += "</div></div>";
                        </sec:authorize>
                        document.getElementById('divViewPexamJsp').innerHTML = pexamCardHtml;
                    });
            }
            getAllPQuizzesViewPExamJsp();
        </script>
    </body>
</html>
