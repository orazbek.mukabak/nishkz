<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Барлық тестілеу жүйелерін көру</title>
        <link rel="stylesheet" href="resources/css/view-all-pexams.css" />
    </head>
    <body>
        <div id="divViewAllPexamsJsp">
        </div>

        <script>
            function showSweetAlertTemp() {
                Swal.fire({
                    title: 'Кешіріңіз, бұл бет жақында толықтай іске қосылатын болады...',
                    showClass: {
                      popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                      popup: 'animate__animated animate__fadeOutUp'
                    }
                  });
            }
            
            function getAllPExamsViewAllPExamsJsp() {
                fetch("${pageContext.request.contextPath}/premiumexam/get-all-pexams/")
                    .then(response => response.json())
                    .then(data => {
                        const pexamPanel = (id, title, description, priority, status, index) => {
                            let temp = "<div class='card-view-pexam'>";
                            temp += "<img src='https://firebasestorage.googleapis.com/v0/b/school-online-2020/o/images%2Fpremiumexam%2F" + id + ".png?alt=media' />";
                            temp += "<div class='card-view-pexam-info'>";
                            temp += "<h5>" + title + "</h5>";
                            temp += "<p>" + description + "</p>";
                            if(id === 1) {
                                <sec:authorize access="isAuthenticated()">
                                    temp += "<a href=\"javascript: includeContent('premiumexam/view-pexam/" + id + "', 'container_fluid');\" class='btn'>Өзіңді сынап көр</a>";
                                </sec:authorize>
                                <sec:authorize access="!isAuthenticated()">
                                    temp += "<a href='${pageContext.request.contextPath}/signin?u=null&p=null' class='btn'>Өзіңді сынап көр</a>";
                                </sec:authorize>
                            } else {
                                temp += "<a href=\"javascript: showSweetAlertTemp();\" class='btn'>Өзіңді сынап көр</a>";
                            }
                            temp += "</div>";
                            temp += "</div>";
                            return temp;
                        };

                        let pexamCardHtml = "";
                        for(var i = 0; i < data.length; i++) {
                            pexamCardHtml += pexamPanel(data[i].id, data[i].title, data[i].description, data[i].priority, data[i].status, i);
                        }

                        document.getElementById('divViewAllPexamsJsp').innerHTML = pexamCardHtml;
                    });
            }
            getAllPExamsViewAllPExamsJsp();
        </script>
    </body>
</html>
