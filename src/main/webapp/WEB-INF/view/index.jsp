<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NISH.KZ</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"/>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</head>
<body>
    
    <%@ include file="header.jsp" %>

    <!-- <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Home</li>
        </ol>
    </nav> -->
    
    <%@ include file="banner.jsp" %>
    <div id='all_courses' class='all-courses'>
    </div>
    <%@ include file="teacher.jsp" %>

    <div class='outer-wrapper-intro-video-index-title'>
        <div class='inner-wrapper-intro-video-index'>
            <div class='title-intro-video-index' id='section_title_about_course_index' data-aos='fade-right'>"
                    Курс туралы
             </div>
         </div>
    </div>
    <div class='outer-wrapper-intro-video-index'>
        <div class='inner-wrapper-intro-video-index'>
            <div class='about-course-text-index' id='about_course_text_index' data-aos="fade-up">
                Назарбаев Зияткерлік Мектептеріне дайындауға арналған арнайы платформаға қош келдіңіз! Осы курсқа жазылу арқылы математикадан және логикадан емтиханда болатын ең маңызды тақырыптарды өтіп, қиын есептерді шығарудың жылдам және жеңіл әдістерін үйренесіз.
            </div>
            <div class='wrapper-intro-video-index' data-aos="fade-up">
                <iframe id='about_course_video_index' src="//player.vimeo.com/video/453730731?&autoplay=1&title=0&byline=0" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <%@ include file="footer.jsp" %>
    
    <script>

        const lanKazIndex = {
            "titleSectionCourses": "Бағдарлама курстары",
            "class": "сынып",
            "buyTheCourse": "Курсқа жазылу",
            "fullExplanation": "Толық ақпарат",
            "titleSectionAboutCourse": "Курс туралы",
            "aboutCourse": "Назарбаев Зияткерлік Мектептеріне дайындауға арналған арнайы платформаға қош келдіңіз! Осы курсқа жазылу арқылы математикадан және логикадан емтиханда болатын ең маңызды тақырыптарды өтіп, қиын есептерді шығарудың жылдам және жеңіл әдістерін үйренесіз.",
            "aboutCourseVideoUrl": "//player.vimeo.com/video/453730731?&autoplay=1&title=0&byline=0"

        }

        const lanRuIndex = {
            "titleSectionCourses": "Курсы в программе",
            "class": "класс",
            "buyTheCourse": "Записатья на курс",
            "fullExplanation": "Подробнее",
            "titleSectionAboutCourse": "О курсе",
            "aboutCourse": "Добро пожаловать на специальную площадку для подготовки к Назарбаев Интеллектуальным Школам! Записавшись на этот курс, вы изучите самые важные темы экзамена по математике и логике, а также узнаете быстрые и простые способы решения сложных задач.",
            "aboutCourseVideoUrl": "//player.vimeo.com/video/453731096?&autoplay=1&title=0&byline=0"
        }

        let currentLangIndex = lanRuIndex;

        if (getLanguage() === 'kaz') {
          currentLangIndex = lanKazIndex;
        }


        document.querySelectorAll("#section_title_about_course_index")[0].innerHTML = currentLangIndex.titleSectionAboutCourse;
        document.querySelectorAll("#about_course_text_index")[0].innerHTML = currentLangIndex.aboutCourse;

        document.querySelectorAll("#about_course_video_index")[0].src = currentLangIndex.aboutCourseVideoUrl;
        


        function openViewJsp (courseId) {
            window.location = '${pageContext.request.contextPath}/course/view/'+courseId;
        }

        function showSmallContactForm () {
            const contactUsOuter = document.getElementById('contact_us_outer');
            contactUsOuter.style.visibility = "visible";
            contactUsOuter.style.opacity = 1;  
        }

        function drawCousesInCategory_ViewAllCoursesJsp(category,coursesList) {
            let varHtml = "";
            let count = 0;
            for(let i = 0; i < coursesList.length; i++) {
                if (coursesList[i].categoryId === category.id) {
                        varHtml += "<div class='card-view-course' data-aos='zoom-in-down' data-aos-duration='700'>";
                            varHtml += "<div class='image-wrapper-card-view-course' onclick='openViewJsp("+coursesList[i].id+");'>";
                                varHtml += "<img src='https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fcourses%2F" + coursesList[i].id + ".jpg?alt=media' onerror=\"this.onerror=null; this.src='https://firebasestorage.googleapis.com/v0/b/nishkz/o/images%2Fcourses%2Ferror.jpg?alt=media'\" />";
                            varHtml += "</div>";

                            varHtml += "<div class='card-view-course-info'>";
                                varHtml += "<h6>" + coursesList[i].grade + "-" + currentLangIndex.class+ "</h6>";
                                varHtml += "<div class='course-description-card-view'>" + coursesList[i].description + "</div>";

                                let newPrice = 0;
                                let oldPriceClass = "course-price-card-view";
                                let newPriceClass = "course-new-price-card-view";

                                if (coursesList[i].discountValue > 0) {
                                    newPrice = coursesList[i].price * (100 - coursesList[i].discountValue) / 100;
                                    oldPriceClass = "course-price-card-view line-over-course-price";
                                    newPriceClass = "course-new-price-card-view show-new-course-price";
                                }

                                varHtml += "<div class='course-price-all-card-view'>";
                                    varHtml += "<div class='"+oldPriceClass+"'>" + coursesList[i].price + " тг</div>";
                                    varHtml += "<div class='"+newPriceClass+"'>" + newPrice + " тг</div>";
                                varHtml += "</div>";


                                varHtml += "<div class='buy-course-card-view' onclick='showSmallContactForm()'>";
                                    varHtml += currentLangIndex.buyTheCourse;
                                varHtml += "</div>";
                                varHtml += "<div class='enter-course-card-view' onclick='openViewJsp("+coursesList[i].id+");'>";
                                    varHtml += currentLangIndex.fullExplanation;
                                varHtml += "</div>";
                            varHtml += "</div>";
                        varHtml += "</div>";
                }
            }

            return varHtml;
                    
        }

        function drawCategory_ViewAllCoursesJsp(category, coursesList) {
            let varHtml = "";

            varHtml += "<div class='category-wrapper'>";
                varHtml += "<div class='category-courses'>";
                    varHtml += drawCousesInCategory_ViewAllCoursesJsp(category,coursesList);
                varHtml += "</div>";
            varHtml += "</div>";

            return varHtml;
        }

        function drawAllCourses_ViewAllCoursesJsp(categoryList, coursesList) {
            let varHtml = "";
            
            varHtml += "<div class='inner-wrapper-all-courses'>";
                
                varHtml += "<div class='title-all-courses' data-aos='fade-right'>"
                    varHtml += currentLangIndex.titleSectionCourses;
                varHtml += "</div>"
                
                for (let i=0; i < categoryList.length; i++) {
                    varHtml += drawCategory_ViewAllCoursesJsp(categoryList[i], coursesList);
                }

            varHtml += "</div>";

            document.getElementById('all_courses').innerHTML = varHtml;
            AOS.init();
        }
        
        function getAllCoursesViewAllCoursesJsp(categoryList) {
            $.ajax({
                type: 'get',
                url: "${pageContext.request.contextPath}/course/get-all-courses/",
                data: {},
                cache: false,
                success: function(data) {
                    drawAllCourses_ViewAllCoursesJsp(categoryList, data);
                },
                error: function(data, status, error) {
                }
            });
        }

        function compareByPriority( a, b ) {
          if ( a.priority < b.priority ){
            return -1;
          }
          if ( a.priority > b.priority ){
            return 1;
          }
          return 0;
        }

        
        function getAllCategoriesViewAllCoursesJsp() {
            fetch('${pageContext.request.contextPath}/course/get-all-categories/')
                .then(response => response.json())
                .then(data => {
                    getAllCoursesViewAllCoursesJsp(data.sort(compareByPriority));
                });
        }
        getAllCategoriesViewAllCoursesJsp();

    </script>
</body>
</html>