<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кіру беті...</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/signin.css" />
</head>
<body>
    <div class="login-page-signin-jsp">
        <div class="login-wrap-signin-jsp">
            <div class="login-title-signin-jsp" id='login_title_signin_jsp'>
                NISH.KZ Аккаунтыңызға Кіріңіз
            </div>
            <div class="login-content-signin-jsp">
                <form:form action="${pageContext.request.contextPath}/authenticate" method="POST">
                    <!-- Phone -->
                    <div class="login-input-wrapper-signin-jsp">
                        <i class="fas fa-user"></i>
                        <input id="login_phone_signin_jsp" type="phone" name="username" placeholder="Телефон" class="form-control" />
                    </div>
                    <!-- Password -->
                    <div class="login-input-wrapper-signin-jsp">
                        <i class="fas fa-lock"></i>
                        <input id="login_password_signin_jsp" type="password" name="password" placeholder="Пароль" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Пароль тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <!-- Login/Submit Button -->
                    <button type="submit" class="btn login-btn-signin-jsp" id='login_btn_signin_jsp'>Кіру</button>

                    <div class="login-forget-wrapper-signin-jsp">
                        <a id='login_forget_password_signin' href="${pageContext.request.contextPath}/register/recovery">Құпия сөзді ұмыттыңыз ба?</a><br/>
                    </div>

                    <div class="login-register-wrapper-signin-jsp">
                        <span id='login_no_account_signin'>Сізде әлі аккаунт жоқ па?</span>
                        <a id='login_register_signin' href="${pageContext.request.contextPath}/register/signup">Тіркеліңіз</a>
                    </div>
                    <!-- Place for messages: error, alert etc ... -->
                    <div>
                        <!-- Check for login error -->
                        <c:if test="${param.error != null}">
                            <script>

                                Swal.fire({
                                    title: 'Попробуйте еще раз...',
                                    text: 'Номер телефона или пароль неверны',
                                    confirmButtonColor: '#4B2D73'
                                });
                            </script>
                        </c:if>
                        <!-- Check for logout -->
                        <c:if test="${param.logout != null}">
                            <script>
                                window.location.replace("${pageContext.request.contextPath}");
                            </script>
                        </c:if>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    
    <%@ include file="footer.jsp" %>
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/lib/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">

        const lanKazSignIn = {
            "enterYourNishAccount": "NISH.KZ Аккаунтыңызға Кіріңіз",
            "login": "Кіру",
            "forgetPassword": "Құпия сөзді ұмыттыңыз ба?",
            "doYouHaveAccount": "Сізде әлі аккаунт жоқ па?",
            "register": "Тіркеліңіз",
            "passwordRequirement": "Пароль тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек",
            "writeDownKeys": "Төмендегі кілт сөздерді жазып алыңыз!"

        }

        const lanRuSignIn = {
            "enterYourNishAccount": "Войдите в учетную запись NISH.KZ",
            "login": "Вход",
            "forgetPassword": "Забыли пароль?",
            "doYouHaveAccount": "Не зарегистрированы?",
            "register": "Регистрация",
            "passwordRequirement": "Пароль должен содержать только латинские буквы и цифры. Длина пароля должна быть от 6 до 32",
            "writeDownKeys": "Запишите Ваш логин и пароль!"
        }

        let currentLangSignIn = lanRuSignIn;

        if (getLanguage() === 'kaz') {
          currentLangSignIn = lanKazSignIn;
        }

        document.querySelectorAll("#login_title_signin_jsp")[0].innerHTML = currentLangSignIn.enterYourNishAccount;
        document.querySelectorAll("#login_btn_signin_jsp")[0].innerHTML = currentLangSignIn.login;
        document.querySelectorAll("#login_forget_password_signin")[0].innerHTML = currentLangSignIn.forgetPassword;
        document.querySelectorAll("#login_no_account_signin")[0].innerHTML = currentLangSignIn.doYouHaveAccount;
        document.querySelectorAll("#login_register_signin")[0].innerHTML = currentLangSignIn.register;
        document.querySelectorAll("#login_password_signin_jsp")[0].title = currentLangSignIn.passwordRequirement;



        $(function(){
            $("#login_phone_signin_jsp").mask("8 (999) 999-99-99");
        });
        
        function getUrlVars() {
            var vars = {};
            window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        
        function getUrlParam(parameter, defaultvalue){
            var urlparameter = defaultvalue;
            if(window.location.href.indexOf(parameter) > -1){
                urlparameter = getUrlVars()[parameter];
                }
            return urlparameter;
        }
        
        var u = getUrlParam('u', "null");
        var p = getUrlParam('p', "null");
        if(u !== "null" && p !== "null") {
            u = u.replace("%20", " ");
            u = u.replace("%20", " ");
            window.history.pushState({}, document.title, "${pageContext.request.contextPath}/signin?u=null&p=null");
            var sweetAlertLoginMessage = currentLangSignIn.writeDownKeys + "<br>Логин: <b style='color: #ff0000;'>" + u + "</b><br>" + "Пароль: <b style='color: #ff0000;'>" + p + "</b>";
            Swal.fire(
                'Сіз сәтті тіркелдіңіз!',
                sweetAlertLoginMessage,
                'success'
            );
            document.getElementById("login_phone_signin_jsp").value = u;
            document.getElementById("login_password_signin_jsp").value = p;
        }
    </script>
</body>
</html>