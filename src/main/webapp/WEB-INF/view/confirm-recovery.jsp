<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кіру беті...</title>
    <link id="dynamic-favicon" rel="shortcut icon" href="resources/images/website/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css" />
    <link rel="stylesheet" href="resources/css/main.css" />
    <link rel="stylesheet" href="resources/css/signin.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
</head>
<body>
    <div class="login-header-signin-jsp">
        <a class="navbar-brand" href="${pageContext.request.contextPath}/">
            <img src='./resources/images/website/logo.svg'/>
        </a>
        <sec:authorize access="!isAuthenticated()" style="display: flex;width: 100%">
        <a class="signin-btn-header"  href="${pageContext.request.contextPath}/signin?u=null&p=null">Кіру</a>
        <a class="signup-btn-header"  href="${pageContext.request.contextPath}/register/signup">Тіркелу</a>
    </sec:authorize>
    </div>
    <div class="login-page-signin-jsp">
        <div class="login-wrap-signin-jsp">
            <div class="login-title-signin-jsp">
                Сіздің ұялы телефоныңызға SMS-код жіберілді...
            </div>
            <div class="login-content-signin-jsp">
                <!-- SMS Code -->
                <div class="login-input-wrapper-signin-jsp">
                    <i class="fas fa-sms"></i>
                    <input id="code_confirm_recovery_jsp" type="number" placeholder="SMS Код" class="form-control" max="9999" />
                </div>
                <!-- Login/Submit Button -->
                <button class="btn login-btn-signin-jsp" onclick='return callRecoveryConfirming();'>OK</button>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="resources/lib/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        
        $(function(){
            $("#phone_confirm_recovery_jsp").mask("8 (999) 999-99-99");
        });
        
        var u = getUrlParam('u', "null");
        var p = getUrlParam('p', "null");
        if(u !== "null" && p !== "null") {
            u = u.replace("%20", " ");
            u = u.replace("%20", " ");
            window.history.pushState({}, document.title, "${pageContext.request.contextPath}/confirm-recovery?u=null&p=null");
            document.getElementById("login_phone_signin_jsp").value = u;
            document.getElementById("login_password_signin_jsp").value = p;
        }
        
        function callRecoveryConfirming() {
            var code = document.getElementById('code_confirm_recovery_jsp').value;
            if(code <= 9999 && code >= 1000) {
                fetch('${pageContext.request.contextPath}/register/recovery-confirming/?code=' + code + '&phone=' + u + '&password=' + p)
                    .then(function(response) {
                        return response.text();
                    })
                    .then(function(text) {
                        if(text.includes('MESSAGE_FAIL')) {
                            sweetAlertMessageConfirmRecovery = "Сіз енгізген SMS-код сәйкес келмеді. Басынан қайталап көріңіз.";
                            Swal.fire(
                                'Кешіріңіз!',
                                sweetAlertMessageConfirmRecovery,
                                'error'
                            ).then((result) => {
                                if(result.value) {
                                    window.location.replace("${pageContext.request.contextPath}/register/recovery");
                                }
                            });
                        } else if(text.includes('MESSAGE_SUCCESSFUL')) {
                            sweetAlertMessageConfirmRecovery = "Төмендегі кілт сөздерді жазып алыңыз!<br>Логин: <b style='color: #ff0000;'>" + u + "</b><br>" + "Пароль: <b style='color: #ff0000;'>" + p + "</b>";
                            Swal.fire(
                                'Құпия сөз сәтті өзгертілді!',
                                sweetAlertMessageConfirmRecovery,
                                'success'
                            ).then((result) => {
                                if(result.value) {
                                    window.location.replace("${pageContext.request.contextPath}/signin?u=null&p=null");
                                }
                            });
                        } else {
                            window.location.replace("${pageContext.request.contextPath}/signin?u=null&p=null");
                        }
                    });
            } else {
                sweetAlertMessageConfirmRecovery = "SMS-код 4 орынды саннан тұрады.";
                Swal.fire(
                    'Кешіріңіз!',
                    sweetAlertMessageConfirmRecovery,
                    'error'
                );
            }
        }

        function getUrlVars() {
            var vars = {};
            window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
        
        function getUrlParam(parameter, defaultvalue){
            var urlparameter = defaultvalue;
            if(window.location.href.indexOf(parameter) > -1){
                urlparameter = getUrlVars()[parameter];
                }
            return urlparameter;
        }
        
    </script>
</body>
</html>