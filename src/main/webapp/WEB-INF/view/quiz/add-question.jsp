<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Жаңа сұрақ қосу</title>
    </head>
    <body>
        <div>
            <table>
                <tr>
                    <td>
                        <textarea id="textAddQuestion" class="form-control input-sm" name="text" rows="5" placeholder="Сұрақтың мәтіні..." maxlength="2048"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>A жауабы: </td>
                                <td><input id="answerAAddQuestion" name="answerA" type="text" class="form-control input-sm" placeholder="A жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>B жауабы: </td>
                                <td><input id="answerBAddQuestion" name="answerB" type="text" class="form-control input-sm" placeholder="B жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>C жауабы: </td>
                                <td><input id="answerCAddQuestion" name="answerC" type="text" class="form-control input-sm" placeholder="C жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>D жауабы: </td>
                                <td><input id="answerDAddQuestion" name="answerD" type="text" class="form-control input-sm" placeholder="D жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>E жауабы: </td>
                                <td><input id="answerEAddQuestion" name="answerE" type="text" class="form-control input-sm" placeholder="E жауабы..." /></td>
                            </tr>
                            <tr>
                                <td>Дұрыс жауабы: </td>
                                <td>
                                    <select id="correctAnswerAddQuestion" name="correctAnswer" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Дұрыс жауабын талдаңыз...</option>
                                        <option value="a">A</option>
                                        <option value="b">B</option>
                                        <option value="c">C</option>
                                        <option value="d">D</option>
                                        <option value="e">E</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityAddQuestion" name="priority" type="number" step="0.01" class="form-control input-sm" value="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Қиындығы: </td>
                                <td>
                                    <select id="levelAddQuestion" name="level" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Қиындық дәрежесін талдаңыз...</option>
                                        <option value="1">Оңай</option>
                                        <option value="2">Орташа</option>
                                        <option value="3">Қиын</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Жауаптың атауы: </td>
                                <td>
                                    <select id="solutionTitleAddQuestion" name="solutionTitle" class="form-control input-sm">
                                        <option value="0" selected>Жауап қарастырылмаған</option>
                                        <option value="1">Жауабы</option>
                                        <option value="2">Есептің шығарылу видеосы</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Жауабы: </td>
                                <td><input id="solutionAddQuestion" name="solution" type="text" class="form-control input-sm" value="" placeholder="Текст түрінде, немесе Vimeo видеосы (URL)..." /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return saveAddQuestion();'><i class="fas fa-save"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return cancelAddQuestion();'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                </tr>
            </table>
        </div>
        <script>
            function cancelAddQuestion() {
                includeContent("${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${quiz.lessonId}", "page-quiz");
            }
            
            function postAddQuestion(text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, solutionTitle, solution) {
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, solutionTitle, solution};
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };
                
                fetch('${pageContext.request.contextPath}/quiz/add-question?quizId=${quiz.id}', requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        cancelAddQuestion();
                    });
            }

            function saveAddQuestion() {
                var text = document.getElementById('textAddQuestion').value;
                var answerA = document.getElementById('answerAAddQuestion').value;
                var answerB = document.getElementById('answerBAddQuestion').value;
                var answerC = document.getElementById('answerCAddQuestion').value;
                var answerD = document.getElementById('answerDAddQuestion').value;
                var answerE = document.getElementById('answerEAddQuestion').value;
                var correctAnswer = document.getElementById('correctAnswerAddQuestion').value;
                var priority = document.getElementById('priorityAddQuestion').value;
                var level = document.getElementById('levelAddQuestion').value;
                var solutionTitle = document.getElementById('solutionTitleAddQuestion').value;
                var solution = document.getElementById('solutionAddQuestion').value;
                if(solutionTitle === "0") {
                    solution = "";
                }
                postAddQuestion(text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, solutionTitle, solution);
            }
        </script>
    </body>
</html>