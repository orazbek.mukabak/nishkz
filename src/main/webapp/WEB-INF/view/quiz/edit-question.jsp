<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Сұрақты өңдеу</title>
    </head>
    <body>
        <div>
            <table>
                <tr>
                    <td>
                        <textarea id="textEditQuestion" class="form-control input-sm" name="text" rows="5" placeholder="Сұрақтың мәтіні..." maxlength="2048">${question.text}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>A жауабы: </td>
                                <td><input id="answerAEditQuestion" name="answerA" type="text" class="form-control input-sm" placeholder="A жауабы..." value="${question.answerA}" /></td>
                            </tr>
                            <tr>
                                <td>B жауабы: </td>
                                <td><input id="answerBEditQuestion" name="answerB" type="text" class="form-control input-sm" placeholder="B жауабы..." value="${question.answerB}" /></td>
                            </tr>
                            <tr>
                                <td>C жауабы: </td>
                                <td><input id="answerCEditQuestion" name="answerC" type="text" class="form-control input-sm" placeholder="C жауабы..." value="${question.answerC}" /></td>
                            </tr>
                            <tr>
                                <td>D жауабы: </td>
                                <td><input id="answerDEditQuestion" name="answerD" type="text" class="form-control input-sm" placeholder="D жауабы..." value="${question.answerD}" /></td>
                            </tr>
                            <tr>
                                <td>E жауабы: </td>
                                <td><input id="answerEEditQuestion" name="answerE" type="text" class="form-control input-sm" placeholder="E жауабы..." value="${question.answerE}" /></td>
                            </tr>
                            <tr>
                                <td>Дұрыс жауабы: </td>
                                <td>
                                    <select id="correctAnswerEditQuestion" name="correctAnswer" class="form-control input-sm">
                                        <option value="" disabled selected hidden>Дұрыс жауабын талдаңыз...</option>
                                        <option value="a">A</option>
                                        <option value="b">B</option>
                                        <option value="c">C</option>
                                        <option value="d">D</option>
                                        <option value="e">E</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Орнын талдау: </td>
                                <td>
                                    <input id="priorityEditQuestion" name="priority" type="number" step="0.01" class="form-control input-sm" value="${question.priority}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Қиындығы: </td>
                                <td>
                                    <select id="levelEditQuestion" name="level" class="form-control input-sm" value="${question.level}">
                                        <option value="" disabled selected hidden>Қиындық дәрежесін талдаңыз...</option>
                                        <option value="1">Оңай</option>
                                        <option value="2">Орташа</option>
                                        <option value="3">Қиын</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Жауаптың атауы: </td>
                                <td>
                                    <select id="solutionTitleEditQuestion" name="solutionTitle" class="form-control input-sm">
                                        <option value="0" selected>Жауап қарастырылмаған</option>
                                        <option value="1">Жауабы</option>
                                        <option value="2">Есептің шығарылу видеосы</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Жауабы: </td>
                                <td><input id="solutionEditQuestion" name="solution" type="text" class="form-control input-sm" value="${question.solution}" placeholder="Текст түрінде, немесе Vimeo видеосы (URL)..." /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return saveEditQuestion();'><i class="fas fa-edit"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return cancelEditQuestion();'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                </tr>
            </table>
        </div>
        <script>
            function cancelEditQuestion() {
                includeContent("${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${question.quiz.lessonId}", "page-quiz");
            }
            
            function postVideoEditQuestion(id, text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, solutionTitle, solution) {
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                
                const raw = {id, text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, solutionTitle, solution};
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };
                
                fetch('${pageContext.request.contextPath}/quiz/edit-question', requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        cancelEditQuestion();
                    });
            }
            
            function saveEditQuestion () {
                var id = "${question.id}";
                var text = document.getElementById('textEditQuestion').value;
                var answerA = document.getElementById('answerAEditQuestion').value;
                var answerB = document.getElementById('answerBEditQuestion').value;
                var answerC = document.getElementById('answerCEditQuestion').value;
                var answerD = document.getElementById('answerDEditQuestion').value;
                var answerE = document.getElementById('answerEEditQuestion').value;
                var priority = document.getElementById('priorityEditQuestion').value;
                var correctAnswer = document.getElementById('correctAnswerEditQuestion').value;
                var level = document.getElementById('levelEditQuestion').value;
                var solutionTitle = document.getElementById('solutionTitleEditQuestion').value;
                var solution = document.getElementById('solutionEditQuestion').value;
                if(solutionTitle === "0") {
                    solution = "";
                }
                postVideoEditQuestion(id, text, answerA, answerB, answerC, answerD, answerE, correctAnswer, priority, level, solutionTitle, solution);
            }
            
            document.getElementById('correctAnswerEditQuestion').value = '${question.correctAnswer}';
            document.getElementById('levelEditQuestion').value = '${question.level}';
            document.getElementById('solutionTitleEditQuestion').value = '${question.solutionTitle}';
        </script>
    </body>
</html>