<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестті қарау</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view-quiz.css" />
    </head>
    <body>
        <div class="description-view-quiz-jsp" id="description_view_quiz_jsp">
            <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                <button class="btn btn-primary btn-sm default-btn" onclick='return includeContent("${pageContext.request.contextPath}/quiz/view-edit-quiz/${quiz.id}", "description_view_quiz_jsp");'><i class="fas fa-edit"></i> Тестті өңдеу</button><br>
            </sec:authorize>
            ${quiz.description}
        </div>
        <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
            <div class="add-question-view-quiz-jsp" id="add_question_view_quiz_jsp">
                <button class="btn btn-primary btn-sm default-btn" onclick='return includeContent("${pageContext.request.contextPath}/quiz/display-add-question/${quiz.id}", "add_question_view_quiz_jsp");'><i class="fas fa-edit"></i> Жаңа сұрақ қосу</button>
            </div>
        </sec:authorize>
        <div class="view-lesson-intro-quiz unselectable" id="view-quiz-questions-jsp">
        </div>
        <script>
            function finishViewQuizJsp(questionCounter) {
                fetch('${pageContext.request.contextPath}/quiz/get-responses-by-participant-id/?participantId=${participant.id}')
                    .then(response => response.json())
                    .then(data => {
                        if(data.length === 0) {
                            Swal.fire({
                                position: 'center',
                                title: 'Ешқандай сұрақта, жауапта табылмады...',
                                timer: 3000
                            });
                        } else if(data.length < questionCounter) {
                            Swal.fire({
                                position: 'center',
                                title: 'Сіз барлық сұрақтарға жауап бермедіңіз...',
                                timer: 3000
                            });
                        } else {
                            includeContent("${pageContext.request.contextPath}/quiz/submit-responses/${participant.id}", "page-quiz");
                        }
                    }
                );
            }

            function editQuestionViewQuizJsp(id) {
                includeContent('${pageContext.request.contextPath}/quiz/view-edit-question/' + id, 'page-quiz');
            }

            function deleteQuestionViewQuizJsp(id) {
                Swal.fire({
                    title: 'Осы сұрақты жойғыңыз келеді ма?',
                    text: "Осы сұрақ түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/quiz/delete-question/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Сұрақ сәтті жойылды.',
                                    'success'
                                );
                                includeContent('${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${quiz.lessonId}', 'page-quiz');
                            });
                    }
                });
            }

            async function updateAnswerResponse(answer, questionId, participantId) {
                let response = await fetch("${pageContext.request.contextPath}/quiz/add-response/?answer=" + answer + "&questionId=" + questionId + "&participantId=" + participantId);
                let data = await response.text();
                return data;
            }

            function updateAnswersStyle(id, answer) {
                const activeAnswer = document.querySelector("#answer_box_" + id + " .active-answer");
                if (activeAnswer !== null){
                    activeAnswer.className = "answer-box";
                }
                document.getElementById("answer_" + answer + "_box_" + id).classList.add('active-answer');
            }

            function updateAnswer(id, answer) {
                //document.getElementById("answer_box_" + id).style.display = "none";
                updateAnswerResponse(answer, id, "${participant.id}").then(function(text) {
                        //document.getElementById("answer_box_" + questionId).style.display = "block";
                        if(text.includes('STATUS.SUCCESSFUL')) {
                            updateAnswersStyle(id, answer);
                        } else if(text.includes('STATUS.QUESTION_NOT_FOUND')) {
                            includeContent('${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${quiz.lessonId}', 'page-quiz');
                        } else {
                            includeContent('${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${quiz.lessonId}', 'page-quiz');
                        }
                    });;
            }

            function getResponses() {
                fetch('${pageContext.request.contextPath}/quiz/get-responses-by-participant-id/?participantId=${participant.id}')
                    .then(response => response.json())
                    .then(data => {
                        for(var i = 0; i < data.length; i++) {
                            document.getElementById("answer_" + data[i].answer + "_box_" + data[i].questionId).classList.add('active-answer');
                        }
                    });
            }

            function getQuestions() {
                let questionCounter = 0;
                
                fetch('${pageContext.request.contextPath}/quiz/get-questions-by-quiz-id/?id=${quiz.id}')
                    .then(response => response.json())
                    .then(data => {
                        index = 1;
                        const quizBox = (id, q, a, b, c, d, e, l) => {
                            var quizHtml = "<div class='quiz-box'>";
                            <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                                quizHtml += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                                quizHtml += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                                quizHtml += "<a class='dropdown-item' href='javascript: editQuestionViewQuizJsp(" + id + ");'><i class='fas fa-pen'></i> Сұрақты өңдеу</a>";
                                quizHtml += "<a class='dropdown-item' href='javascript: deleteQuestionViewQuizJsp(" + id + ");'><i class='fas fa-trash-alt'></i> Сұрақты жою</a>";
                                quizHtml += "</div></div>";
                            </sec:authorize>
                            if (l === 1) { quizHtml += "<div class='level-box'><i class='fas fa-star'></i></div>"; }
                            if (l === 2) { quizHtml += "<div class='level-box'><i class='fas fa-star'></i><i class='fas fa-star'></i></div>"; }
                            if (l === 3) { quizHtml += "<div class='level-box'><i class='fas fa-star'></i><i class='fas fa-star'></i><i class='fas fa-star'></i></div>"; }
                            quizHtml += "<div class='question-box' id='question_box_" + id + "'>" + index + ". " + q + "</div>";
                            quizHtml += "<div id='answer_box_" + id + "'>";
                            if (a.length !== 0) { quizHtml += "<div class='answer-box' id='answer_a_box_" + id + "' onclick='updateAnswer(" + id + ", \"a\")'>A. " + a + "</div>"; }
                            if (b.length !== 0) { quizHtml += "<div class='answer-box' id='answer_b_box_" + id + "' onclick='updateAnswer(" + id + ", \"b\")'>B. " + b + "</div>"; }
                            if (c.length !== 0) { quizHtml += "<div class='answer-box' id='answer_c_box_" + id + "' onclick='updateAnswer(" + id + ", \"c\")'>C. " + c + "</div>"; }
                            if (d.length !== 0) { quizHtml += "<div class='answer-box' id='answer_d_box_" + id + "' onclick='updateAnswer(" + id + ", \"d\")'>D. " + d + "</div>"; }
                            if (e.length !== 0) { quizHtml += "<div class='answer-box' id='answer_e_box_" + id + "' onclick='updateAnswer(" + id + ", \"e\")'>E. " + e + "</div>"; }
                            quizHtml += "</div></div>";
                            index++;
                            return quizHtml;
                        };

                        let quizListHtml = "";
                        for(var i = 0; i < data.length; i++) {
                            questionCounter ++;
                            quizListHtml += quizBox(data[i].id, data[i].text, data[i].answerA, data[i].answerB, data[i].answerC, data[i].answerD, data[i].answerE, data[i].level);
                        }
                        quizListHtml += "<button class='btn btn-primary btn-sm default-btn' onclick='return finishViewQuizJsp(" + questionCounter + ");'><i class='fas fa-flag-checkered'></i> Аяқтау</button>";
                        document.getElementById('view-quiz-questions-jsp').innerHTML = quizListHtml;
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,"view-quiz-questions-jsp"]);
                        getResponses();
                    });
            }
            getQuestions();

            $(document).ready(function () {
                $("#view-quiz-questions-jsp").on("contextmenu",function(e){
                    return false;
                });
                
                $('#view-quiz-questions-jsp').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });                
            });

        </script>
    </body>
</html>
