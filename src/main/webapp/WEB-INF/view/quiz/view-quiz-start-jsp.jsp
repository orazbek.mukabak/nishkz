<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестті бастау</title>
    </head>
    <body>
        <div id="view-quiz-start-jsp">
            <br><br><br>
            <center><b>Осы сабаққа байланысты ұй жұмысы берілген жоқ</b></center>
        </div>
        <script>
            function getQuestionsForStartButton() {
                fetch('${pageContext.request.contextPath}/quiz/get-questions-by-quiz-id/?id=${quiz.id}')
                    .then(response => response.json())
                    .then(data => {
                        let htmlText = "<div style='padding: 20px; text-align: justify;'><b>Құрметті оқушым,</b><br>Бұл курстан жақсы нәтиже алып шығу үшін әр сабақтағы барлық видеоесептердің шығару жолдарын мұқият тыңдап, дәптеріңізге асықпай көшіріп жазып отырыңыз. Видеосабақты көріп болған соң төмендегі <b>Бастау</b> батырмасын басып осы тақырып бойынша берілген тесттерді орындап көріңіз. Сіздің жинаған ұпайыңыз және тесттің дұрыс жауаптары <b>Аяқтау</b> батырмасын басқаннан кейін көрінетін болады.<br><br><b>Сізге сәттілік тілеймін!</b><br><b>Құрметпен,</b><br><b>Тлеуғали Бақты ағайыңыз</b><br></div>";
                        htmlText += "<center><button class='btn btn-primary btn-sm default-btn' onclick='return includeContent(\"${pageContext.request.contextPath}/quiz/display-view-quiz/${participant.id}\", \"page-quiz\");'><i class='fas fa-play'></i> Бастау</button></center>";
                        if(data.length === 0) {
                            <sec:authorize access="hasAuthority('TUTOR') and hasAuthority('MANAGER') and isAuthenticated()">
                                document.getElementById("view-quiz-start-jsp").innerHTML = htmlText;
                            </sec:authorize>
                        } else {
                            document.getElementById("view-quiz-start-jsp").innerHTML = htmlText;
                        }
                        
                    });
            }
            getQuestionsForStartButton();
        </script>
    </body>
</html>