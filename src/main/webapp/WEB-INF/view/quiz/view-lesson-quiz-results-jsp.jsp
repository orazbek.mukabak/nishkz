<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тест нәтижелерін қарау</title>
        <link rel="stylesheet" href="resources/css/view-lesson-quiz-results.css" />
    </head>
    <body>
        <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
            <div>
                <button class="btn btn-primary btn-sm" style="background-color: #4B2D73; border-color: #4B2D73; margin: 10px;" onclick='return updateParticipantsList();'>Жаңарту</button>
            </div>
        </sec:authorize>
        <div id="div_view_lesson_quiz_results_jsp">
            <img class="loading-image-quiz-results" src="resources/images/website/loading.gif"/>
        </div>
        <script>
            function updateParticipantsList() {
                fetch("${pageContext.request.contextPath}/quiz/update-partisipants-list?courseId=${lesson.course.id}&quizId=${quiz.id}")
                    .then(function(response) {
                        return response.text();
                    })
                    .then(function(text) {
                        if(text.includes('STATUS.UPDATED')) {
                            includeContent('quiz/view-lesson-quiz-results/${lesson.id}', 'right_content');
                        }
                    });
            }
            function deleteParticipantViewLessonQuizResultsJsp(id) {
                Swal.fire({
                    title: 'Осы қолданушының алған бағасын жойғыңыз келеді ма?',
                    text: "Осы қолданушының алған бағасы түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/quiz/delete-participant/' + id)
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Алған бағасы сәтті жойылды.',
                                    'success'
                                );
                                includeContent('quiz/view-lesson-quiz-results/${lesson.id}', 'right_content');
                            });
                    }
                });
            }

            function getParticipantsViewLessonQuizResultsJsp() {
                fetch('${pageContext.request.contextPath}/quiz/get-participants/?quizId=${quiz.id}')
                    .then(response => response.json())
                    .then(data => {
                        drawQuizResultsForLesson(data);
                    });
            }

            function drawQuizResultsForLesson(results) {
                let htmlStr = "";

                for (let i=0; i < results.length; i++) {
                    htmlStr += drawSingleQuizResultForLesson(results[i], i);
                }

                document.getElementById('div_view_lesson_quiz_results_jsp').innerHTML = htmlStr;
            }

            function drawSingleQuizResultForLesson(result, index) {
                let htmlStr = "";

                htmlStr += "<div class='quiz-row-view-lesson-quiz-results-jsp'>";
                    htmlStr += "<div style='width:40px' class='row-item-view-lesson-quiz-results-jsp'>";
                        htmlStr += index + 1;
                    htmlStr += "</div>";
                    htmlStr += "<div class='row-item-big-view-lesson-quiz-results-jsp' onclick='return displayProfile(\"" + result.username + "\");' style='cursor: pointer;'>";
                        htmlStr += result.username;
                    htmlStr += "</div>";
                    //htmlStr += "<div class='row-item-big-view-lesson-quiz-results-jsp'>";
                    //    htmlStr += result.firstName;
                    //htmlStr += "</div>";
                    //htmlStr += "<div class='row-item-big-view-lesson-quiz-results-jsp'>";
                    //    htmlStr += result.lastName;
                    //htmlStr += "</div>";
                    
                    htmlStr += "<div class='row-item-big-view-lesson-quiz-results-jsp'>";
                    if (result.endDateTime) {

                        if (result.endDateTime.date){
                            htmlStr += result.endDateTime.date.year+"-"+result.endDateTime.date.month+"-"+result.endDateTime.date.day+" "+result.endDateTime.time.hour+":"+result.endDateTime.time.minute+":"+result.endDateTime.time.second;
                        } else {
                            htmlStr += result.endDateTime.year+"-"+result.endDateTime.monthValue+"-"+result.endDateTime.dayOfMonth+" "+result.endDateTime.hour+":"+result.endDateTime.minute+":"+result.endDateTime.second;
                        }
                       
                    }
                    htmlStr += "</div>";
                    
                    htmlStr += "<div class='row-item-small-view-lesson-quiz-results-jsp'>";
                    htmlStr += "(" + result.correctAnswers + "/" + result.totalQuestion + ") " + result.totalScore + "%";
                    htmlStr += "</div>";
                    htmlStr += "<div class='row-item-view-lesson-quiz-results-jsp'>";
                        if(result.status === 2) {
                            htmlStr += "<a class='delete-result-view-lesson-quiz-results-jsp' href=\"javascript: deleteParticipantViewLessonQuizResultsJsp(" + result.id + ");\">Нәтижені жою</a>";
                            }
                    htmlStr += "</div>";
                htmlStr += "</div>";

                return htmlStr;
            }

            getParticipantsViewLessonQuizResultsJsp();
            
        </script>
    </body>
</html>
