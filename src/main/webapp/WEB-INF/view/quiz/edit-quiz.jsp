<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестті өңдеу</title>
    </head>
    <body>
        <div>
            <table class="tableView">
                <tr>
                    <td>
                        <label style="font-size: 12px; font-weight: bolder; font-family: Verdana; color: #24303F;">Тестті өңдеу:</label>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>Ұзақтығы (минут): </td>
                    <td>
                        <input id="durationEditQuiz" name="duration" type="number" step="0.01" class="form-control input-sm" value="${quiz.duration}" />
                    </td>
                </tr>
                <tr>
                    <td>Қысқаша ақпарат: </td>
                    <td>
                        <textarea id="descriptionEditQuiz" name="description" class="form-control input-sm" style="min-height: 300px; width: 100%;">${quiz.description}</textarea>
                    </td>
                </tr>
                <tr>
                    <td>Тапсыру мерзімін қосу: </td>
                    <td>
                        <select id="deadlineAllowedEditQuiz" name="deadlineAllowed" class="form-control input-sm">
                            <option id="deadlineAllowedNoEditQuiz" value="0" selected>Жоқ, тапсыру мерзімі болмайды</option>
                            <option id="deadlineAllowedYesEditQuiz" value="1">Йә, тапсыру мерзімі болады</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Ашылу уақыты: </td>
                    <td>
                        <input id="deadlineValueEditQuiz" name="deadlineValue" type="datetime-local" class="form-control input-sm" value="${quiz.deadlineValue}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return saveEditQuiz();'><i class="fas fa-save"></i> Сақтау</button>
                        <button class="btn btn-primary btn-sm default-btn" onclick='return cancelEditQuiz();'><i class="fas fa-times"></i> Болдырмау</button>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
        <script>
            function cancelEditQuiz() {
                includeContent("${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${quiz.lessonId}", "page-quiz");
            }
            
            function saveEditQuiz() {
                var id = "${quiz.id}";
                var duration = document.getElementById('durationEditQuiz').value;
                var description = document.getElementById('descriptionEditQuiz').value;
                
                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                const raw = {
                    id,
                    duration,
                    description
                };
                
                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };
                
                fetch('${pageContext.request.contextPath}/quiz/edit-quiz', requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        cancelEditQuiz();
                    });
            }
        </script>
    </body>
</html>