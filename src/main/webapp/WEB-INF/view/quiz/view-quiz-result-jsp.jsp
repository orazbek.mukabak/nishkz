<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Қысқаша видео</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/view-quiz-result.css" />
    </head>
    <body>
        <div class="bg-white rounded-lg p-5">
            <h2 class="h6 font-weight-bold text-center mb-4">Тесттің нәтижесі</h2>
            <div class="progress mx-auto" id="totalPercentageValue" data-value='12'>
                <span class="progress-left">
                    <span class="progress-bar border-primary"></span>
                </span>
                <span class="progress-right">
                    <span class="progress-bar border-primary"></span>
                </span>
                <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold" id="totalPercentage">0<sup class="small">%</sup></div>
                </div>
            </div>

            <div class="row text-center mt-4">
                <div class="col-6 border-right">
                    <div class="h4 font-weight-bold mb-0" id="totalQuestion">0</div><span class="small text-gray">Барлық сұрақтар</span>
                </div>
                <div class="col-6">
                    <div class="h4 font-weight-bold mb-0" id="totalCorrectAnswer">0</div><span class="small text-gray">Дұрыс жауаптар</span>
                </div>
            </div>
        </div>

        <div class="view-lesson-intro-quiz unselectable" id="view-quiz-questions-jsp">
        </div>
        
        <div class="delete-participant" id="delete_participant">
            <sec:authorize access="hasAuthority('TUTOR') or hasAuthority('MANAGER') and isAuthenticated()">
                <button class="btn btn-primary btn-sm default-btn" onclick='return deleteParticipantViewQuizResultJsp();'><i class="fas fa-trash-alt"></i> Нәтижені жою</button>
            </sec:authorize>
        </div>
        <script>
            function deleteParticipantViewQuizResultJsp() {
                Swal.fire({
                    title: 'Осы тесттен алған бағаңызды жойғыңыз келеді ма?',
                    text: "Осы тесттен алған бағаңыз түбегейлі жойылатын болады!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4B2D73',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Йә',
                    cancelButtonText: 'Жоқ'
                }).then((result) => {
                    if(result.value) {
                        fetch('${pageContext.request.contextPath}/quiz/delete-participant/${participant.id}')
                            .then(response => {
                                Swal.fire(
                                    'Жойылды!',
                                    'Алған бағаңыз сәтті жойылды.',
                                    'success'
                                );
                                includeContent('${pageContext.request.contextPath}/quiz/display-view-lesson-quiz/${quiz.lessonId}', 'page-quiz');
                            });
                    }
                });
            }
            
            function sweetAlertPlaySolution(videoId) {
                Swal.fire({
                    customClass: 'sweetAlertSolution',
                    html: "<iframe src='//player.vimeo.com/video/" + videoId + "?&autoplay=1&title=0&byline=0' width='640px' height='360px' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>",  
                    showConfirmButton: false
                });
            }
            
            function getQuestions() {
                fetch('${pageContext.request.contextPath}/quiz/get-questions-by-quiz-id/?id=${quiz.id}')
                    .then(response => response.json())
                    .then(data => {
                        var index = 1;
                        var questionCounter = 0;
                        const quizBox = (id, q, a, b, c, d, e, l, st, s) => {
                            var quizHtml = "<div class='quiz-box'>";
                            if (l === 1) { quizHtml += "<div class='level-box'><i class='far fa-star'></i></div>"; }
                            if (l === 2) { quizHtml += "<div class='level-box'><i class='far fa-star'></i><i class='far fa-star'></i></div>"; }
                            if (l === 3) { quizHtml += "<div class='level-box'><i class='far fa-star'></i><i class='far fa-star'></i><i class='far fa-star'></i></div>"; }
                            quizHtml += "<div class='question-box' id='question_box_" + id + "'>" + index + ". " + q + "</div>";
                            quizHtml += "<div id='answer-box-" + id + "'>";
                            if (a.length !== 0) { quizHtml += "<div class='answer-box' id='answer_a_box_" + id + "'>A. " + a + "</div>"; }
                            if (b.length !== 0) { quizHtml += "<div class='answer-box' id='answer_b_box_" + id + "'>B. " + b + "</div>"; }
                            if (c.length !== 0) { quizHtml += "<div class='answer-box' id='answer_c_box_" + id + "'>C. " + c + "</div>"; }
                            if (d.length !== 0) { quizHtml += "<div class='answer-box' id='answer_d_box_" + id + "'>D. " + d + "</div>"; }
                            if (e.length !== 0) { quizHtml += "<div class='answer-box' id='answer_e_box_" + id + "'>E. " + e + "</div>"; }
                            quizHtml += "</div>";
                            if(st === 1) {
                                quizHtml += "<div class='solution-box'><b><i class='fas fa-lightbulb'></i> Сұрақтың жауабы: </b>" + s + "</div>";
                            } else if (st === 2){
                                s = s.substring(s.indexOf(".com/") + 5);
                                s = s.substring(0, s.indexOf("/"));
                                quizHtml += "<div class='solution-box' onclick='sweetAlertPlaySolution(" + s + ");'><b><i class='fas fa-film'></i> Есептің шығарылу жолы</b></div>";
                            } else {}
                            quizHtml += "</div>";
                            index++;
                            questionCounter++;
                            return quizHtml;
                        };

                        let quizListHtml = "";
                        for(var i = 0; i < data.length; i++) {
                            quizListHtml += quizBox(data[i].id, data[i].text, data[i].answerA, data[i].answerB, data[i].answerC, data[i].answerD, data[i].answerE, data[i].level, data[i].solutionTitle, data[i].solution);
                        }
                        document.getElementById('totalQuestion').innerHTML = questionCounter;
                        document.getElementById('view-quiz-questions-jsp').innerHTML = quizListHtml;
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,"view-quiz-questions-jsp"]);
                        getResponses();
                    });
            }
            
            function updateParticipantTableViewQuizResultJsp(result) {
                fetch('${pageContext.request.contextPath}/quiz/update-result/${participant.id}?totalScore=' + result)
                    .then(data => {
                    });
            }
            
            function getTotalScore(responses, questions) {
                var totalScore = 0;
                var totalPercentage = 0;
                for(var i = 0; i < responses.length; i++) {
                    for(var j = 0; j < questions.length; j++) {
                        if(responses[i].questionId === questions[j].id) {
                            if(responses[i].answer === questions[j].correctAnswer) {
                                totalScore += 1;
                            } else {
                                break;
                            }
                        }
                    }
                }
                totalPercentage = Math.round((totalScore/questions.length)*100);
                document.getElementById('totalCorrectAnswer').innerHTML = totalScore;
                
                drawDiagram(totalPercentage);
                //document.getElementById('totalPercentageValue').data('value', totalPercentage);
                document.getElementById('totalPercentage').innerHTML = totalPercentage + "%";
                updateParticipantTableViewQuizResultJsp(totalPercentage);
            }
            
            function getResponses() {
                fetch('${pageContext.request.contextPath}/quiz/get-responses-by-participant-id/?participantId=${participant.id}')
                    .then(response => response.json())
                    .then(data => {
                        for(var i = 0; i < data.length; i++) {
                            document.getElementById("answer_" + data[i].answer + "_box_" + data[i].questionId).classList.add('red-answer');
                        }
                        getCorrectAnswers(data);
                    });
            }
            
            function getCorrectAnswers(responses) {
                fetch('${pageContext.request.contextPath}/quiz/get-correct-answers-by-quiz-id/?quizId=${quiz.id}')
                    .then(response => response.json())
                    .then(data => {
                        for(var i = 0; i < data.length; i++) {
                            document.getElementById("answer_" + data[i].correctAnswer + "_box_" + data[i].id).classList.add('green-answer');
                        }
                        getTotalScore(responses, data);
                    });
            }
            getQuestions();

            function drawDiagram(percentage) {
                $(function() {
                    $(".progress").each(function() {
                        var value = percentage;
                        var left = $(this).find('.progress-left .progress-bar');
                        var right = $(this).find('.progress-right .progress-bar');
                        if (value > 0) {
                            if(value <= 50) {
                                right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)');
                            } else {
                                right.css('transform', 'rotate(180deg)');
                                left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)');
                            }
                        }
                    });

                    function percentageToDegrees(percentage) {
                        return percentage / 100 * 360;
                    }
                });
            }

            $(document).ready(function () {
                $("#view-quiz-questions-jsp").on("contextmenu",function(e){
                    return false;
                });
                
                $('#view-quiz-questions-jsp').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
            });
        </script>
    </body>
</html>