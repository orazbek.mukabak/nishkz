<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banner</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/banner.css" />
</head>
<body>
  <div class='outer-wrapper-banner'>
    <div class='filter-outer-wrapper-banner'>
      <div class='inner-wrapper-banner'>
        <div class='text-part-banner' id='text_part_banner'>
          <h1 data-aos='fade-right'>НИШ-қа Сапалы Дайындау 4 Айлық Бағдарламасы</h1>
          <!-- <h2 data-aos='fade-up'>Курс 15 Қыркүйекте басталады. Сабақ басталғанға дейін 50% жеңілдікпен жазылыңыз!</h2> -->
          <h3 data-aos='fade-right'>Қазақстандағы Кәсіби Математик Тренермен Бірге</h3>
        </div>
        <div class='image-part-banner'>
          <!-- <img src='resources/images/website/online-class.svg'> -->
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    const lanKazBanner = {
        "bannerText1": "НИШ-қа 4 Айлық Сапалы Дайындық Бағдарламасы",
        "bannerText2": "Сабақтар 15 Қыркүйек басталады. Сабақ басталғанға дейін 50% жеңілдікпен жазылыңыз!",
        "bannerText3": "Қазақстандағы Кәсіби Математик Тренермен Бірге",
    }

    const lanRuBanner = {
        "bannerText1": "4-Месячная Программа Качественной Подготовки в НИШ",
        "bannerText2": "Курс начинается c 15 сентября. Зарегистрируйтесь со скидкой 50% до начала уроков!",
        "bannerText3": "С Профессиональным Тренером по Математике в Казахстане",
    }

    let currentLangBanner = lanRuBanner;

    if (getLanguage() === 'kaz') {
      currentLangBanner = lanKazBanner;
    }

    document.querySelectorAll("#text_part_banner > h1")[0].innerHTML = currentLangBanner.bannerText1;
    //document.querySelectorAll("#text_part_banner > h2")[0].innerHTML = currentLangBanner.bannerText2;
    document.querySelectorAll("#text_part_banner > h3")[0].innerHTML = currentLangBanner.bannerText3;

  </script>
</body>
</html>