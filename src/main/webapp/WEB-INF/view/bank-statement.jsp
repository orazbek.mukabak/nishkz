<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="resources/css/main.css" /> 
        <title>Bank statement</title>    
    </head>
    <body>
        <div class='wrapper-bank-statement-jsp'>
            <h5>Реквизиттер</h5>
            <p>
                «Ozat Online»
                <br>Заңды мекен-жайы / Нақты мекен-жайы:
                <br>Қазақстан Республикасы, Нұр-Сұлтан қ., Бауыржан Момышұлы к-сі, 16
                <br>тел: 8 (775) 899 77 55
                БСН / ЖСН: 200640023676
                <br>БеК / КБЕ: 17
                <br>Шот нөмірі: KZ7196503F0009723642
                <br>Банк деректемелері:
                <br>«ForteBank» АҚ
                <br>БСК/БИК IRTYKZKA
            </p>
        </div>
    </body>
</html>
