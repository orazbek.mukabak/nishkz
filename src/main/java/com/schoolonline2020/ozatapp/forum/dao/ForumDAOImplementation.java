package com.schoolonline2020.ozatapp.forum.dao;

import com.schoolonline2020.ozatapp.forum.entity.Reply;
import com.schoolonline2020.ozatapp.forum.entity.Topic;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

@Repository
public class ForumDAOImplementation implements ForumDAO {

    @Autowired
    private SessionFactory forumSessionFactory;

    @Override
    public List<Topic> findTopicsByLessonId(int lessonId) {
        Session currentSession = forumSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Topic.class);
        query.add(Restrictions.eq("lessonId", lessonId));
        query.addOrder(Order.asc("createdDate"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("text"), "text")
        .add(Projections.property("lessonId"), "lessonId")
        .add(Projections.property("username"), "username")
        .add(Projections.property("replyCounter"), "replyCounter")
        .add(Projections.property("createdDate"), "createdDate"))
        .setResultTransformer(Transformers.aliasToBean(Topic.class));
        return query.list();
    }

    @Override
    public String save(Topic topic) {
        Session currentSession = forumSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(topic);
        return "Done! Topic is saved!";
    }

    @Override
    public List<Reply> findRepliesByTopicId(int topicId) {
        Session currentSession = forumSessionFactory.getCurrentSession();
        Topic topic = currentSession.get(Topic.class, topicId);
        Criteria query = currentSession.createCriteria(Reply.class);
        query.add(Restrictions.eq("topic", topic));
        query.addOrder(Order.asc("createdDate"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("text"), "text")
        //.add(Projections.property("topic"), "topic")
        .add(Projections.property("username"), "username")
        .add(Projections.property("createdDate"), "createdDate"))
        .setResultTransformer(Transformers.aliasToBean(Reply.class));
        return query.list();
    }

    @Override
    public String save(int topicId, Reply reply) {
        Session currentSession = forumSessionFactory.getCurrentSession();
        Topic topic = currentSession.get(Topic.class, topicId);
        topic.addReply(reply);
        topic.setReplyCounter(topic.getReplyCounter() + 1);
        reply.setTopic(topic);
        currentSession.saveOrUpdate(reply);
        return "Done! Reply is saved!";
    }

    @Override
    public String deleteTopic(int id) {
        Session currentSession = forumSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Topic where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "STATUS.DELETED";
    }
    
    @Override
    public String deleteReply(int id) {
        Session currentSession = forumSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Reply where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "STATUS.DELETED";
    }
    
}