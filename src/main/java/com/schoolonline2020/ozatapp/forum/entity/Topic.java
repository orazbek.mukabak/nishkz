package com.schoolonline2020.ozatapp.forum.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="topic")
public class Topic implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="text")
    private String text;
    @Column(name="lesson_id")
    private int lessonId;
    @Column(name="username")
    private String username;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="topic", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<Reply> replies;
    @Column(name="reply_counter")
    private int replyCounter;
    @Column(name="created_date")
    private LocalDateTime createdDate;
    
    @Transient
    private String firstName;
    @Transient
    private String lastName;
    @Transient
    private String email;
    @Transient
    private int innerId;
    @Transient
    protected Object[] jdoDetachedState;

    public Topic() {
    }

    public Topic(String text, int lessonId, String username, int replyCounter, LocalDateTime createdDate) {
        this.text = text;
        this.lessonId = lessonId;
        this.username = username;
        this.replyCounter = replyCounter;
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public void addReply(Reply reply) {
        if(replies == null) {
            replies = new ArrayList<>();
        }
        replies.add(reply);
        reply.setTopic(this);
    }
    
    public List<Reply> getReplies() {
        return replies;
    }

    public void setReplies(List<Reply> replies) {
        this.replies = replies;
    }

    public int getReplyCounter() {
        return replyCounter;
    }

    public void setReplyCounter(int replyCounter) {
        this.replyCounter = replyCounter;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getInnerId() {
        return innerId;
    }

    public void setInnerId(int innerId) {
        this.innerId = innerId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
    
}