package com.schoolonline2020.ozatapp.forum.service;

import com.schoolonline2020.ozatapp.forum.dao.ForumDAO;
import com.schoolonline2020.ozatapp.forum.entity.Reply;
import com.schoolonline2020.ozatapp.forum.entity.Topic;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ForumServiceImplementation implements ForumService {

    @Autowired
    private ForumDAO forumDAO;

    @Override
    @Transactional("forumTransactionManager")
    public List<Topic> findTopicsByLessonId(int lessonId) {
        return forumDAO.findTopicsByLessonId(lessonId);
    }

    @Override
    @Transactional("forumTransactionManager")
    public String save(Topic topic) {
        return forumDAO.save(topic);
    }

    @Override
    @Transactional("forumTransactionManager")
    public List<Reply> findRepliesByTopicId(int topicId) {
        return forumDAO.findRepliesByTopicId(topicId);
    }

    @Override
    @Transactional("forumTransactionManager")
    public String save(int topicId, Reply reply) {
        return forumDAO.save(topicId, reply);
    }

    @Override
    @Transactional("forumTransactionManager")
    public String deleteTopic(int id) {
        return forumDAO.deleteTopic(id);
    }

    @Override
    @Transactional("forumTransactionManager")
    public String deleteReply(int id) {
        return forumDAO.deleteReply(id);
    }
}