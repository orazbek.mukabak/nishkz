package com.schoolonline2020.ozatapp.forum.service;

import com.schoolonline2020.ozatapp.forum.entity.Reply;
import com.schoolonline2020.ozatapp.forum.entity.Topic;
import java.util.List;

public interface ForumService {

    public List<Topic> findTopicsByLessonId(int lessonId);
    public String save(Topic topic);
    public List<Reply> findRepliesByTopicId(int topicId);
    public String save(int topicId, Reply reply);
    public String deleteTopic(int id);
    public String deleteReply(int id);

}