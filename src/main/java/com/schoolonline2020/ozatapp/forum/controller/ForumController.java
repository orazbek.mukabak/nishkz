package com.schoolonline2020.ozatapp.forum.controller;

import com.schoolonline2020.ozatapp.course.entity.Course;
import com.schoolonline2020.ozatapp.course.entity.Lesson;
import com.schoolonline2020.ozatapp.course.service.CourseService;
import com.schoolonline2020.ozatapp.forum.entity.Reply;
import com.schoolonline2020.ozatapp.forum.entity.Topic;
import com.schoolonline2020.ozatapp.forum.service.ForumService;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuiz;
import com.schoolonline2020.ozatapp.profile.entity.Profile;
import com.schoolonline2020.ozatapp.profile.service.ProfileService;
import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.entity.Question;
import com.schoolonline2020.ozatapp.quiz.entity.Quiz;
import com.schoolonline2020.ozatapp.quiz.entity.Response;
import com.schoolonline2020.ozatapp.quiz.service.QuizService;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/forum")
public class ForumController {

    @Autowired
    private ForumService forumService;
        
    @Autowired
    private CourseService courseService;
    
    @Autowired
    private ProfileService profileService;
    
    @GetMapping(path="/display-view-forum/")
    public String displayViewForumJspPage(@RequestParam("lessonId") int lessonId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Profile profile = profileService.findProfileById(username);
        Lesson lesson = courseService.findLessonById(lessonId);
        model.addAttribute("profile", profile);
        model.addAttribute("lesson", lesson);
        return "forum/view-forum-jsp";
    }
    
    @GetMapping(value = "/get-topics-by-lesson-id/")
    public @ResponseBody
    List getTopicsByLessonId(@RequestParam("lessonId") int lessonId){
        List<Topic> topics = forumService.findTopicsByLessonId(lessonId);
        for(Topic topic:topics) {
            Profile profile = profileService.findProfileById(topic.getUsername());
            if(profile != null) {
                topic.setFirstName(profile.getFirstName());
                topic.setLastName(profile.getLastName());
                topic.setEmail(profile.getEmail());
                topic.setInnerId(profile.getInnerId());
            }
        }
        return topics;
    }
    
    @PostMapping(value = "/add-topic")
    @ResponseBody
    public String addTopic(@RequestBody Topic topic) {
        LocalDateTime createdDate = LocalDateTime.now();
        topic.setCreatedDate(createdDate);
        return forumService.save(topic);
    }
    
    @GetMapping(value = "/get-replies-by-topic-id/")
    public @ResponseBody
    List getRepliesByTopicId(@RequestParam("topicId") int topicId){
        List<Reply> replies = forumService.findRepliesByTopicId(topicId);
        for(Reply reply:replies) {
            Profile profile = profileService.findProfileById(reply.getUsername());
            if(profile != null) {
                reply.setFirstName(profile.getFirstName());
                reply.setLastName(profile.getLastName());
                reply.setEmail(profile.getEmail());
                reply.setInnerId(profile.getInnerId());
            }
        }
        return replies;
    }
    
    @PostMapping(value = "/add-reply")
    @ResponseBody
    public String addReply(@RequestBody Reply reply, @RequestParam("topicId") int topicId) {
        LocalDateTime createdDate = LocalDateTime.now();
        reply.setCreatedDate(createdDate);
        return forumService.save(topicId, reply);
    }
    
    @GetMapping(path="/delete-topic/{id:.+}")
    public String deleteTopicProcess(@PathVariable int id, Model model) {
        String status = forumService.deleteTopic(id);
        model.addAttribute("status", status);
        return "status";
    }
    
    @GetMapping(path="/delete-reply/{id:.+}")
    public String deleteReplyProcess(@PathVariable int id, Model model) {
        String status = forumService.deleteReply(id);
        model.addAttribute("status", status);
        return "status";
    }

}