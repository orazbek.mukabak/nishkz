package com.schoolonline2020.ozatapp.course.dao;

import com.schoolonline2020.ozatapp.course.entity.Category;
import com.schoolonline2020.ozatapp.course.entity.Course;
import com.schoolonline2020.ozatapp.course.entity.Lesson;
import com.schoolonline2020.ozatapp.course.entity.Video;
import java.util.List;

public interface CourseDAO {
    
    public Course findCourseById(int id);
    public List<Course> findAllCourses();
    public List<Lesson> findLessonsByCourseId(int id);
    public String saveLesson(int courseId, Lesson lesson);
    public Lesson findLessonById(int id);
    public List<Video> findVideosByLessonId(int lessonId);
    public String saveVideo(int lessonId, Video video);
    public String save(Lesson lesson);
    public String deleteLesson(int id);
    public String deleteVideo(int id);
    public String save(Course course);
    public String deleteCourse(int id);
    public List<Category> findAllCategories();

}