package com.schoolonline2020.ozatapp.course.dao;

import com.schoolonline2020.ozatapp.course.entity.Category;
import com.schoolonline2020.ozatapp.course.entity.Course;
import com.schoolonline2020.ozatapp.course.entity.Lesson;
import com.schoolonline2020.ozatapp.course.entity.Video;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

@Repository
public class CourseDAOImplementation implements CourseDAO {

    @Autowired
    private SessionFactory courseSessionFactory;
    
    @Override
    public List<Course> findAllCourses() {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Course.class);
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("categoryId"), "categoryId")
        .add(Projections.property("description"), "description")
        .add(Projections.property("grade"), "grade")
        .add(Projections.property("subject"), "subject")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discountTitle"), "discountTitle")
        .add(Projections.property("discountValue"), "discountValue")
        .add(Projections.property("videoId"), "videoId")
        .add(Projections.property("startDateTime"), "startDateTime")
        .add(Projections.property("feature1"), "feature1")
        .add(Projections.property("feature2"), "feature2")
        .add(Projections.property("feature3"), "feature3")
        .add(Projections.property("feature4"), "feature4")
        .add(Projections.property("feature5"), "feature5")
        .add(Projections.property("feature6"), "feature6")
        .add(Projections.property("feature7"), "feature7")
        .add(Projections.property("feature8"), "feature8")
        .add(Projections.property("feature9"), "feature9")
        .add(Projections.property("feature10"), "feature10")
        .add(Projections.property("priority"), "priority")
        .add(Projections.property("username"), "username")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Course.class));
        return query.list();
    }
    
    @Override
    public Course findCourseById(int id) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        return currentSession.get(Course.class, id);
    }
    
    @Override
    public List<Lesson> findLessonsByCourseId(int id) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Course course = currentSession.get(Course.class, id);
        Criteria query = currentSession.createCriteria(Lesson.class);
        query.add(Restrictions.eq("course", course));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("liveUrl"), "liveUrl")
        //.add(Projections.property("summary"), "summary")
        .add(Projections.property("priority"), "priority")
        .add(Projections.property("startDateTime"), "startDateTime")
        .add(Projections.property("regDate"), "regDate")
        .add(Projections.property("quizAllowed"), "quizAllowed")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Lesson.class));
        return query.list();
    }
    
    @Override
    public String saveLesson(int courseId, Lesson lesson) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Course course = currentSession.get(Course.class, courseId);
        course.addLesson(lesson);
        lesson.setCourse(course);
        currentSession.saveOrUpdate(lesson);
        return "Done! Lesson is saved!";
    }
    
    @Override
    public Lesson findLessonById(int id) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        return currentSession.get(Lesson.class, id);
    }
    
    @Override
    public List<Video> findVideosByLessonId(int lessonId) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Lesson lesson = currentSession.get(Lesson.class, lessonId);
        Criteria query = currentSession.createCriteria(Video.class);
        query.add(Restrictions.eq("lesson", lesson));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("videoId"), "videoId")
        .add(Projections.property("title"), "title")
        .add(Projections.property("url"), "url")
        .add(Projections.property("thumbnailUrl"), "thumbnailUrl")
        .add(Projections.property("duration"), "duration")
        .add(Projections.property("priority"), "priority"))
        .setResultTransformer(Transformers.aliasToBean(Video.class));
        return query.list();
    }
    
    @Override
    public String saveVideo(int lessonId, Video video) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Lesson lesson = currentSession.get(Lesson.class, lessonId);
        lesson.addVideo(video);
        video.setLesson(lesson);
        currentSession.saveOrUpdate(video);
        return "Done! Video is saved!";
    }
    
    @Override
    public String save(Lesson lesson) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(lesson);
        return "Done! Lesson is saved!";
    }

    @Override
    public String deleteLesson(int id) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Lesson where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Lesson is deleted!";
    }

    @Override
    public String deleteVideo(int id) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Video where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Video is deleted!";
    }

    @Override
    public String save(Course course) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(course);
        return "Done! Course is saved!";
    }

    @Override
    public String deleteCourse(int id) {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Course where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Course is deleted!";
    }

    @Override
    public List<Category> findAllCategories() {
        Session currentSession = courseSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Category.class);
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("priority"), "priority"))
        .setResultTransformer(Transformers.aliasToBean(Category.class));
        return query.list();
    }
}