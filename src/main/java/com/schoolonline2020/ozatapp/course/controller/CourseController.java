package com.schoolonline2020.ozatapp.course.controller;

import com.schoolonline2020.ozatapp.course.entity.Course;
import com.schoolonline2020.ozatapp.course.entity.Lesson;
import com.schoolonline2020.ozatapp.course.entity.Video;
import com.schoolonline2020.ozatapp.course.service.CourseService;
import com.schoolonline2020.ozatapp.payment.entity.Purchase;
import com.schoolonline2020.ozatapp.payment.service.PaymentService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseService courseService;
    
    @Autowired
    private PaymentService paymentService;
    
    @GetMapping(path="/add")
    public String add() {
        return "course/add";
    }
    
    @GetMapping(path="/edit/{id:.+}")
    public String edit(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        model.addAttribute("course", course);
        return "course/edit";
    }
    
    @PostMapping(value = "/edit-course")
    @ResponseBody
    public String editCourse(@RequestBody Course course, @RequestParam("startDateTime") String startDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime parsedDateTime = LocalDateTime.parse(startDateTime, formatter);
        Course tempCourse = courseService.findCourseById(course.getId());
        tempCourse.setCategoryId(course.getCategoryId());
        tempCourse.setDescription(course.getDescription());
        tempCourse.setGrade(course.getGrade());
        tempCourse.setSubject(course.getSubject());
        tempCourse.setLanguage(course.getLanguage());
        tempCourse.setPrice(course.getPrice());
        tempCourse.setDiscountTitle(course.getDiscountTitle());
        tempCourse.setDiscountValue(course.getDiscountValue());
        tempCourse.setVideoId(course.getVideoId());
        tempCourse.setStartDateTime(parsedDateTime);
        tempCourse.setFeature1(course.getFeature1());
        tempCourse.setFeature2(course.getFeature2());
        tempCourse.setFeature3(course.getFeature3());
        tempCourse.setFeature4(course.getFeature4());
        tempCourse.setFeature5(course.getFeature5());
        tempCourse.setFeature6(course.getFeature6());
        tempCourse.setFeature7(course.getFeature7());
        tempCourse.setFeature8(course.getFeature8());
        tempCourse.setFeature9(course.getFeature9());
        tempCourse.setFeature10(course.getFeature10());
        tempCourse.setPriority(course.getPriority());
        return courseService.save(tempCourse);
    }
    
    @GetMapping(path="/view-all")
    public String viewAll() {
        return "course/view-all";
    }
    
    @GetMapping(path="/view-my-courses")
    public String viewMyCourses() {
        return "course/my-courses";
    }
    
    @GetMapping(value = "/get-all-courses/")
    public @ResponseBody
    List getAllCourses() {
        List<Course> courses = courseService.findAllCourses();
        if(courses == null) {
            courses = new ArrayList<>();
        }
        return courses;
    }
    
    @GetMapping(path="/view/{id:.+}")
    public String view(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        if(course == null) {
            return "exception/exception404";
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authentication.getName();
            if(username.equals("anonymousUser")) {
                model.addAttribute("status", "STATUS.ANONYMOUSUSER");
            } else {
                Purchase purchase = paymentService.findPurchaseByCourseIdAndUsername(course.getId(), username);
                // Create purchase for the owner
                if(purchase == null) {
                    if(course.getUsername().equals(username)) {
                        paymentService.save(new Purchase("C" + course.getId(), username, new Date()));
                        purchase = paymentService.findPurchaseByCourseIdAndUsername(course.getId(), username);
                    }
                }
                if(purchase == null) {
                    model.addAttribute("status", "STATUS.NEWUSER");
                } else {
                    model.addAttribute("status", "STATUS.PAIDUSER");
                }
            }
            
            model.addAttribute("course", course);
            return "course/view";
        }
    }
    
    @GetMapping(path="/view-special/{id:.+}")
    public String viewSpecial(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        if(course == null) {
            return "exception/exception404";
        } else {
            List<Lesson> lessons = courseService.findLessonsByCourseId(id);
            if(lessons.isEmpty()) {
                LocalDateTime now = LocalDateTime.now();
                courseService.save(new Lesson("Test lesson", "This is a special test lesson", null, 0, now, new Date(), 1, course, 's'));
                lessons = courseService.findLessonsByCourseId(id);
                return "redirect:/course/view-lesson/" + lessons.get(0).getId();
            } else {
                return "redirect:/course/view-lesson/" + lessons.get(0).getId();
            }
        }
    }
    
    @GetMapping(path="/view-lesson/{id:.+}")
    public String displayViewLesson(@PathVariable("id") int id, Model model) {
        Lesson lesson = courseService.findLessonById(id);
        if(lesson == null) {
            return "exception/exception404";
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authentication.getName();
            
            Course course = courseService.findCourseById(lesson.getCourse().getId());
            model.addAttribute("course", course);
            
            Purchase purchase = paymentService.findPurchaseByCourseIdAndUsername(lesson.getCourse().getId(), username);
            if(purchase == null || username.equals("anonymousUser")) {
                if(lesson.getStatus() == 's') {
                    model.addAttribute("status", "STATUS.NEWUSER");
                    model.addAttribute("lesson", lesson);
                    return "course/view-lesson";
                } else {
                    return "redirect:/course/view-special/" + lesson.getCourse().getId();
                }
            } else {
                model.addAttribute("status", "STATUS.PAIDUSER");
                if(lesson.getStatus() == 'c') {
                    LocalDateTime now = LocalDateTime.now();
                    if(now.plusHours(6).isAfter(lesson.getStartDateTime())) {
                        lesson.setStatus('a');
                        courseService.save(lesson);
                        lesson = courseService.findLessonById(id);
                        model.addAttribute("lesson", lesson);
                        return "course/view-lesson";
                    } else {
                        if(course.getUsername().equals(username)) {
                            model.addAttribute("lesson", lesson);
                            return "course/view-lesson";
                        } else {
                            model.addAttribute("lesson", lesson);
                            return "course/view-lesson-locked";
                        }
                    }
                } else {
                    model.addAttribute("lesson", lesson);
                    return "course/view-lesson";
                }
            }
        }
    }
    
    @GetMapping(path="/view-lesson-summary/{id:.+}")
    public String viewLessonSummary(@PathVariable("id") int id, Model model) {
        Lesson lesson = courseService.findLessonById(id);
        model.addAttribute("lesson", lesson);
        return "course/view-lesson-summary";
    }
    
    @GetMapping(path="/view-lesson-video/{id:.+}")
    public String viewLessonVideo(@PathVariable("id") int id, Model model) {
        Lesson lesson = courseService.findLessonById(id);
        model.addAttribute("lesson", lesson);
        return "course/view-lesson-video";
    }

    
    @GetMapping(path="/display-add-lesson/{id:.+}")
    public String displayAddLessonJspPage(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        model.addAttribute("course", course);
        return "course/add-lesson-jsp";
    }
    
    @GetMapping(path="/display-all-courses")
    public String displayViewAllCoursesJspPage() {
        return "course/view-all-courses-jsp";
    }
    
    @GetMapping(path="/view-all-lessons/{id:.+}")
    public String displayViewAllLessonsJspPage(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        model.addAttribute("course", course);
        return "course/view-all-lessons-jsp";
    }

    @GetMapping(value = "/get-lessons-by-course-id/")
    public @ResponseBody
    List getLessonsByCourseId(@RequestParam("id") int id){
        return courseService.findLessonsByCourseId(id);
    }
    
    @PostMapping(value = "/add-lesson")
    @ResponseBody
    public String addLesson(@RequestBody Lesson lesson, @RequestParam("courseId") int courseId, @RequestParam("startDateTime") String startDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime parsedDateTime = LocalDateTime.parse(startDateTime, formatter);
        lesson.setStartDateTime(parsedDateTime);
        Date regDate = new Date();
        lesson.setRegDate(regDate);
        return courseService.saveLesson(courseId, lesson);
    }
    
    @GetMapping(value = "/get-videos-by-lesson-id/")
    public @ResponseBody
    List getVideosByLessonId(@RequestParam("lessonId") int lessonId){
        return courseService.findVideosByLessonId(lessonId);
    }
    
    @PostMapping(value = "/add-video")
    @ResponseBody
    public String addVideo(@RequestBody Video video, @RequestParam("lessonId") int lessonId) {
        return courseService.saveVideo(lessonId, video);
    }
    
    @GetMapping(path="/display-view-lesson-locked/{id:.+}")
    public String displayViewLessonLocked(@PathVariable("id") int id, Model model) {
        Lesson lesson = courseService.findLessonById(id);
        lesson.setSummary("");
        lesson.setPriority(0);
        lesson.setRegDate(null);
        lesson.setVideos(null);
        model.addAttribute("lesson", lesson);
        return "course/view-lesson-locked-jsp";
    }
    
    @GetMapping(path="/delete-lesson/{id:.+}")
    public String deleteLessonProcess(@PathVariable int id, Model model) {
        String status = courseService.deleteLesson(id);
        model.addAttribute("status", status);
        return "status";
    }
    
    @GetMapping(path="/delete-video/{id:.+}")
    public String deleteVideoProcess(@PathVariable int id, Model model) {
        String status = courseService.deleteVideo(id);
        model.addAttribute("status", status);
        return "status";
    }
    
    @GetMapping(path="/view-edit-lesson/{id:.+}")
    public String displayEditLesson(@PathVariable("id") int id, Model model) {
        Lesson lesson = courseService.findLessonById(id);
        model.addAttribute("lesson", lesson);
        return "course/edit-lesson-jsp";
    }
    
    @PostMapping(value = "/edit-lesson")
    @ResponseBody
    public String editLesson(@RequestBody Lesson lesson, @RequestParam("startDateTime") String startDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime parsedDateTime = LocalDateTime.parse(startDateTime, formatter);
        Lesson tempLesson = courseService.findLessonById(lesson.getId());
        tempLesson.setTitle(lesson.getTitle());
        tempLesson.setLiveUrl(lesson.getLiveUrl());
        tempLesson.setPriority(lesson.getPriority());
        tempLesson.setSummary(lesson.getSummary());
        tempLesson.setQuizAllowed(lesson.getQuizAllowed());
        tempLesson.setStatus(lesson.getStatus());
        tempLesson.setStartDateTime(parsedDateTime);
        return courseService.saveLesson(tempLesson.getCourse().getId(), tempLesson);
    }
    
    @PostMapping(value = "/add-course")
    @ResponseBody
    public String addCourse(@RequestBody Course course, @RequestParam("startDateTime") String startDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime parsedDateTime = LocalDateTime.parse(startDateTime, formatter);
        course.setStartDateTime(parsedDateTime);
        course.setStatus('c');
        return courseService.save(course);
    }
    
    @GetMapping(path="/delete-course/{id:.+}")
    public String deleteCourseProcess(@PathVariable int id, Model model) {
        String status = courseService.deleteCourse(id);
        model.addAttribute("status", status);
        return "status";
    }
    
    @GetMapping(path="/edit-picture/{id:.+}")
    public String editPicture(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        model.addAttribute("course", course);
        return "course/edit-picture";
    }
    
    @GetMapping(value = "/get-all-categories/")
    public @ResponseBody
    List getAllCategories(){
        return courseService.findAllCategories();
    }
    
    @GetMapping(path="/update-course-status/{id:.+}")
    public String updateCourseStatusProcess(@PathVariable int id, @RequestParam("status") char status) {
        Course course = courseService.findCourseById(id);
        course.setStatus(status);
        return courseService.save(course);
    }
    
    @GetMapping(path="/view-course-result/{id:.+}")
    public String displayCourseResultJsp(@PathVariable("id") int id, Model model) {
        Course course = courseService.findCourseById(id);
        model.addAttribute("course", course);
        return "course/view-course-result-jsp";
    }
    
    @GetMapping(path="/view-all-course-results")
    public String displayViewAllCourseResultsJspPage() {
        return "course/view-all-course-results-jsp";
    }
    
    @GetMapping(value = "/get-my-courses")
    public @ResponseBody
    List getMyCourses() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        List<Purchase> purchases = paymentService.findPurchasesByUsername(username);
        List<Course> courses = new ArrayList<>();
        if(purchases != null) {
            for(Purchase purchase:purchases) {
                String product = purchase.getProduct();
                if(product.contains("C")) {
                    int courseId = Integer.parseInt(product.substring(1));
                    Course course = courseService.findCourseById(courseId);
                    course.setLessons(null);
                    courses.add(course);
                }
            }
        }
        return courses;
    }

}