package com.schoolonline2020.ozatapp.course.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="lesson")
public class Lesson implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="live_url")
    private String liveUrl;
    @Column(name="summary")
    private String summary;
    @Column(name="priority")
    private double priority;
    @Column(name="start_date_time")
    private LocalDateTime startDateTime;
    @Column(name="reg_date")
    private Date regDate;
    @Column(name="quiz_allowed")
    private int quizAllowed;
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="course_id")
    private Course course;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="lesson", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<Video> videos;
    @Column(name="status")
    private char status;
    @Transient
    protected Object[] jdoDetachedState;
    
    public Lesson() {
    }

    public Lesson(String title, String liveUrl, String summary, double priority, LocalDateTime startDateTime, Date regDate, int quizAllowed, Course course, char status) {
        this.title = title;
        this.liveUrl = liveUrl;
        this.summary = summary;
        this.priority = priority;
        this.startDateTime = startDateTime;
        this.regDate = regDate;
        this.quizAllowed = quizAllowed;
        this.course = course;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public int getQuizAllowed() {
        return quizAllowed;
    }

    public void setQuizAllowed(int quizAllowed) {
        this.quizAllowed = quizAllowed;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    public void addVideo(Video video) {
        if(videos == null) {
            videos = new ArrayList<>();
        }
        videos.add(video);
        video.setLesson(this);
    }
    
    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> lessons) {
        this.videos = videos;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}