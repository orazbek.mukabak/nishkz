package com.schoolonline2020.ozatapp.course.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="course")
public class Course implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="category_id")
    private int categoryId;
    @Column(name="description")
    private String description;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="course", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<Lesson> lessons;
    @Column(name="grade")
    private int grade;
    @Column(name="subject")
    private String subject;
    @Column(name="language")
    private String language;
    @Column(name="price")
    private int price;
    @Column(name="discount_title")
    private String discountTitle;
    @Column(name="discount_value")
    private int discountValue;
    @Column(name="video_id")
    private String videoId;
    @Column(name="start_date_time")
    private LocalDateTime startDateTime;
    @Column(name="feature1")
    private String feature1;
    @Column(name="feature2")
    private String feature2;
    @Column(name="feature3")
    private String feature3;
    @Column(name="feature4")
    private String feature4;
    @Column(name="feature5")
    private String feature5;
    @Column(name="feature6")
    private String feature6;
    @Column(name="feature7")
    private String feature7;
    @Column(name="feature8")
    private String feature8;
    @Column(name="feature9")
    private String feature9;
    @Column(name="feature10")
    private String feature10;
    @Column(name="priority")
    private double priority;
    @Column(name="username")
    private String username;
    @Column(name="status")
    private char status;
    @Transient
    protected Object[] jdoDetachedState;

    public Course() {
    }

    public Course(int categoryId, String description, int grade, String subject, String language, int price, String discountTitle, int discountValue, String videoId, LocalDateTime startDateTime, String feature1, String feature2, String feature3, String feature4, String feature5, String feature6, String feature7, String feature8, String feature9, String feature10, double priority, String username, char status) {
        this.categoryId = categoryId;
        this.description = description;
        this.grade = grade;
        this.subject = subject;
        this.language = language;
        this.price = price;
        this.discountTitle = discountTitle;
        this.discountValue = discountValue;
        this.videoId = videoId;
        this.startDateTime = startDateTime;
        this.feature1 = feature1;
        this.feature2 = feature2;
        this.feature3 = feature3;
        this.feature4 = feature4;
        this.feature5 = feature5;
        this.feature6 = feature6;
        this.feature7 = feature7;
        this.feature8 = feature8;
        this.feature9 = feature9;
        this.feature10 = feature10;
        this.priority = priority;
        this.username = username;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void addLesson(Lesson lesson) {
        if(lessons == null) {
            lessons = new ArrayList<>();
        }
        lessons.add(lesson);
        lesson.setCourse(this);
    }
    
    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }
    
    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDiscountTitle() {
        return discountTitle;
    }

    public void setDiscountTitle(String discountTitle) {
        this.discountTitle = discountTitle;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getFeature1() {
        return feature1;
    }

    public void setFeature1(String feature1) {
        this.feature1 = feature1;
    }

    public String getFeature2() {
        return feature2;
    }

    public void setFeature2(String feature2) {
        this.feature2 = feature2;
    }

    public String getFeature3() {
        return feature3;
    }

    public void setFeature3(String feature3) {
        this.feature3 = feature3;
    }

    public String getFeature4() {
        return feature4;
    }

    public void setFeature4(String feature4) {
        this.feature4 = feature4;
    }

    public String getFeature5() {
        return feature5;
    }

    public void setFeature5(String feature5) {
        this.feature5 = feature5;
    }

    public String getFeature6() {
        return feature6;
    }

    public void setFeature6(String feature6) {
        this.feature6 = feature6;
    }

    public String getFeature7() {
        return feature7;
    }

    public void setFeature7(String feature7) {
        this.feature7 = feature7;
    }

    public String getFeature8() {
        return feature8;
    }

    public void setFeature8(String feature8) {
        this.feature8 = feature8;
    }

    public String getFeature9() {
        return feature9;
    }

    public void setFeature9(String feature9) {
        this.feature9 = feature9;
    }

    public String getFeature10() {
        return feature10;
    }

    public void setFeature10(String feature10) {
        this.feature10 = feature10;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}