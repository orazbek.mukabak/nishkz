package com.schoolonline2020.ozatapp.course.service;

import com.schoolonline2020.ozatapp.course.entity.Course;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.schoolonline2020.ozatapp.course.dao.CourseDAO;
import com.schoolonline2020.ozatapp.course.entity.Category;
import com.schoolonline2020.ozatapp.course.entity.Lesson;
import com.schoolonline2020.ozatapp.course.entity.Video;

@Service
public class CourseServiceImplementation implements CourseService {

    @Autowired
    private CourseDAO courseDAO;

    @Override
    @Transactional("courseTransactionManager")
    public List<Course> findAllCourses() {
        return courseDAO.findAllCourses();
    }

    @Override
    @Transactional("courseTransactionManager")
    public Course findCourseById(int id) {
        return courseDAO.findCourseById(id);
    }
    
    @Override
    @Transactional("courseTransactionManager")
    public List<Lesson> findLessonsByCourseId(int id) {
        return courseDAO.findLessonsByCourseId(id);
    }

    @Override
    @Transactional("courseTransactionManager")
    public String saveLesson(int courseId, Lesson lesson) {
        return courseDAO.saveLesson(courseId, lesson);
    }

    @Override
    @Transactional("courseTransactionManager")
    public Lesson findLessonById(int id) {
        return courseDAO.findLessonById(id);
    }
    
    @Override
    @Transactional("courseTransactionManager")
    public List<Video> findVideosByLessonId(int lessonId) {
        return courseDAO.findVideosByLessonId(lessonId);
    }
    
    @Override
    @Transactional("courseTransactionManager")
    public String saveVideo(int lessonId, Video video) {
        return courseDAO.saveVideo(lessonId, video);
    }
    
    @Override
    @Transactional("courseTransactionManager")
    public String save(Lesson lesson) {
        return courseDAO.save(lesson);
    }

    @Override
    @Transactional("courseTransactionManager")
    public String deleteLesson(int id) {
        return courseDAO.deleteLesson(id);
    }

    @Override
    @Transactional("courseTransactionManager")
    public String deleteVideo(int id) {
        return courseDAO.deleteVideo(id);
    }

    @Override
    @Transactional("courseTransactionManager")
    public String save(Course course) {
        return courseDAO.save(course);
    }

    @Override
    @Transactional("courseTransactionManager")
    public String deleteCourse(int id) {
        return courseDAO.deleteCourse(id);
    }
    
    @Override
    @Transactional("courseTransactionManager")
    public List<Category> findAllCategories() {
        return courseDAO.findAllCategories();
    }
}