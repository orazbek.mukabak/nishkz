package com.schoolonline2020.ozatapp.premiumexam.controller;

import com.schoolonline2020.ozatapp.premiumexam.entity.PExam;
import com.schoolonline2020.ozatapp.premiumexam.entity.PParticipant;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuestion;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuiz;
import com.schoolonline2020.ozatapp.premiumexam.entity.PResponse;
import com.schoolonline2020.ozatapp.premiumexam.service.PremiumexamService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/premiumexam")
public class PremiumexamController {

    @Autowired
    private PremiumexamService premiumexamService;
    
    @GetMapping(path="/display-all-pexams")
    public String displayViewAllPexamsJspPage() {
        return "premiumexam/view-all-pexams-jsp";
    }
    
    @GetMapping(value = "/get-all-pexams/")
    public @ResponseBody
    List getAllPexams(){
        return premiumexamService.findAllPexams();
    }
    
    @GetMapping(path="/view-pexam/{id:.+}")
    public String displayViewPexamJsp(@PathVariable("id") int id, Model model) {
        PExam pExam = premiumexamService.findPexamById(id);
        model.addAttribute("pExam", pExam);
        return "premiumexam/view-pexam-jsp";
    }
    
    @GetMapping(path="/view-pquiz/{id:.+}")
    public String displayViewPQuizJsp(@PathVariable("id") int id, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        PQuiz pQuiz = premiumexamService.findPQuizById(id);
        PParticipant pParticipant = premiumexamService.findPParticipantByPQuizIdAndUsername(id, username);
        model.addAttribute("pQuiz", pQuiz);
        model.addAttribute("pParticipant", pParticipant);
        return "premiumexam/view-pquiz-jsp";
    }
    
    @GetMapping(value = "/get-pquizzes-by-pexam-id/")
    public @ResponseBody
    List getPquizzesByPexamId(@RequestParam("pexamId") int pexamId){
        return premiumexamService.findPquizzesByPexamId(pexamId);
    }

    @GetMapping(value = "/get-pquestions-by-pquiz-id/")
    public @ResponseBody
    List getPquestionsByPquizId(@RequestParam("pquizId") int pquizId){
        return premiumexamService.findPquestionsByPquizId(pquizId);
    }
    
    @PostMapping(value = "/add-results")
    @ResponseBody
    public String addResults(@RequestBody List<PResponse> pResponses, @RequestParam("pQuizId") int pQuizId, @RequestParam("pParticipantId") int pParticipantId) {
        if(pResponses == null) {
            pResponses = new ArrayList<>();
        }
        PParticipant pParticipant = premiumexamService.findPParticipantById(pParticipantId);
        if(!pResponses.isEmpty()) {
            for(PResponse pResponse:pResponses) {
                pResponse.setpParticipant(pParticipant);
                PQuestion pQuestion = premiumexamService.findPQuestionById(pResponse.getpQuestionId());
                int score = 0;
                if(pResponse.getAnswer().equals(pQuestion.getCorrectAnswer())) {
                    score = 1;
                }
                pResponse.setScore(score);
                premiumexamService.save(pParticipantId, pResponse);
            }
        }
        pParticipant.setStatus(2);
        premiumexamService.save(pParticipant);
        return "DONE";
    }
    
    @GetMapping(value = "/get-pparticipant-status/")
    public String getPParticipantStatus(@RequestParam("pQuizId") int pQuizId, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        PParticipant pParticipant = premiumexamService.findPParticipantByPQuizIdAndUsername(pQuizId, username);
        if(pParticipant == null) {
            model.addAttribute("status", "STATUS.NOT_FOUND");
            return "status";
        } else {
            if(pParticipant.getStatus() == 2) {
                model.addAttribute("status", "STATUS.SUBMITTED");
                return "status";
            } else if(pParticipant.getStatus() == 1) {
                model.addAttribute("status", "STATUS.PROCESSING");
                return "status";
            } else {
                model.addAttribute("status", "STATUS.NOT_SUBMITTED");
                return "status";
            }
        }
    }
    
    @GetMapping(path="/start-pquiz/{pQuizId:.+}")
    public String startPQuiz(@PathVariable("pQuizId") int pQuizId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        PParticipant pParticipant = premiumexamService.findPParticipantByPQuizIdAndUsername(pQuizId, username);
        if(pParticipant == null) {
            pParticipant = new PParticipant(username, pQuizId, 1);
            premiumexamService.save(pParticipant);
        }
        model.addAttribute("status", "STATUS.OK");
        return "status";
    }
    
    @GetMapping(path="/view-pquiz-result/{pQuizId:.+}")
    public String displayViewPQuizResultJsp(@PathVariable("pQuizId") int pQuizId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        PQuiz pQuiz = premiumexamService.findPQuizById(pQuizId);
        PParticipant pParticipant = premiumexamService.findPParticipantByPQuizIdAndUsername(pQuizId, username);
        model.addAttribute("pQuiz", pQuiz);
        model.addAttribute("pParticipant", pParticipant);
        return "premiumexam/view-pquiz-result-jsp";
    }
    
    @GetMapping(value="/get-results-by-pparticipant-id/")
    public @ResponseBody
    List getResultsByPParticipantId(@RequestParam("pParticipantId") int pParticipantId){
        PParticipant pParticipant = premiumexamService.findPParticipantById(pParticipantId);
        List<PQuestion> pQuestions = premiumexamService.findPquestionsByPquizId(pParticipant.getpQuizId());
        for(PQuestion pQuestion:pQuestions) {
            PResponse pResponse = premiumexamService.findPResponseByPParticipantAndPQuestionId(pParticipant, pQuestion.getId());
            if(pResponse == null) {
                pQuestion.setStudentAnswer("");
                pQuestion.setScore(0);
            } else {
                pQuestion.setStudentAnswer(pResponse.getAnswer());
                pQuestion.setScore(pResponse.getScore());
            }
        }
        return pQuestions;
    }
    
    @PostMapping(value = "/add-variant")
    @ResponseBody
    public String addVariant(@RequestBody PQuiz pQuiz, @RequestParam("pExamId") int pExamId) {
        return premiumexamService.save(pExamId, pQuiz);
    }
    
    @GetMapping(path="/delete-variant/{id:.+}")
    public String deletePQuizProcess(@PathVariable int id) {
        return premiumexamService.deletePQuiz(id);
    }
    
    @GetMapping(path="/display-add-pquestion/{id:.+}")
    public String displayAddPQuestionJspPage(@PathVariable("id") int id, Model model) {
        PQuiz pQuiz = premiumexamService.findPQuizById(id);
        model.addAttribute("pQuiz", pQuiz);
        return "premiumexam/add-pquestion-jsp";
    }
    
    @PostMapping(value = "/add-pquestion")
    @ResponseBody
    public String addPQuestion(@RequestBody PQuestion pQuestion, @RequestParam("pQuizId") int pQuizId) {
        return premiumexamService.save(pQuizId, pQuestion);
    }
    
    @GetMapping(path="/delete-pquestion/{id:.+}")
    public String deletePQuestionProcess(@PathVariable int id) {
        return premiumexamService.deletePQuestion(id);
    }
    
    @GetMapping(path="/delete-pparticipant/{id:.+}")
    public String deletePParticipantProcess(@PathVariable int id) {
        return premiumexamService.deletePParticipant(id);
    }
}