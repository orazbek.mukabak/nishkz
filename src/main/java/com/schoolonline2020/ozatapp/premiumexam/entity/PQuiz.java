package com.schoolonline2020.ozatapp.premiumexam.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pquiz")
public class PQuiz implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="duration")
    private int duration;
    @Column(name="priority")
    private double priority;
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="pexam_id")
    private PExam pExam;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="pQuiz", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<PQuestion> pQuestions;
    @Transient
    protected Object[] jdoDetachedState;

    public PQuiz() {
    }

    public PQuiz(int duration, double priority) {
        this.duration = duration;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public PExam getpExam() {
        return pExam;
    }

    public void setpExam(PExam pExam) {
        this.pExam = pExam;
    }

    public void addPQuestion(PQuestion pQuestion) {
        if(pQuestions == null) {
            pQuestions = new ArrayList<>();
        }
        pQuestions.add(pQuestion);
        pQuestion.setpQuiz(this);
    }

    public List<PQuestion> getpQuestions() {
        return pQuestions;
    }

    public void setpQuestions(List<PQuestion> pQuestions) {
        this.pQuestions = pQuestions;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
    
}