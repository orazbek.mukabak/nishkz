package com.schoolonline2020.ozatapp.premiumexam.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="presponse")
public class PResponse implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="answer")
    private String answer;
    @Column(name="pquestion_id")
    private int pQuestionId;
    @Column(name="score")
    private int score;
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="pparticipant_id")
    private PParticipant pParticipant;
    
    public PResponse() {
    }

    public PResponse(String answer, int pQuestionId, int score, PParticipant pParticipant) {
        this.answer = answer;
        this.pQuestionId = pQuestionId;
        this.score = score;
        this.pParticipant = pParticipant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getpQuestionId() {
        return pQuestionId;
    }

    public void setpQuestionId(int pQuestionId) {
        this.pQuestionId = pQuestionId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public PParticipant getpParticipant() {
        return pParticipant;
    }

    public void setpParticipant(PParticipant pParticipant) {
        this.pParticipant = pParticipant;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}