package com.schoolonline2020.ozatapp.premiumexam.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pparticipant")
public class PParticipant implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="username")
    private String username;
    @Column(name="pquiz_id")
    private int pQuizId;
    @Column(name="status")
    private int status;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="pParticipant", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<PResponse> pResponses;
    @Transient
    protected Object[] jdoDetachedState;

    public PParticipant() {
    }

    public PParticipant(String username, int pQuizId, int status) {
        this.username = username;
        this.pQuizId = pQuizId;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getpQuizId() {
        return pQuizId;
    }

    public void setpQuizId(int pQuizId) {
        this.pQuizId = pQuizId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void addPResponse(PResponse pResponse) {
        if(pResponses == null) {
            pResponses = new ArrayList<>();
        }
        pResponses.add(pResponse);
        pResponse.setpParticipant(this);
    }

    public List<PResponse> getpResponses() {
        return pResponses;
    }

    public void setpResponses(List<PResponse> pResponses) {
        this.pResponses = pResponses;
    }
    
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}