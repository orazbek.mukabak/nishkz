package com.schoolonline2020.ozatapp.premiumexam.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="pexam")
public class PExam implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="description")
    private String description;
    @Column(name="priority")
    private double priority;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="pExam", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<PQuiz> pQuizzes;
    @Column(name="status")
    private char status;
    @Transient
    protected Object[] jdoDetachedState;

    public PExam() {
    }

    public PExam(String title, String description, double priority, char status) {
        this.title = title;
        this.description = description;
        this.priority = priority;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public void addPQuiz(PQuiz pQuiz) {
        if(pQuizzes == null) {
            pQuizzes = new ArrayList<>();
        }
        pQuizzes.add(pQuiz);
        pQuiz.setpExam(this);
    }

    public List<PQuiz> getpQuizzes() {
        return pQuizzes;
    }

    public void setpQuizzes(List<PQuiz> pQuizzes) {
        this.pQuizzes = pQuizzes;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}