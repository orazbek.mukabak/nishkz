package com.schoolonline2020.ozatapp.premiumexam.dao;

import com.schoolonline2020.ozatapp.premiumexam.entity.PExam;
import com.schoolonline2020.ozatapp.premiumexam.entity.PParticipant;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuestion;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuiz;
import com.schoolonline2020.ozatapp.premiumexam.entity.PResponse;
import java.util.List;

public interface PremiumexamDAO {

    public List<PExam> findAllPexams();
    public PExam findPexamById(int id);
    public List<PQuiz> findPquizzesByPexamId(int pexamId);
    public List<PQuestion> findPquestionsByPquizId(int pquizId);
    public PQuiz findPQuizById(int id);
    public String save(PParticipant pParticipant);
    public PParticipant findPParticipantByPQuizIdAndUsername(int pQuizId, String username);
    public PParticipant findPParticipantById(int id);
    public PQuestion findPQuestionById(int id);
    public String save(int pParticipantId, PResponse pResponse);
    public PResponse findPResponseByPParticipantAndPQuestionId(PParticipant pParticipant, int pQuestionId);
    public String save(int pExamId, PQuiz pQuiz);
    public String deletePQuiz(int id);
    public String save(int pQuizId, PQuestion pQuestion);
    public String deletePQuestion(int id);
    public String deletePParticipant(int id);

}