package com.schoolonline2020.ozatapp.premiumexam.dao;

import com.schoolonline2020.ozatapp.premiumexam.entity.PExam;
import com.schoolonline2020.ozatapp.premiumexam.entity.PParticipant;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuestion;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuiz;
import com.schoolonline2020.ozatapp.premiumexam.entity.PResponse;
import java.util.List;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

@Repository
public class PremiumexamDAOImplementation implements PremiumexamDAO {

    @Autowired
    private SessionFactory premiumexamSessionFactory;
    
    @Override
    public List<PExam> findAllPexams() {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(PExam.class);
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("description"), "description")
        .add(Projections.property("priority"), "priority")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(PExam.class));
        return query.list();
    }
    
    @Override
    public PExam findPexamById(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        return currentSession.get(PExam.class, id);
    }

    @Override
    public List<PQuiz> findPquizzesByPexamId(int pexamId) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        PExam pexam = currentSession.get(PExam.class, pexamId);
        Criteria query = currentSession.createCriteria(PQuiz.class);
        query.add(Restrictions.eq("pExam", pexam));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("duration"), "duration")
        .add(Projections.property("priority"), "priority"))
        .setResultTransformer(Transformers.aliasToBean(PQuiz.class));
        return query.list();
    }
    
    @Override
    public List<PQuestion> findPquestionsByPquizId(int pquizId) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        PQuiz pquiz = currentSession.get(PQuiz.class, pquizId);
        Criteria query = currentSession.createCriteria(PQuestion.class);
        query.add(Restrictions.eq("pQuiz", pquiz));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("priority"), "priority")
        .add(Projections.property("text"), "text")
        .add(Projections.property("answerA"), "answerA")
        .add(Projections.property("answerB"), "answerB")
        .add(Projections.property("answerC"), "answerC")
        .add(Projections.property("answerD"), "answerD")
        .add(Projections.property("answerE"), "answerE")
        .add(Projections.property("correctAnswer"), "correctAnswer")
        .add(Projections.property("level"), "level")
        .add(Projections.property("solution"), "solution"))
        .setResultTransformer(Transformers.aliasToBean(PQuestion.class));
        return query.list();
    }
    
    @Override
    public PQuiz findPQuizById(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        return currentSession.get(PQuiz.class, id);
    }
    
    @Override
    public String save(PParticipant pParticipant) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(pParticipant);
        return "Done! PParticipant is saved!";
    }

    @Override
    public PParticipant findPParticipantByPQuizIdAndUsername(int pQuizId, String username) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(PParticipant.class);
        query.add(Restrictions.eq("pQuizId", pQuizId));
        query.add(Restrictions.eq("username", username));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("pQuizId"), "pQuizId")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(PParticipant.class));
        return (PParticipant)query.uniqueResult();
    }

    @Override
    public PParticipant findPParticipantById(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        return currentSession.get(PParticipant.class, id);
    }

    @Override
    public PQuestion findPQuestionById(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        return currentSession.get(PQuestion.class, id);
    }

    @Override
    public String save(int pParticipantId, PResponse pResponse) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        PParticipant pParticipant = currentSession.get(PParticipant.class, pParticipantId);
        pParticipant.addPResponse(pResponse);
        pResponse.setpParticipant(pParticipant);
        currentSession.saveOrUpdate(pResponse);
        return "Done! PResponse is saved!";
    }

    @Override
    public PResponse findPResponseByPParticipantAndPQuestionId(PParticipant pParticipant, int pQuestionId) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(PResponse.class);
        query.add(Restrictions.eq("pQuestionId", pQuestionId));
        query.add(Restrictions.eq("pParticipant", pParticipant));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("answer"), "answer")
        .add(Projections.property("score"), "score"))
        .setResultTransformer(Transformers.aliasToBean(PResponse.class));
        return (PResponse)query.uniqueResult();
    }

    @Override
    public String save(int pExamId, PQuiz pQuiz) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        PExam pExam = currentSession.get(PExam.class, pExamId);
        pExam.addPQuiz(pQuiz);
        pQuiz.setpExam(pExam);
        currentSession.saveOrUpdate(pQuiz);
        return "Done! PQuiz is saved!";
    }

    @Override
    public String deletePQuiz(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from PQuiz where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! PQuiz is deleted!";
    }

    @Override
    public String save(int pQuizId, PQuestion pQuestion) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        PQuiz pQuiz = currentSession.get(PQuiz.class, pQuizId);
        pQuiz.addPQuestion(pQuestion);
        pQuestion.setpQuiz(pQuiz);
        currentSession.saveOrUpdate(pQuestion);
        return "Done! PQuestion is saved!";
    }

    @Override
    public String deletePQuestion(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from PQuestion where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! PQuestion is deleted!";
    }

    @Override
    public String deletePParticipant(int id) {
        Session currentSession = premiumexamSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from PParticipant where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! PParticipant is deleted!";
    }
}