package com.schoolonline2020.ozatapp.premiumexam.service;

import com.schoolonline2020.ozatapp.premiumexam.dao.PremiumexamDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.schoolonline2020.ozatapp.premiumexam.entity.PExam;
import com.schoolonline2020.ozatapp.premiumexam.entity.PParticipant;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuestion;
import com.schoolonline2020.ozatapp.premiumexam.entity.PQuiz;
import com.schoolonline2020.ozatapp.premiumexam.entity.PResponse;

@Service
public class PremiumexamServiceImplementation implements PremiumexamService {

    @Autowired
    private PremiumexamDAO premiumexamDAO;

    @Override
    @Transactional("premiumexamTransactionManager")
    public List<PExam> findAllPexams() {
        return premiumexamDAO.findAllPexams();
    }
    
    @Override
    @Transactional("premiumexamTransactionManager")
    public PExam findPexamById(int id) {
        return premiumexamDAO.findPexamById(id);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public List<PQuiz> findPquizzesByPexamId(int pexamId) {
        return premiumexamDAO.findPquizzesByPexamId(pexamId);
    }
    
    @Override
    @Transactional("premiumexamTransactionManager")
    public List<PQuestion> findPquestionsByPquizId(int pquizId) {
        return premiumexamDAO.findPquestionsByPquizId(pquizId);
    }
    
    @Override
    @Transactional("premiumexamTransactionManager")
    public PQuiz findPQuizById(int id) {
        return premiumexamDAO.findPQuizById(id);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String save(PParticipant pParticipant) {
        return premiumexamDAO.save(pParticipant);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public PParticipant findPParticipantByPQuizIdAndUsername(int pQuizId, String username) {
        return premiumexamDAO.findPParticipantByPQuizIdAndUsername(pQuizId, username);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public PParticipant findPParticipantById(int id) {
        return premiumexamDAO.findPParticipantById(id);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public PQuestion findPQuestionById(int id) {
        return premiumexamDAO.findPQuestionById(id);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String save(int pParticipantId, PResponse pResponse) {
        return premiumexamDAO.save(pParticipantId, pResponse);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public PResponse findPResponseByPParticipantAndPQuestionId(PParticipant pParticipant, int pQuestionId) {
        return premiumexamDAO.findPResponseByPParticipantAndPQuestionId(pParticipant, pQuestionId);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String save(int pExamId, PQuiz pQuiz) {
        return premiumexamDAO.save(pExamId, pQuiz);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String deletePQuiz(int id) {
        return premiumexamDAO.deletePQuiz(id);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String save(int pQuizId, PQuestion pQuestion) {
        return premiumexamDAO.save(pQuizId, pQuestion);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String deletePQuestion(int id) {
        return premiumexamDAO.deletePQuestion(id);
    }

    @Override
    @Transactional("premiumexamTransactionManager")
    public String deletePParticipant(int id) {
        return premiumexamDAO.deletePParticipant(id);
    }
}