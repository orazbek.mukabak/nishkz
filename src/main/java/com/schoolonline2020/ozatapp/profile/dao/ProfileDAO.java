package com.schoolonline2020.ozatapp.profile.dao;

import java.util.List;
import com.schoolonline2020.ozatapp.profile.entity.Profile;

public interface ProfileDAO {

    public List<Profile> findAll();
    public String save(Profile profile);
    public Profile findProfileById(String username);
    public void delete(String username);
    public List<Profile> findProfileByIdJSON(String username);
}