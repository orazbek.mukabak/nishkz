package com.schoolonline2020.ozatapp.profile.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.schoolonline2020.ozatapp.profile.entity.Profile;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

@Repository
public class ProfileDAOImplementation implements ProfileDAO {
    // need to inject the session factory
    @Autowired
    private SessionFactory profileSessionFactory;

    @Override
    public List<Profile> findAll() {
        // get the current hibernate session
        Session currentSession = profileSessionFactory.getCurrentSession();
        // create a query  ... sort by name
        Query<Profile> theQuery = currentSession.createQuery("from Profile order by firstName", Profile.class);
        // execute query and get result list
        List<Profile> profiles = theQuery.getResultList();
        // return the results		
        return profiles;
    }

    @Override
    public String save(Profile profile) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(profile);
        return "Done! Profile is saved!";
    }

    @Override
    public Profile findProfileById(String username) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        Profile profile = (Profile)currentSession.createQuery("select new Profile(username,innerId,firstName,lastName,gender,email,aboutMe,birthDate,regDate) from Profile where username=:username").setString("username", username).uniqueResult();
        return profile;
    }

    @Override
    public void delete(String username) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Profile where username=:username");
        theQuery.setParameter("username", username);
        theQuery.executeUpdate();		
    }
    
    public List<Profile> findProfileByIdJSON(String username) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Profile.class);
        query.add(Restrictions.eq("username", username));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("username"), "username")
        .add(Projections.property("innerId"), "innerId")
        .add(Projections.property("firstName"), "firstName")
        .add(Projections.property("lastName"), "lastName")
        .add(Projections.property("gender"), "gender")
        .add(Projections.property("email"), "email")
        .add(Projections.property("aboutMe"), "aboutMe")
        .add(Projections.property("birthDate"), "birthDate")
        .add(Projections.property("regDate"), "regDate"))
        .setResultTransformer(Transformers.aliasToBean(Profile.class));
        List<Profile> list = new ArrayList<Profile>();
        list.add((Profile)query.uniqueResult());
        return list;
    }

}