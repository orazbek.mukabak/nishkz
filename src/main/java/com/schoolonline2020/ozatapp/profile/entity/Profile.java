package com.schoolonline2020.ozatapp.profile.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="profile")
public class Profile implements Serializable {
    @Id
    @Column(name="username")
    private String username;
    @Column(name="inner_id")
    private int innerId;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="gender")
    private boolean gender;
    @Column(name="email")
    private String email;
    @Column(name="about_me")
    private String aboutMe;
    @Column(name="birth_date")
    private Date birthDate;
    @Column(name="reg_date")
    private Date regDate;
    
    @Transient
    private double balance;
    @Transient
    protected Object[] jdoDetachedState;
    
    public Profile() {
    }
    
    public Profile(String username, String firstName, String lastName, boolean gender, String email, String aboutMe, Date birthDate, Date regDate) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.aboutMe = aboutMe;
        this.birthDate = birthDate;
        this.regDate = regDate;
    }
    
    public Profile(String username, int innerId, String firstName, String lastName, boolean gender, String email, String aboutMe, Date birthDate, Date regDate) {
        this.username = username;
        this.innerId = innerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.aboutMe = aboutMe;
        this.birthDate = birthDate;
        this.regDate = regDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getInnerId() {
        return innerId;
    }

    public void setInnerId(int innerId) {
        this.innerId = innerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}