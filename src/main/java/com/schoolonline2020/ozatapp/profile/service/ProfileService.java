package com.schoolonline2020.ozatapp.profile.service;

import java.util.List;
import com.schoolonline2020.ozatapp.profile.entity.Profile;

public interface ProfileService {

    public List<Profile> findAll();

    public String save(Profile profile);

    public Profile findProfileById(String username);

    public void delete(String username);

    public List<Profile> findProfileByIdJSON(String username);

    public Profile createProfile(String username);
}