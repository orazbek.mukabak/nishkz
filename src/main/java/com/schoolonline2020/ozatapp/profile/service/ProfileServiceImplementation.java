package com.schoolonline2020.ozatapp.profile.service;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.schoolonline2020.ozatapp.profile.dao.ProfileDAO;
import com.schoolonline2020.ozatapp.profile.entity.Profile;
import java.util.Date;

@Service
public class ProfileServiceImplementation implements ProfileService {
    // need to inject customer dao
    @Autowired
    private ProfileDAO profileDAO;

    @Override
    @Transactional("profileTransactionManager")
    public List<Profile> findAll() {
        return profileDAO.findAll();
    }

    @Override
    @Transactional("profileTransactionManager")
    public String save(Profile profile) {
        return profileDAO.save(profile);
    }

    @Override
    @Transactional("profileTransactionManager")
    public Profile findProfileById(String username) {
        return profileDAO.findProfileById(username);
    }

    @Override
    @Transactional("profileTransactionManager")
    public void delete(String username) {
        profileDAO.delete(username);
    }
    
    @Override
    @Transactional("profileTransactionManager")
    public List<Profile> findProfileByIdJSON(String username) {
        return profileDAO.findProfileByIdJSON(username);
    }

    @Override
    @Transactional("profileTransactionManager")
    public Profile createProfile(String username) {
        Profile profile = profileDAO.findProfileById(username);
        if(profile == null) {
            Date regDate = new Date();
            save(new Profile(username, "", "", false, "", "", regDate, regDate));
            profile = profileDAO.findProfileById(username);
            TelegramBot bot = new TelegramBot("1187575630:AAFkNYMmIwb1N6uE5UtOgMye8ILY3maqIA0");
            bot.setUpdatesListener(updates -> {
                return UpdatesListener.CONFIRMED_UPDATES_ALL;
            });
            String message = "New user: " + username;
            SendResponse response = bot.execute(new SendMessage(-307305224, message));
        }
        return profileDAO.findProfileById(username);
    }
}