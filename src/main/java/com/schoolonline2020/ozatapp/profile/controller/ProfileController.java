package com.schoolonline2020.ozatapp.profile.controller;

import com.schoolonline2020.ozatapp.payment.entity.Account;
import com.schoolonline2020.ozatapp.payment.service.PaymentService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.schoolonline2020.ozatapp.profile.entity.Profile;
import com.schoolonline2020.ozatapp.profile.service.ProfileService;
import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.service.QuizService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    // need to inject our profile service
    @Autowired
    private ProfileService profileService;

    @Autowired
    private PaymentService paymentService;
    
    @Autowired
    private QuizService quizService;
    
    @GetMapping(value = "/my-profile")
    public String displayMyProfilePage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Profile profile = profileService.findProfileById(username);
        if(profile == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("profile", profile);
            return "profile/my-profile";
        }
    }
    
    @GetMapping(path = "/view/{username:.+}")
    public String displayViewPage(@PathVariable String username, Model model) {
        Profile profile = profileService.findProfileById(username);
        if(profile == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("profile", profile);
            return "profile/view";
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @GetMapping("/view-all")
    public String displayViewAllPage(Model model) {
        // get profiles from the service
        List<Profile> profiles = profileService.findAll();
        // add the profiles to the model
        model.addAttribute("profiles", profiles);
        return "profile/view-all";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("profile") Profile profile) {
        profileService.save(profile);	
        return "redirect:/profile/view-all";
    }
    
    @GetMapping("/update")
    public String update(@RequestParam("username") String username, Model model) {
        // get the profile from service
        Profile profile = profileService.findProfileById(username);	
        // set profile as a model attribute to pre-populate the form
        model.addAttribute("profile", profile);		
        return "profile/view-profile-old";
    }

    @GetMapping("/delete")
    public String deleteCustomer(@RequestParam("username") String username) {
        // delete the profile
        profileService.delete(username);
        return "redirect:/profile/view-all";
    }

    @GetMapping(path="/view-basic-info/{username:.+}")
    public String displayViewProfilePage(@PathVariable String username, Model model) {
        Profile profile = profileService.findProfileById(username);
        model.addAttribute("profile", profile);
        return "profile/view-basic-info-jsp";
    }
    
    @GetMapping(path="/view-picture/{username:.+}")
    public String displayViewPicturePage(@PathVariable String username, Model model) {
        Profile profile = profileService.findProfileById(username);
        model.addAttribute("profile", profile);
        return "profile/view-picture-jsp";
    }
    
    @GetMapping(path="/edit-basic-info/{username:.+}")
    public String displayEditBasicInfoPage(@PathVariable String username, Model model) {
        Profile profile = profileService.findProfileById(username);
        model.addAttribute("profile", profile);
        return "profile/edit-basic-info-jsp";
    }
    
    @GetMapping(path="/edit-picture/{username:.+}")
    public String displayEditPicturePage(@PathVariable String username, Model model) {
        Profile profile = profileService.findProfileById(username);
        model.addAttribute("username", profile.getUsername());
        model.addAttribute("innerId", profile.getInnerId());
        return "profile/edit-picture-jsp";
    }
    
    @GetMapping(path="/view-profile-menu")
    public String displayViewProfileMenuJspPage() {
        return "profile/view-profile-menu-jsp";
    }

    @GetMapping(path="/save-basic-info/{username:.+}")
    public String saveBasicInfoProcess(
        @PathVariable String username,
        @RequestParam("firstName") String firstName,
        @RequestParam("lastName") String lastName,
        @RequestParam("gender") boolean gender,
        @RequestParam("birthDate") String strBirthDate,
        @RequestParam("email") String email,
        @RequestParam("aboutMe") String aboutMe) {
        Profile profile = profileService.findProfileById(username);
        // Second try get profile
        if(profile == null) {
            profile = profileService.findProfileById(username);
        }
        
        if(profile.getFirstName().equals(firstName) && profile.getFirstName().equals(lastName)) {
        } else {
            List<Participant> participants = quizService.findParticipantsByUsername(username);
            for(Participant participant:participants) {
                participant.setFirstName(firstName);
                participant.setLastName(lastName);
                quizService.save(participant);
            }
        }
        
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setGender(gender);
        profile.setEmail(email);
        profile.setAboutMe(aboutMe);
        Date birthDate;
        try {
            birthDate = new SimpleDateFormat("yyyy-MM-dd").parse(strBirthDate);
            profile.setBirthDate(birthDate);
        } catch (ParseException ex) {}
        return profileService.save(profile);
    }
    
    @GetMapping(value = "/get-profile-info")
    public @ResponseBody
    List getProfileInfo(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Profile profile = profileService.findProfileById(username);
        List<Profile> profiles = profileService.findProfileByIdJSON(username);
        profiles.get(0).setBalance(paymentService.findAccountByInnerId(profile.getInnerId()).getBalance());
        return profiles;
    }
    
    @GetMapping(path="/update-names/{username:.+}")
    public String updateNames(
        @PathVariable String username,
        @RequestParam("firstName") String firstName,
        @RequestParam("lastName") String lastName) {
        Profile profile = profileService.findProfileById(username);
        
        if(profile.getFirstName().equals(firstName) && profile.getFirstName().equals(lastName)) {
        } else {
            List<Participant> participants = quizService.findParticipantsByUsername(username);
            for(Participant participant:participants) {
                participant.setFirstName(firstName);
                participant.setLastName(lastName);
                quizService.save(participant);
            }
        }
                
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        return profileService.save(profile);
    }
    
    @GetMapping(value = "/get-profile-by-username/{username:.+}")
    public @ResponseBody
    List getProfileByUsername(@PathVariable String username){
        profileService.findProfileById(username);
        List<Profile> profiles = profileService.findProfileByIdJSON(username);
        return profiles;
    }
    
    
   //////////////////////////////////////////////////////////////// 
    
    

    
}