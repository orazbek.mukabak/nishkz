package com.schoolonline2020.ozatapp.sms.dao;

import com.schoolonline2020.ozatapp.sms.entity.PasswordRecovery;

public interface SmsDAO {

    public String savePasswordRecovery(PasswordRecovery passwordRecovery);
    public PasswordRecovery findPasswordRecovery(String username);
    
}