package com.schoolonline2020.ozatapp.sms.dao;

import com.schoolonline2020.ozatapp.quiz.dao.*;
import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.entity.Question;
import com.schoolonline2020.ozatapp.quiz.entity.Quiz;
import com.schoolonline2020.ozatapp.quiz.entity.Response;
import com.schoolonline2020.ozatapp.sms.entity.PasswordRecovery;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

@Repository
public class SmsDAOImplementation implements SmsDAO {

    @Autowired
    private SessionFactory smsSessionFactory;
   
    @Override
    public String savePasswordRecovery(PasswordRecovery passwordRecovery) {
        Session currentSession = smsSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(passwordRecovery);
        return "Done! PasswordRecovery is saved!";
    }

    @Override
    public PasswordRecovery findPasswordRecovery(String username) {
        Session currentSession = smsSessionFactory.getCurrentSession();
        return currentSession.get(PasswordRecovery.class, username);
    }
    
}