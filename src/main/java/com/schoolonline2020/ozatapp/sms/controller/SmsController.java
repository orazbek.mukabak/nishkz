package com.schoolonline2020.ozatapp.sms.controller;

import com.schoolonline2020.ozatapp.sms.entity.PasswordRecovery;
import com.schoolonline2020.ozatapp.sms.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private SmsService smsService;

    @GetMapping(path = "/add-password-recovery")
    public String addPasswordRecovery(@RequestParam("username") String username, @RequestParam("code") int code) {
        PasswordRecovery passwordRecovery = new PasswordRecovery();
        return smsService.savePasswordRecovery(passwordRecovery);
    }
}