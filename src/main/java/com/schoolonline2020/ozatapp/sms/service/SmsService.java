package com.schoolonline2020.ozatapp.sms.service;

import com.schoolonline2020.ozatapp.sms.entity.PasswordRecovery;

public interface SmsService {
    
    public String savePasswordRecovery(PasswordRecovery passwordRecovery);
    public PasswordRecovery findPasswordRecovery(String username);
}