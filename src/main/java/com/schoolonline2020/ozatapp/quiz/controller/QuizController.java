package com.schoolonline2020.ozatapp.quiz.controller;

import com.schoolonline2020.ozatapp.course.entity.Lesson;
import com.schoolonline2020.ozatapp.course.service.CourseService;
import com.schoolonline2020.ozatapp.payment.entity.Purchase;
import com.schoolonline2020.ozatapp.payment.service.PaymentService;
import com.schoolonline2020.ozatapp.profile.entity.Profile;
import com.schoolonline2020.ozatapp.profile.service.ProfileService;
import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.entity.Question;
import com.schoolonline2020.ozatapp.quiz.entity.Quiz;
import com.schoolonline2020.ozatapp.quiz.entity.Response;
import com.schoolonline2020.ozatapp.quiz.service.QuizService;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/quiz")
public class QuizController {

    @Autowired
    private QuizService quizService;
    
    @Autowired
    private CourseService courseService;
    
    @Autowired
    private PaymentService paymentService;
    
    @Autowired
    private ProfileService profileService;
    
    @GetMapping(path="/display-view-lesson-quiz/{lessonId:.+}")
    public String displayViewLessonQuizJspPage(@PathVariable("lessonId") int lessonId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        if(username.equals("anonymousUser")) {
            model.addAttribute("command", "COMMAND.REFRESH");
            return "command";
        } else {
            Quiz quiz = quizService.findQuizByLessonId(lessonId);
            if(quiz == null) {
                Quiz newQuiz = new Quiz(60, "", 0, null, lessonId);
                quizService.save(newQuiz);
                quiz = quizService.findQuizByLessonId(lessonId);
            }
            int quizId = quiz.getId();
            Participant participant = quizService.findParticipantByQuizIdAndUsername(quizId, username);
            if(participant == null) {
                Lesson lesson = courseService.findLessonById(lessonId);
                
                List<Question> questions = quizService.findQuestionsByQuizId(quizId);
                // Second try get questions
                if(questions == null) {
                    questions = quizService.findQuestionsByQuizId(quizId);
                }
                
                Profile profile = profileService.findProfileById(username);
                // Second try get profile
                if(profile == null) {
                    profile = profileService.findProfileById(username);
                }
                
                if(quiz.getDeadlineAllowed() == 0) {
                    quiz.setDeadlineValue(null);
                }
                
                Participant newParticipant = new Participant(username, profile.getInnerId(), profile.getFirstName(), profile.getLastName(), quizId, quiz.getDeadlineAllowed(), quiz.getDeadlineValue(), null, null, lesson.getCourse().getId(), lessonId, questions.size(), 0, 0, 0);
                quizService.save(newParticipant);
                participant = quizService.findParticipantByQuizIdAndUsername(quizId, username);
            }
            model.addAttribute("quiz", quiz);
            model.addAttribute("participant", participant);
            int status = participant.getStatus();
            if(status == 2) {
                return "quiz/view-quiz-result-jsp";
            } else if(status == 1) {
                return "quiz/view-quiz-jsp";
            } else {
                return "quiz/view-quiz-start-jsp";
            }
        }
    }
    
    @GetMapping(path="/display-view-quiz/{id:.+}")
    public String displayViewQuizJspPage(@PathVariable("id") int id, Model model) {
        Participant participant = quizService.findParticipantById(id);
        participant.setStatus(1);
        ZoneId zonedId = ZoneId.of("Asia/Almaty");
        participant.setStartDateTime(LocalDateTime.now(zonedId));
        quizService.save(participant);
        Quiz quiz = quizService.findQuizById(participant.getQuizId());
        model.addAttribute("quiz", quiz);
        model.addAttribute("participant", participant);
        return "quiz/view-quiz-jsp";
    }
    
    @GetMapping(value="/get-questions-by-quiz-id/")
    public @ResponseBody
    List getQuestionsByQuizId(@RequestParam("id") int quizId){
        return quizService.findQuestionsByQuizId(quizId);
    }
    
    @GetMapping(path="/display-add-question/{id:.+}")
    public String displayAddQuestionJspPage(@PathVariable("id") int id, Model model) {
        Quiz quiz = quizService.findQuizById(id);
        model.addAttribute("quiz", quiz);
        return "quiz/add-question";
    }
    
    @PostMapping(value = "/add-question")
    @ResponseBody
    public String addQuestion(@RequestBody Question question, @RequestParam("quizId") int quizId) {
        return quizService.saveQuestion(quizId, question);
    }
    
    @GetMapping(path = "/add-response")
    public String addResponse(@RequestParam("answer") String answer, @RequestParam("questionId") int questionId, @RequestParam("participantId") int participantId, Model model) {
        Response response = quizService.findResponseByParticipantAndQuestionId(participantId, questionId);
        Question question = quizService.findQuestionById(questionId);
        //SECOND TRY FOR THE RESPONSE
        if(response == null) {
            response = quizService.findResponseByParticipantAndQuestionId(participantId, questionId);
        }
        //SECOND TRY FOR THE QUESTION
        if(question == null) {
            question = quizService.findQuestionById(questionId);
        }
        if(question == null) {
            model.addAttribute("status", "STATUS.QUESTION_NOT_FOUND");
            return "status";
        } else {
            Participant participant = quizService.findParticipantById(participantId);
            int score = 0;
            if(answer.equals(question.getCorrectAnswer())) {
                score = 1;
            }
            if(response == null) {
                response = new Response(answer, questionId, score, participant);
            } else {
                response.setAnswer(answer);
                response.setScore(score);
            }
            String status = quizService.saveResponse(participantId, response);
            model.addAttribute("status", status);
            return "status";
        }
    }
    
    @GetMapping(path="/submit-responses/{id:.+}")
    public String submitResponses(@PathVariable("id") int id, Model model) {
        Participant participant = quizService.findParticipantById(id);
        participant.setStatus(2);
        ZoneId zonedId = ZoneId.of("Asia/Almaty");
        participant.setEndDateTime(LocalDateTime.now(zonedId));
        
        List<Question> questions = quizService.findQuestionsByQuizId(participant.getQuizId());
        // Second try get questions
        if(questions == null) {
            questions = quizService.findQuestionsByQuizId(participant.getQuizId());
        }
        participant.setTotalQuestion(questions.size());

        List<Response> responses = quizService.findResponsesByParticipantId(participant.getId());
        // Second try get responses
        if(responses == null) {
            responses = quizService.findResponsesByParticipantId(participant.getId());
        }
        int counter = 0;
        for(Response response:responses) {
            Question question = quizService.findQuestionById(response.getQuestionId());
            // Second try get question
            if(question == null) {
                question = quizService.findQuestionById(response.getQuestionId());
            }
            if(response.getAnswer().equals(question.getCorrectAnswer())) {
                counter ++;
            }
        }
        participant.setCorrectAnswers(counter);

        quizService.save(participant);
        
        Quiz quiz = quizService.findQuizById(participant.getQuizId());
        model.addAttribute("participant", participant);
        model.addAttribute("quiz", quiz);
        return "quiz/view-quiz-result-jsp";
    }
    
    @GetMapping(value="/get-responses-by-participant-id/")
    public @ResponseBody
    List getResponsesByParticipantId(@RequestParam("participantId") int participantId){
        return quizService.findResponsesByParticipantId(participantId);
    }
    
    @GetMapping(value="/get-correct-answers-by-quiz-id/")
    public @ResponseBody
    List getCorrectAnswersByQuizId(@RequestParam("quizId") int quizId){
        return quizService.findCorrectAnswersByQuizId(quizId);
    }
    
    @GetMapping(path="/delete-question/{id:.+}")
    public String deleteQuestionProcess(@PathVariable int id) {
        return quizService.deleteQuestion(id);
    }
    
    @GetMapping(path="/view-edit-question/{id:.+}")
    public String displayEditQuestion(@PathVariable("id") int id, Model model) {
        Question question = quizService.findQuestionById(id);
        model.addAttribute("question", question);
        return "quiz/edit-question";
    }
    
    @PostMapping(value = "/edit-question")
    @ResponseBody
    public String editQuestion(@RequestBody Question question) {
        Question tempQuestion = quizService.findQuestionById(question.getId());
        tempQuestion.setText(question.getText());
        tempQuestion.setAnswerA(question.getAnswerA());
        tempQuestion.setAnswerB(question.getAnswerB());
        tempQuestion.setAnswerC(question.getAnswerC());
        tempQuestion.setAnswerD(question.getAnswerD());
        tempQuestion.setAnswerE(question.getAnswerE());
        tempQuestion.setCorrectAnswer(question.getCorrectAnswer());
        tempQuestion.setPriority(question.getPriority());
        tempQuestion.setLevel(question.getLevel());
        tempQuestion.setSolutionTitle(question.getSolutionTitle());
        tempQuestion.setSolution(question.getSolution());
        return quizService.saveQuestion(tempQuestion.getQuiz().getId(), tempQuestion);
    }
    
    @GetMapping(path="/delete-participant/{id:.+}")
    public String deleteParticipantProcess(@PathVariable int id) {
        return quizService.deleteParticipant(id);
    }
    
    @GetMapping(path="/view-edit-quiz/{id:.+}")
    public String displayEditQuiz(@PathVariable("id") int id, Model model) {
        Quiz quiz = quizService.findQuizById(id);
        model.addAttribute("quiz", quiz);
        return "quiz/edit-quiz";
    }
    
    @PostMapping(value = "/edit-quiz")
    @ResponseBody
    public String editQuiz(@RequestBody Quiz quiz) {
        Quiz tempQuiz = quizService.findQuizById(quiz.getId());
        tempQuiz.setDuration(quiz.getDuration());
        tempQuiz.setDescription(quiz.getDescription());
        return quizService.save(tempQuiz);
    }
    
    @GetMapping(path="/update-result/{id:.+}")
    public String updateResultProcess(@PathVariable("id") int id, @RequestParam("totalScore") double totalScore, Model model) {
        Participant participant = quizService.findParticipantById(id);
        participant.setTotalScore(totalScore);
        quizService.save(participant);
        model.addAttribute("status", "UPDATED!");
        return "status";
    }
    
    @GetMapping(path="/view-lesson-quiz-results/{lessonId:.+}")
    public String displayLessonQuizResultsJsp(@PathVariable("lessonId") int lessonId, Model model) {
        Lesson lesson = courseService.findLessonById(lessonId);
        Quiz quiz = quizService.findQuizByLessonId(lessonId);
        model.addAttribute("lesson", lesson);
        model.addAttribute("quiz", quiz);
        return "quiz/view-lesson-quiz-results-jsp";
    }
    
    @GetMapping(value="/update-partisipants-list")
    public String updateParticipantsList(@RequestParam("courseId") int courseId, @RequestParam("quizId") int quizId, Model model){
        List<Purchase> purchases = paymentService.findPurchasesByCourseId(courseId);
        for(Purchase purchase:purchases) {
            Participant participant = quizService.findParticipantByQuizIdAndUsername(quizId, purchase.getUsername());
            if(participant == null) {
                Quiz quiz = quizService.findQuizById(quizId);
                
                List<Question> questions = quizService.findQuestionsByQuizId(quizId);
                // Second try get questions
                if(questions == null) {
                    questions = quizService.findQuestionsByQuizId(quizId);
                }
                
                Profile profile = profileService.findProfileById(purchase.getUsername());
                // Second try get profile
                if(profile == null) {
                    profile = profileService.findProfileById(purchase.getUsername());
                }

                if(quiz.getDeadlineAllowed() == 0) {
                    quiz.setDeadlineValue(null);
                }
                
                quizService.save(new Participant(purchase.getUsername(), profile.getInnerId(), profile.getFirstName(), profile.getLastName(), quizId, quiz.getDeadlineAllowed(), quiz.getDeadlineValue(), null, null, courseId, quiz.getLessonId(), questions.size(), 0, 0, 0));
            }
        }
        model.addAttribute("status", "STATUS.UPDATED");
        return "status";
    }
    
    @GetMapping(value="/get-participants/")
    public @ResponseBody
    List getParticipants(@RequestParam("quizId") int quizId){
        return quizService.findParticipantsByQuizId(quizId);
    }
    
    @GetMapping(value="/get-participants-by-course-id/")
    public @ResponseBody
    List getParticipantsByCourseId(@RequestParam("courseId") int courseId){
        return quizService.findParticipantsByCourseId(courseId);
    }

    @GetMapping(value = "/update-info/")
    public @ResponseBody
    List updateCourseId(){
        System.out.println("SSSSSSSSSSSSSSSSSSSTTTTTTTTTTTTTTTTAAAAAAAAAAAAAAARRRRRRRRRRRRRRRRRRRTTTTTTTTTTTTTTTT");
        List<Participant> participants = quizService.findAllParticipants();
        for(Participant participant:participants) {
//            Profile profile = profileService.findProfileById(participant.getUsername());
//            // Second try get profile
//            if(profile == null) {
//                profile = profileService.findProfileById(participant.getUsername());
//            }
//            if(profile == null) {
//                System.out.println("EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRR");
//                System.out.println("PARTICIPANT ID: " + participant.getUsername());
//            }
//
//            participant.setFirstName(profile.getFirstName());
//            participant.setLastName(profile.getLastName());
            Quiz quiz = quizService.findQuizById(participant.getQuizId());
            // Second try get profile
            if(quiz == null) {
                quiz = quizService.findQuizById(participant.getQuizId());
            }
            if(quiz == null) {
                System.out.println("EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOORRRRRRRRRRRRRRRRRRR");
                System.out.println("PARTICIPANT ID: " + participant.getUsername());
            }
            
            if(quiz.getDeadlineAllowed() == 0) {
                quiz.setDeadlineValue(null);
            }
            participant.setDeadlineAllowed(quiz.getDeadlineAllowed());
            participant.setDeadlineValue(quiz.getDeadlineValue());
            
            String message = "SAVING ERROR PARTICIPANT ID: " + participant.getId() + " EEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOOOOORRRRRRRRRRRRRRRR";
            message = quizService.save(participant);
            System.out.println(message);
        }
        return participants;
    }

}