package com.schoolonline2020.ozatapp.quiz.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="quiz")
public class Quiz implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="duration")
    private int duration;
    @Column(name="description")
    private String description;
    @Column(name="deadline_allowed")
    private int deadlineAllowed;
    @Column(name="deadline_value")
    private LocalDateTime deadlineValue;
    @Column(name="lesson_id")
    private int lessonId;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="quiz", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<Question> questions;
    @Transient
    protected Object[] jdoDetachedState;

    public Quiz() {
    }

    public Quiz(int duration, String description, int deadlineAllowed, LocalDateTime deadlineValue, int lessonId) {
        this.duration = duration;
        this.description = description;
        this.deadlineAllowed = deadlineAllowed;
        this.deadlineValue = deadlineValue;
        this.lessonId = lessonId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDeadlineAllowed() {
        return deadlineAllowed;
    }

    public void setDeadlineAllowed(int deadlineAllowed) {
        this.deadlineAllowed = deadlineAllowed;
    }

    public LocalDateTime getDeadlineValue() {
        return deadlineValue;
    }

    public void setDeadlineValue(LocalDateTime deadlineValue) {
        this.deadlineValue = deadlineValue;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public void addQuestion(Question question) {
        if(questions == null) {
            questions = new ArrayList<>();
        }
        questions.add(question);
        question.setQuiz(this);
    }
    
    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
    
}