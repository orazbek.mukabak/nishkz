package com.schoolonline2020.ozatapp.quiz.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="participant")
public class Participant implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="username")
    private String username;
    @Column(name="inner_id")
    private int innerId;
    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    @Column(name="quiz_id")
    private int quizId;
    @Column(name="deadline_allowed")
    private int deadlineAllowed;
    @Column(name="deadline_value")
    private LocalDateTime deadlineValue;
    @Column(name="start_date_time")
    private LocalDateTime startDateTime;
    @Column(name="end_date_time")
    private LocalDateTime endDateTime;
    @Column(name="course_id")
    private int courseId;
    @Column(name="lesson_id")
    private int lessonId;
    @Column(name="total_question")
    private int totalQuestion;
    @Column(name="total_score")
    private double totalScore;
    @Column(name="correct_answers")
    private int correctAnswers;
    @Column(name="status")
    private int status;
    @OneToMany(fetch=FetchType.LAZY, mappedBy="participant", cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<Response> responses;
    @Transient
    protected Object[] jdoDetachedState;

    public Participant() {
    }

    public Participant(String username, int innerId, String firstName, String lastName, int quizId, int deadlineAllowed, LocalDateTime deadlineValue, LocalDateTime startDateTime, LocalDateTime endDateTime, int courseId, int lessonId, int totalQuestion, double totalScore, int correctAnswers, int status) {
        this.username = username;
        this.innerId = innerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.quizId = quizId;
        this.deadlineAllowed = deadlineAllowed;
        this.deadlineValue = deadlineValue;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.courseId = courseId;
        this.lessonId = lessonId;
        this.totalQuestion = totalQuestion;
        this.totalScore = totalScore;
        this.correctAnswers = correctAnswers;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getInnerId() {
        return innerId;
    }

    public void setInnerId(int innerId) {
        this.innerId = innerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public int getDeadlineAllowed() {
        return deadlineAllowed;
    }

    public void setDeadlineAllowed(int deadlineAllowed) {
        this.deadlineAllowed = deadlineAllowed;
    }

    public LocalDateTime getDeadlineValue() {
        return deadlineValue;
    }

    public void setDeadlineValue(LocalDateTime deadlineValue) {
        this.deadlineValue = deadlineValue;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public int getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(int correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void addResponse(Response response) {
        if(responses == null) {
            responses = new ArrayList<>();
        }
        responses.add(response);
        response.setParticipant(this);
    }
    
    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}