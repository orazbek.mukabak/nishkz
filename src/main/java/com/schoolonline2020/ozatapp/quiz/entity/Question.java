package com.schoolonline2020.ozatapp.quiz.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="question")
public class Question implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="text")
    private String text;
    @Column(name="answer_a")
    private String answerA;
    @Column(name="answer_b")
    private String answerB;
    @Column(name="answer_c")
    private String answerC;
    @Column(name="answer_d")
    private String answerD;
    @Column(name="answer_e")
    private String answerE;
    @Column(name="correct_answer")
    private String correctAnswer;
    @Column(name="priority")
    private int priority;
    @Column(name="level")
    private int level;
    @Column(name="solution_title")
    private int solutionTitle;
    @Column(name="solution")
    private String solution;
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="quiz_id")
    private Quiz quiz;
    
    public Question() {
    }

    public Question(String text, String answerA, String answerB, String answerC, String answerD, String answerE, String correctAnswer, int priority, int level, int solutionTitle, String solution, Quiz quiz) {
        this.text = text;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.answerE = answerE;
        this.correctAnswer = correctAnswer;
        this.priority = priority;
        this.level = level;
        this.solutionTitle = solutionTitle;
        this.solution = solution;
        this.quiz = quiz;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnswerA() {
        return answerA;
    }

    public void setAnswerA(String answerA) {
        this.answerA = answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public void setAnswerB(String answerB) {
        this.answerB = answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public void setAnswerC(String answerC) {
        this.answerC = answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public void setAnswerD(String answerD) {
        this.answerD = answerD;
    }

    public String getAnswerE() {
        return answerE;
    }

    public void setAnswerE(String answerE) {
        this.answerE = answerE;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSolutionTitle() {
        return solutionTitle;
    }

    public void setSolutionTitle(int solutionTitle) {
        this.solutionTitle = solutionTitle;
    }
    
    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}