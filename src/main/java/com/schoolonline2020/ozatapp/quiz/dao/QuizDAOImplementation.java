package com.schoolonline2020.ozatapp.quiz.dao;

import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.entity.Question;
import com.schoolonline2020.ozatapp.quiz.entity.Quiz;
import com.schoolonline2020.ozatapp.quiz.entity.Response;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.transform.Transformers;

@Repository
public class QuizDAOImplementation implements QuizDAO {

    @Autowired
    private SessionFactory quizSessionFactory;
    
    @Override
    public Quiz findQuizByLessonId(int lessonId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Quiz.class);
        query.add(Restrictions.eq("lessonId", lessonId));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("duration"), "duration")
        .add(Projections.property("description"), "description")
        .add(Projections.property("deadlineAllowed"), "deadlineAllowed")
        .add(Projections.property("deadlineValue"), "deadlineValue")
        .add(Projections.property("lessonId"), "lessonId"))
        .setResultTransformer(Transformers.aliasToBean(Quiz.class));
        return (Quiz)query.uniqueResult();
    }
    
    @Override
    public Quiz findQuizById(int id) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        return currentSession.get(Quiz.class, id);
    }
    
    @Override
    public List<Question> findQuestionsByQuizId(int quizId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Quiz quiz = currentSession.get(Quiz.class, quizId);
        Criteria query = currentSession.createCriteria(Question.class);
        query.add(Restrictions.eq("quiz", quiz));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("text"), "text")
        .add(Projections.property("answerA"), "answerA")
        .add(Projections.property("answerB"), "answerB")
        .add(Projections.property("answerC"), "answerC")
        .add(Projections.property("answerD"), "answerD")
        .add(Projections.property("answerE"), "answerE")
        //.add(Projections.property("correctAnswer"), "correctAnswer")
        .add(Projections.property("solutionTitle"), "solutionTitle")
        .add(Projections.property("solution"), "solution")
        .add(Projections.property("level"), "level"))
        .setResultTransformer(Transformers.aliasToBean(Question.class));
        return query.list();
    }
    
    @Override
    public String save(Quiz quiz) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(quiz);
        return "Done! Quiz is saved!";
    }
    
    @Override
    public String saveQuestion(int quizId, Question question) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Quiz quiz = currentSession.get(Quiz.class, quizId);
        quiz.addQuestion(question);
        question.setQuiz(quiz);
        currentSession.saveOrUpdate(question);
        return "Done! Quiz is saved!";
    }
    
    @Override
    public Participant findParticipantByQuizIdAndUsername(int quizId, String username) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Participant.class);
        query.add(Restrictions.eq("quizId", quizId));
        query.add(Restrictions.eq("username", username));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("quizId"), "quizId")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Participant.class));
        return (Participant)query.uniqueResult();
    }
    
    @Override
    public String save(Participant participant) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(participant);
        return "Done! Participant is saved!";
    }
    
    @Override
    public List<Response> findResponsesByParticipantId(int participantId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Participant participant = currentSession.get(Participant.class, participantId);
        Criteria query = currentSession.createCriteria(Response.class);
        query.add(Restrictions.eq("participant", participant));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("answer"), "answer")
        .add(Projections.property("questionId"), "questionId")
        .add(Projections.property("score"), "score"))
        .setResultTransformer(Transformers.aliasToBean(Response.class));
        return query.list();
    }
    
    @Override
    public Participant findParticipantById(int id) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        return currentSession.get(Participant.class, id);
    }
    
    @Override
    public Response findResponseByParticipantAndQuestionId(int participantId, int questionId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Participant participant = currentSession.get(Participant.class, participantId);
        Criteria query = currentSession.createCriteria(Response.class);
        query.add(Restrictions.eq("participant", participant));
        query.add(Restrictions.eq("questionId", questionId));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("answer"), "answer")
        .add(Projections.property("questionId"), "questionId")
        .add(Projections.property("score"), "score"))
        .setResultTransformer(Transformers.aliasToBean(Response.class));
        return (Response)query.uniqueResult();
    }
    
    @Override
    public Question findQuestionById(int id) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        return currentSession.get(Question.class, id);
    }
    
    @Override
    public String saveResponse(int participantId, Response response) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Participant participant = currentSession.get(Participant.class, participantId);
        participant.addResponse(response);
        response.setParticipant(participant);
        currentSession.saveOrUpdate(response);
        return "STATUS.SUCCESSFUL";
    }
    
    @Override
    public List<Question> findCorrectAnswersByQuizId(int quizId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Quiz quiz = currentSession.get(Quiz.class, quizId);
        Criteria query = currentSession.createCriteria(Question.class);
        query.add(Restrictions.eq("quiz", quiz));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("correctAnswer"), "correctAnswer"))
        .setResultTransformer(Transformers.aliasToBean(Question.class));
        return query.list();
    }

    @Override
    public String deleteQuestion(int id) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Question where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Question is deleted!";
    }

    @Override
    public String deleteParticipant(int id) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Participant where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Participant is deleted!";
    }

    @Override
    public List<Participant> findParticipantsByQuizId(int quizId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Participant.class);
        query.add(Restrictions.eq("quizId", quizId));
        query.addOrder(Order.desc("totalScore"));
        query.addOrder(Order.asc("endDateTime"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("innerId"), "innerId")
        .add(Projections.property("firstName"), "firstName")
        .add(Projections.property("lastName"), "lastName")
        .add(Projections.property("quizId"), "quizId")
        .add(Projections.property("deadlineAllowed"), "deadlineAllowed")
        .add(Projections.property("deadlineValue"), "deadlineValue")
        .add(Projections.property("startDateTime"), "startDateTime")
        .add(Projections.property("endDateTime"), "endDateTime")
        .add(Projections.property("courseId"), "courseId")
        .add(Projections.property("lessonId"), "lessonId")
        .add(Projections.property("totalQuestion"), "totalQuestion")
        .add(Projections.property("totalScore"), "totalScore")
        .add(Projections.property("correctAnswers"), "correctAnswers")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Participant.class));
        return query.list();
    }

    @Override
    public List<Participant> findParticipantsByCourseId(int courseId) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Participant.class);
        query.add(Restrictions.eq("courseId", courseId));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("innerId"), "innerId")
        .add(Projections.property("firstName"), "firstName")
        .add(Projections.property("lastName"), "lastName")
        .add(Projections.property("quizId"), "quizId")
        .add(Projections.property("deadlineAllowed"), "deadlineAllowed")
        .add(Projections.property("deadlineValue"), "deadlineValue")
        .add(Projections.property("startDateTime"), "startDateTime")
        .add(Projections.property("endDateTime"), "endDateTime")
        .add(Projections.property("courseId"), "courseId")
        .add(Projections.property("lessonId"), "lessonId")
        .add(Projections.property("totalQuestion"), "totalQuestion")
        .add(Projections.property("totalScore"), "totalScore")
        .add(Projections.property("correctAnswers"), "correctAnswers")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Participant.class));
        return query.list();
    }
        
    @Override
    public List<Participant> findAllParticipants() {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Participant.class);
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("innerId"), "innerId")
        .add(Projections.property("firstName"), "firstName")
        .add(Projections.property("lastName"), "lastName")
        .add(Projections.property("quizId"), "quizId")
        .add(Projections.property("deadlineAllowed"), "deadlineAllowed")
        .add(Projections.property("deadlineValue"), "deadlineValue")
        .add(Projections.property("startDateTime"), "startDateTime")
        .add(Projections.property("endDateTime"), "endDateTime")
        .add(Projections.property("courseId"), "courseId")
        .add(Projections.property("lessonId"), "lessonId")
        .add(Projections.property("totalQuestion"), "totalQuestion")
        .add(Projections.property("totalScore"), "totalScore")
        .add(Projections.property("correctAnswers"), "correctAnswers")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Participant.class));
        return query.list();
    }

    @Override
    public List<Participant> findParticipantsByUsername(String username) {
        Session currentSession = quizSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Participant.class);
        query.add(Restrictions.eq("username", username));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("innerId"), "innerId")
        .add(Projections.property("firstName"), "firstName")
        .add(Projections.property("lastName"), "lastName")
        .add(Projections.property("quizId"), "quizId")
        .add(Projections.property("deadlineAllowed"), "deadlineAllowed")
        .add(Projections.property("deadlineValue"), "deadlineValue")
        .add(Projections.property("startDateTime"), "startDateTime")
        .add(Projections.property("endDateTime"), "endDateTime")
        .add(Projections.property("courseId"), "courseId")
        .add(Projections.property("lessonId"), "lessonId")
        .add(Projections.property("totalQuestion"), "totalQuestion")
        .add(Projections.property("totalScore"), "totalScore")
        .add(Projections.property("correctAnswers"), "correctAnswers")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Participant.class));
        return query.list();
    }

}