package com.schoolonline2020.ozatapp.quiz.service;

import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.entity.Question;
import com.schoolonline2020.ozatapp.quiz.entity.Quiz;
import com.schoolonline2020.ozatapp.quiz.entity.Response;
import java.util.List;
public interface QuizService {

    public Quiz findQuizByLessonId(int lessonId);
    public Quiz findQuizById(int id);
    public List<Question> findQuestionsByQuizId(int quizId);
    public String save(Quiz quiz);
    public String saveQuestion(int quizId, Question question);
    public Participant findParticipantByQuizIdAndUsername(int quizId, String username);
    public String save(Participant participant);
    public List<Response> findResponsesByParticipantId(int participantId);
    public Participant findParticipantById(int id);
    public Response findResponseByParticipantAndQuestionId(int participantId, int questionId);
    public Question findQuestionById(int id);
    public String saveResponse(int participantId, Response response);
    public List findCorrectAnswersByQuizId(int quizId);
    public String deleteQuestion(int id);
    public String deleteParticipant(int id);
    public List<Participant> findParticipantsByQuizId(int quizId);
    public List<Participant> findParticipantsByCourseId(int courseId);
    public List<Participant> findAllParticipants();
    public List<Participant> findParticipantsByUsername(String username);

}