package com.schoolonline2020.ozatapp.quiz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.schoolonline2020.ozatapp.quiz.dao.QuizDAO;
import com.schoolonline2020.ozatapp.quiz.entity.Participant;
import com.schoolonline2020.ozatapp.quiz.entity.Question;
import com.schoolonline2020.ozatapp.quiz.entity.Quiz;
import com.schoolonline2020.ozatapp.quiz.entity.Response;
import java.util.List;

@Service
public class QuizServiceImplementation implements QuizService {

    @Autowired
    private QuizDAO quizDAO;

    @Override
    @Transactional("quizTransactionManager")
    public Quiz findQuizByLessonId(int lessonId) {
        return quizDAO.findQuizByLessonId(lessonId);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public Quiz findQuizById(int id) {
        return quizDAO.findQuizById(id);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public List<Question> findQuestionsByQuizId(int quizId) {
        return quizDAO.findQuestionsByQuizId(quizId);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public String save(Quiz quiz) {
        return quizDAO.save(quiz);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public String saveQuestion(int quizId, Question question) {
        return quizDAO.saveQuestion(quizId, question);
    }

    @Override
    @Transactional("quizTransactionManager")
    public Participant findParticipantByQuizIdAndUsername(int quizId, String username) {
        return quizDAO.findParticipantByQuizIdAndUsername(quizId, username);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public String save(Participant participant) {
        return quizDAO.save(participant);
    }
    
   @Override
    @Transactional("quizTransactionManager")
    public List<Response> findResponsesByParticipantId(int participantId) {
        return quizDAO.findResponsesByParticipantId(participantId);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public Participant findParticipantById(int id) {
        return quizDAO.findParticipantById(id);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public Response findResponseByParticipantAndQuestionId(int participantId, int questionId) {
        return quizDAO.findResponseByParticipantAndQuestionId(participantId, questionId);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public Question findQuestionById(int id) {
        return quizDAO.findQuestionById(id);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public String saveResponse(int participantId, Response response) {
        return quizDAO.saveResponse(participantId, response);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public List<Question> findCorrectAnswersByQuizId(int quizId) {
        return quizDAO.findCorrectAnswersByQuizId(quizId);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public String deleteQuestion(int id) {
        return quizDAO.deleteQuestion(id);
    }

    @Override
    @Transactional("quizTransactionManager")
    public String deleteParticipant(int id) {
        return quizDAO.deleteParticipant(id);
    }

    @Override
    @Transactional("quizTransactionManager")
    public List<Participant> findParticipantsByQuizId(int quizId) {
        return quizDAO.findParticipantsByQuizId(quizId);
    }

    @Override
    @Transactional("quizTransactionManager")
    public List<Participant> findParticipantsByCourseId(int courseId) {
        return quizDAO.findParticipantsByCourseId(courseId);
    }
    
    @Override
    @Transactional("quizTransactionManager")
    public List<Participant> findAllParticipants() {
        return quizDAO.findAllParticipants();
    }

    @Override
    @Transactional("quizTransactionManager")
    public List<Participant> findParticipantsByUsername(String username) {
        return quizDAO.findParticipantsByUsername(username);
    }

}