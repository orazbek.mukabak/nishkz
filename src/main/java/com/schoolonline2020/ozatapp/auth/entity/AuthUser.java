package com.schoolonline2020.ozatapp.auth.entity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;

public class AuthUser {
    
    @Size(min=16, max=18,message="Phone number must be between 16 and 18 characters!")
    private String username;

    @NotNull(message="Password cannot be empty!")
    @Size(min=6, max=32,message="Password must be between 6 and 32 characters!")
    private String password;

    public AuthUser() {
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    
}