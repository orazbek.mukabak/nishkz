package com.schoolonline2020.ozatapp.auth.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired
    private DataSource authDataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(authDataSource);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/course/add-course")
                .antMatchers("/course/edit-course")
                .antMatchers("/course/add-lesson")
                .antMatchers("/course/edit-lesson")
                .antMatchers("/course/add-video")
                .antMatchers("/quiz/add-question")
                .antMatchers("/quiz/edit-question")
                .antMatchers("/quiz/edit-quiz")
                .antMatchers("/premiumexam/add-results")
                .antMatchers("/premiumexam/add-variant")
                .antMatchers("/premiumexam/add-pquestion")
                .antMatchers("/forum/add-topic")
                .antMatchers("/forum/add-reply")
                .antMatchers("/register/signing")
                .antMatchers("/register/recoverying");
                    //.antMatchers("/authenticate");
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.requiresChannel().anyRequest().requiresSecure();
        http.authorizeRequests()
//            .antMatchers("/profile/view-all*").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/profile/save*").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/profile/delete").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/profile/**").hasRole("EMPLOYEE")
            //.antMatchers("/product/**").hasRole("EMPLOYEE")
//            .antMatchers("/shoppingcart/**").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/product/view").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/lesson/add").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/product/save").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/product/edit").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/product/add-picture").hasAnyRole("MANAGER", "ADMIN")
//            .antMatchers("/product/save-picture").hasAnyRole("MANAGER", "ADMIN")
            .antMatchers("/profile/r").hasAnyRole("MANAGER", "ADMIN", "STUDENT")
            .antMatchers("/course/**").permitAll()
            .antMatchers("/quiz/**").permitAll()
            .antMatchers("/resources/**").permitAll()
        .and()
        .formLogin()
        .loginPage("/signin?u=null&p=null&error=")
        .loginProcessingUrl("/authenticate")
        .defaultSuccessUrl("/course/view-all")
        //.failureUrl("/exception404")
        .permitAll()
        .and()
        .logout()
            .permitAll()
        .and()
        .exceptionHandling()
            .accessDeniedPage("/exception404");
    }
    
    @Bean(name="userDetailsManager")
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(authDataSource);
        return jdbcUserDetailsManager; 
    }
}