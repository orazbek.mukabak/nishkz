package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:premiumexam-persistence-mysql.properties"})
public class ConfigPremiumExam implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="premiumexamDataSource")
    public DataSource premiumexamDataSource() {
        ComboPooledDataSource premiumexamDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("premiumexam.localhost.hostname");
        String localHostName2 = env.getProperty("premiumexam.localhost.hostname2");
        try {
            premiumexamDataSource.setDriverClass(env.getProperty("premiumexam.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigPremiumExam.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            premiumexamDataSource.setJdbcUrl(env.getProperty("premiumexam.jdbc.url"));
        } else {
            premiumexamDataSource.setJdbcUrl(env.getProperty("premiumexam.google.cloud.jdbc.url"));
        }
        premiumexamDataSource.setUser(env.getProperty("premiumexam.jdbc.user"));
        premiumexamDataSource.setPassword(env.getProperty("premiumexam.jdbc.password"));
        premiumexamDataSource.setInitialPoolSize(getIntProperty("premiumexam.connection.pool.initialPoolSize"));
        premiumexamDataSource.setMinPoolSize(getIntProperty("premiumexam.connection.pool.minPoolSize"));
        premiumexamDataSource.setMaxPoolSize(getIntProperty("premiumexam.connection.pool.maxPoolSize"));		
        premiumexamDataSource.setMaxIdleTime(getIntProperty("premiumexam.connection.pool.maxIdleTime"));
        return premiumexamDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("premiumexam.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("premiumexam.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="premiumexamSessionFactory")
    public LocalSessionFactoryBean premiumexamSessionFactory(){
        LocalSessionFactoryBean premiumexamSessionFactory = new LocalSessionFactoryBean();
        premiumexamSessionFactory.setDataSource(premiumexamDataSource());
        premiumexamSessionFactory.setPackagesToScan(env.getProperty("premiumexam.hibernate.packagesToScan"));
        premiumexamSessionFactory.setHibernateProperties(getHibernateProperties());
        return premiumexamSessionFactory;
    }

    @Bean(name="premiumexamTransactionManager")
    @Autowired
    public HibernateTransactionManager premiumexamTransactionManager(SessionFactory premiumexamSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(premiumexamSessionFactory);
        return txManager;
    }
}