package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:course-persistence-mysql.properties"})
public class ConfigCourse implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="courseDataSource")
    public DataSource courseDataSource() {
        ComboPooledDataSource courseDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("course.localhost.hostname");
        String localHostName2 = env.getProperty("course.localhost.hostname2");
        try {
            courseDataSource.setDriverClass(env.getProperty("course.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigCourse.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            courseDataSource.setJdbcUrl(env.getProperty("course.jdbc.url"));
        } else {
            courseDataSource.setJdbcUrl(env.getProperty("course.google.cloud.jdbc.url"));
        }
        courseDataSource.setUser(env.getProperty("course.jdbc.user"));
        courseDataSource.setPassword(env.getProperty("course.jdbc.password"));
        courseDataSource.setInitialPoolSize(getIntProperty("course.connection.pool.initialPoolSize"));
        courseDataSource.setMinPoolSize(getIntProperty("course.connection.pool.minPoolSize"));
        courseDataSource.setMaxPoolSize(getIntProperty("course.connection.pool.maxPoolSize"));		
        courseDataSource.setMaxIdleTime(getIntProperty("course.connection.pool.maxIdleTime"));
        return courseDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("course.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("course.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="courseSessionFactory")
    public LocalSessionFactoryBean courseSessionFactory(){
        LocalSessionFactoryBean courseSessionFactory = new LocalSessionFactoryBean();
        courseSessionFactory.setDataSource(courseDataSource());
        courseSessionFactory.setPackagesToScan(env.getProperty("course.hibernate.packagesToScan"));
        courseSessionFactory.setHibernateProperties(getHibernateProperties());
        return courseSessionFactory;
    }

    @Bean(name="courseTransactionManager")
    @Autowired
    public HibernateTransactionManager courseTransactionManager(SessionFactory courseSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(courseSessionFactory);
        return txManager;
    }
}