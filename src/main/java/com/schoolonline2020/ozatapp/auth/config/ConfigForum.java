package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:forum-persistence-mysql.properties"})
public class ConfigForum implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="forumDataSource")
    public DataSource forumDataSource() {
        ComboPooledDataSource forumDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("forum.localhost.hostname");
        String localHostName2 = env.getProperty("forum.localhost.hostname2");
        try {
            forumDataSource.setDriverClass(env.getProperty("forum.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigForum.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            forumDataSource.setJdbcUrl(env.getProperty("forum.jdbc.url"));
        } else {
            forumDataSource.setJdbcUrl(env.getProperty("forum.google.cloud.jdbc.url"));
        }
        forumDataSource.setUser(env.getProperty("forum.jdbc.user"));
        forumDataSource.setPassword(env.getProperty("forum.jdbc.password"));
        forumDataSource.setInitialPoolSize(getIntProperty("forum.connection.pool.initialPoolSize"));
        forumDataSource.setMinPoolSize(getIntProperty("forum.connection.pool.minPoolSize"));
        forumDataSource.setMaxPoolSize(getIntProperty("forum.connection.pool.maxPoolSize"));		
        forumDataSource.setMaxIdleTime(getIntProperty("forum.connection.pool.maxIdleTime"));
        return forumDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("forum.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("forum.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="forumSessionFactory")
    public LocalSessionFactoryBean forumSessionFactory(){
        LocalSessionFactoryBean forumSessionFactory = new LocalSessionFactoryBean();
        forumSessionFactory.setDataSource(forumDataSource());
        forumSessionFactory.setPackagesToScan(env.getProperty("forum.hibernate.packagesToScan"));
        forumSessionFactory.setHibernateProperties(getHibernateProperties());
        return forumSessionFactory;
    }

    @Bean(name="forumTransactionManager")
    @Autowired
    public HibernateTransactionManager forumTransactionManager(SessionFactory forumSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(forumSessionFactory);
        return txManager;
    }
}