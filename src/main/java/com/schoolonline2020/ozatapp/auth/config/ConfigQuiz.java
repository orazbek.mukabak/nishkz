package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:quiz-persistence-mysql.properties"})
public class ConfigQuiz implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="quizDataSource")
    public DataSource quizDataSource() {
        ComboPooledDataSource quizDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("quiz.localhost.hostname");
        String localHostName2 = env.getProperty("quiz.localhost.hostname2");
        try {
            quizDataSource.setDriverClass(env.getProperty("quiz.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigQuiz.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            quizDataSource.setJdbcUrl(env.getProperty("quiz.jdbc.url"));
        } else {
            quizDataSource.setJdbcUrl(env.getProperty("quiz.google.cloud.jdbc.url"));
        }
        quizDataSource.setUser(env.getProperty("quiz.jdbc.user"));
        quizDataSource.setPassword(env.getProperty("quiz.jdbc.password"));
        quizDataSource.setInitialPoolSize(getIntProperty("quiz.connection.pool.initialPoolSize"));
        quizDataSource.setMinPoolSize(getIntProperty("quiz.connection.pool.minPoolSize"));
        quizDataSource.setMaxPoolSize(getIntProperty("quiz.connection.pool.maxPoolSize"));		
        quizDataSource.setMaxIdleTime(getIntProperty("quiz.connection.pool.maxIdleTime"));
        return quizDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("quiz.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("quiz.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="quizSessionFactory")
    public LocalSessionFactoryBean quizSessionFactory(){
        LocalSessionFactoryBean quizSessionFactory = new LocalSessionFactoryBean();
        quizSessionFactory.setDataSource(quizDataSource());
        quizSessionFactory.setPackagesToScan(env.getProperty("quiz.hibernate.packagesToScan"));
        quizSessionFactory.setHibernateProperties(getHibernateProperties());
        return quizSessionFactory;
    }

    @Bean(name="quizTransactionManager")
    @Autowired
    public HibernateTransactionManager quizTransactionManager(SessionFactory quizSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(quizSessionFactory);
        return txManager;
    }
}