package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:auth-persistence-mysql.properties"})
public class ConfigAuth implements WebMvcConfigurer {
    @Autowired
    private Environment env;
    
    @Bean(name="viewResolver")
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/view/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean(name="authDataSource")
    @Primary
    public DataSource authDataSource() {
        ComboPooledDataSource authDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("auth.localhost.hostname");
        String localHostName2 = env.getProperty("auth.localhost.hostname2");
        try {
            authDataSource.setDriverClass(env.getProperty("auth.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigAuth.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            authDataSource.setJdbcUrl(env.getProperty("auth.jdbc.url"));
        } else {
            authDataSource.setJdbcUrl(env.getProperty("auth.google.cloud.jdbc.url"));
        }
        authDataSource.setUser(env.getProperty("auth.jdbc.user"));
        authDataSource.setPassword(env.getProperty("auth.jdbc.password"));
        authDataSource.setInitialPoolSize(getIntProperty("auth.connection.pool.initialPoolSize"));
        authDataSource.setMinPoolSize(getIntProperty("auth.connection.pool.minPoolSize"));
        authDataSource.setMaxPoolSize(getIntProperty("auth.connection.pool.maxPoolSize"));
        authDataSource.setMaxIdleTime(getIntProperty("auth.connection.pool.maxIdleTime"));
        return authDataSource;
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
            .addResourceHandler("/resources/**")
            .addResourceLocations("/resources/"); 
    }
}