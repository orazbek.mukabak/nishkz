package com.schoolonline2020.ozatapp.auth.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class SpringMvcDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {
            ConfigAuth.class,
            ConfigProfile.class,
            ConfigCourse.class,
            ConfigQuiz.class,
            ConfigSms.class,
            ConfigPremiumExam.class,
            ConfigPayment.class,
            ConfigForum.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

}