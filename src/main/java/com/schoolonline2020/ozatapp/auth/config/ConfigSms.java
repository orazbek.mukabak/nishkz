package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:sms-persistence-mysql.properties"})
public class ConfigSms implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="smsDataSource")
    public DataSource smsDataSource() {
        ComboPooledDataSource smsDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("sms.localhost.hostname");
        String localHostName2 = env.getProperty("sms.localhost.hostname2");
        try {
            smsDataSource.setDriverClass(env.getProperty("sms.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigSms.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            smsDataSource.setJdbcUrl(env.getProperty("sms.jdbc.url"));
        } else {
            smsDataSource.setJdbcUrl(env.getProperty("sms.google.cloud.jdbc.url"));
        }
        smsDataSource.setUser(env.getProperty("sms.jdbc.user"));
        smsDataSource.setPassword(env.getProperty("sms.jdbc.password"));
        smsDataSource.setInitialPoolSize(getIntProperty("sms.connection.pool.initialPoolSize"));
        smsDataSource.setMinPoolSize(getIntProperty("sms.connection.pool.minPoolSize"));
        smsDataSource.setMaxPoolSize(getIntProperty("sms.connection.pool.maxPoolSize"));		
        smsDataSource.setMaxIdleTime(getIntProperty("sms.connection.pool.maxIdleTime"));
        return smsDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("sms.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("sms.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="smsSessionFactory")
    public LocalSessionFactoryBean smsSessionFactory(){
        LocalSessionFactoryBean smsSessionFactory = new LocalSessionFactoryBean();
        smsSessionFactory.setDataSource(smsDataSource());
        smsSessionFactory.setPackagesToScan(env.getProperty("sms.hibernate.packagesToScan"));
        smsSessionFactory.setHibernateProperties(getHibernateProperties());
        return smsSessionFactory;
    }

    @Bean(name="smsTransactionManager")
    @Autowired
    public HibernateTransactionManager smsTransactionManager(SessionFactory smsSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(smsSessionFactory);
        return txManager;
    }
}