package com.schoolonline2020.ozatapp.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.schoolonline2020.ozatapp")
@PropertySource({"classpath:profile-persistence-mysql.properties"})
public class ConfigProfile implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="profileDataSource")
    public DataSource profileDataSource() {
        ComboPooledDataSource profileDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("profile.localhost.hostname");
        String localHostName2 = env.getProperty("profile.localhost.hostname2");
        try {
            profileDataSource.setDriverClass(env.getProperty("profile.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigProfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            profileDataSource.setJdbcUrl(env.getProperty("profile.jdbc.url"));
        } else {
            profileDataSource.setJdbcUrl(env.getProperty("profile.google.cloud.jdbc.url"));
        }
        profileDataSource.setUser(env.getProperty("profile.jdbc.user"));
        profileDataSource.setPassword(env.getProperty("profile.jdbc.password"));
        profileDataSource.setInitialPoolSize(getIntProperty("profile.connection.pool.initialPoolSize"));
        profileDataSource.setMinPoolSize(getIntProperty("profile.connection.pool.minPoolSize"));
        profileDataSource.setMaxPoolSize(getIntProperty("profile.connection.pool.maxPoolSize"));		
        profileDataSource.setMaxIdleTime(getIntProperty("profile.connection.pool.maxIdleTime"));
        return profileDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("profile.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("profile.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="profileSessionFactory")
    public LocalSessionFactoryBean profileSessionFactory(){
        LocalSessionFactoryBean profileSessionFactory = new LocalSessionFactoryBean();
        profileSessionFactory.setDataSource(profileDataSource());
        profileSessionFactory.setPackagesToScan(env.getProperty("profile.hibernate.packagesToScan"));
        profileSessionFactory.setHibernateProperties(getHibernateProperties());
        return profileSessionFactory;
    }

    @Bean(name="profileTransactionManager")
    @Autowired
    public HibernateTransactionManager profileTransactionManager(SessionFactory profileSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(profileSessionFactory);
        return txManager;
    }
}