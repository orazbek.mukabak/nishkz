package com.schoolonline2020.ozatapp.payment.dao;

import com.schoolonline2020.ozatapp.payment.entity.Account;
import com.schoolonline2020.ozatapp.payment.entity.Purchase;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentDAOImplementation implements PaymentDAO {

    @Autowired
    private SessionFactory paymentSessionFactory;

    @Override
    public Purchase findPurchaseByCourseIdAndUsername(int courseId, String username) {
        Session currentSession = paymentSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Purchase.class);
        String product = "C" + courseId;
        query.add(Restrictions.eq("product", product));
        query.add(Restrictions.eq("username", username));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("product"), "product")
        .add(Projections.property("username"), "username")
        .add(Projections.property("purchasedDate"), "purchasedDate"))
        .setResultTransformer(Transformers.aliasToBean(Purchase.class));
        return (Purchase)query.uniqueResult();
    }

    @Override
    public String save(Account account) {
        Session currentSession = paymentSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(account);
        return "Done! Account is saved!";
    }

    @Override
    public Account findAccountByInnerId(int innerId) {
        Session currentSession = paymentSessionFactory.getCurrentSession();
        return currentSession.get(Account.class, innerId);
    }

    @Override
    public String save(Purchase purchase) {
        Session currentSession = paymentSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(purchase);
        return "Done! Purchase is saved!";
    }

    @Override
    public List<Purchase> findPurchasesByCourseId(int courseId) {
        Session currentSession = paymentSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Purchase.class);
        query.add(Restrictions.eq("product", "C" + courseId));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("product"), "product")
        .add(Projections.property("username"), "username")
        .add(Projections.property("purchasedDate"), "purchasedDate"))
        .setResultTransformer(Transformers.aliasToBean(Purchase.class));
        return query.list();
    }

    @Override
    public List<Purchase> findPurchasesByUsername(String username) {
        Session currentSession = paymentSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Purchase.class);
        query.add(Restrictions.eq("username", username));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("product"), "product")
        .add(Projections.property("username"), "username")
        .add(Projections.property("purchasedDate"), "purchasedDate"))
        .setResultTransformer(Transformers.aliasToBean(Purchase.class));
        return query.list();
    }

}