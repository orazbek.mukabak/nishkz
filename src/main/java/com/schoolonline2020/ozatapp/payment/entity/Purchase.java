package com.schoolonline2020.ozatapp.payment.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="purchase")
public class Purchase implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="product")
    private String product;
    @Column(name="username")
    private String username;
    @Column(name="purchased_date")
    private Date purchasedDate;
    @Transient
    protected Object[] jdoDetachedState;

    public Purchase() {
    }

    public Purchase(String product, String username, Date purchasedDate) {
        this.product = product;
        this.username = username;
        this.purchasedDate = purchasedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
    
}