package com.schoolonline2020.ozatapp.payment.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="account")
public class Account implements Serializable {
    @Id
    @Column(name="inner_id")
    private int innerId;
    @Column(name="balance")
    private double balance;

    public Account() {
    }

    public Account(int innerId, double balance) {
        this.innerId = innerId;
        this.balance = balance;
    }

    public int getInnerId() {
        return innerId;
    }

    public void setInnerId(int innerId) {
        this.innerId = innerId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}