package com.schoolonline2020.ozatapp.payment.service;

import com.schoolonline2020.ozatapp.payment.entity.Account;
import com.schoolonline2020.ozatapp.payment.entity.Purchase;
import java.util.List;

public interface PaymentService {

    public Purchase findPurchaseByCourseIdAndUsername(int courseId, String username);
    public String save(Account account);
    public Account findAccountByInnerId(int innerId);
    public String save(Purchase purchase);
    public List<Purchase> findPurchasesByCourseId(int courseId);
    public List<Purchase> findPurchasesByUsername(String username);

}