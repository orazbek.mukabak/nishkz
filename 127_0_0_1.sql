-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2020 at 02:44 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nishkz-db_auth`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_auth` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_auth`;

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE `authorities` (
  `username` varchar(255) NOT NULL COMMENT 'Contact number',
  `authority` varchar(64) NOT NULL COMMENT 'Assigned role'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The list of user roles';

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`username`, `authority`) VALUES
('8 (222) 222-22-22', 'STUDENT'),
('8 (775) 333-33-33', 'STUDENT'),
('8 (775) 976-08-93', 'MANAGER'),
('8 (775) 976-08-93', 'STUDENT'),
('8 (775) 976-08-93', 'TUTOR'),
('8 (777) 777-77-77', 'STUDENT');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(255) NOT NULL COMMENT 'Contact number',
  `password` char(68) NOT NULL COMMENT 'Encrypted password',
  `enabled` tinyint(4) NOT NULL COMMENT 'Status: (0) inactive (1) active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The list of users and their password';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
('8 (222) 222-22-22', '{bcrypt}$2a$10$4XX7VR2q2bd48PAgoP4DbO7FY/94JWn3xHKs0uFVVVrs6LcsBZ1RC', 1),
('8 (775) 333-33-33', '{bcrypt}$2a$10$QrLfBuc/IrnENdYEjFi2VOzXFJtx9.DbaRgacHGF0IfEkZnxIxUqO', 1),
('8 (775) 976-08-93', '{bcrypt}$2a$10$Bqe5k6cFFpTu6cAGyxulZur/FBYowJdi9UmgB9NuTgF3wm4yzx4Sy', 1),
('8 (777) 777-77-77', '{bcrypt}$2a$10$vHeSLxQ7lWMY4WJnCTaR9Ov0PrmWLdjzEIf2WoBp.MGmKBuvipW7m', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
  ADD UNIQUE KEY `CK_AUTHORITIES` (`username`,`authority`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `authorities`
--
ALTER TABLE `authorities`
  ADD CONSTRAINT `CONST_USERNAME_AUTHORITIES` FOREIGN KEY (`username`) REFERENCES `users` (`username`);
--
-- Database: `nishkz-db_course`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_course` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_course`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Category title',
  `priority` double DEFAULT NULL COMMENT 'Category priority'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `priority`) VALUES
(1, 'Қазақ тіліндегі курстар', 1),
(2, 'Курсы на русском языке', 2);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL COMMENT 'Auto generated course id',
  `category_id` int(11) DEFAULT NULL COMMENT 'Course category ID',
  `description` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Course description',
  `grade` int(1) DEFAULT NULL COMMENT 'Grade',
  `subject` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Subject name',
  `language` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Course language',
  `price` int(11) DEFAULT '0' COMMENT 'Course price',
  `discount_title` varchar(128) CHARACTER SET utf8 DEFAULT '' COMMENT 'Discount title 128 characters',
  `discount_value` int(11) NOT NULL DEFAULT '0' COMMENT 'Discount value',
  `video_id` varchar(255) DEFAULT NULL COMMENT 'Video ID',
  `start_date_time` datetime DEFAULT NULL COMMENT 'Start date time',
  `feature1` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 1',
  `feature2` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 2',
  `feature3` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 3',
  `feature4` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 4',
  `feature5` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 5',
  `feature6` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 6',
  `feature7` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 7',
  `feature8` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 8',
  `feature9` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 9',
  `feature10` varchar(255) CHARACTER SET utf8 DEFAULT '' COMMENT 'Feature 10',
  `priority` double DEFAULT '1' COMMENT 'Priority',
  `username` varchar(255) DEFAULT NULL COMMENT 'Username',
  `status` char(1) CHARACTER SET utf8 DEFAULT 'a' COMMENT 'The status: (a) active, (c) created'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `category_id`, `description`, `grade`, `subject`, `language`, `price`, `discount_title`, `discount_value`, `video_id`, `start_date_time`, `feature1`, `feature2`, `feature3`, `feature4`, `feature5`, `feature6`, `feature7`, `feature8`, `feature9`, `feature10`, `priority`, `username`, `status`) VALUES
(1, 1, 'Осы бөлімде 5 сыныпта оқыған тақырыптарды бір қайталап өтеміз және тесттер орындаймыз.', 6, 'НЗМ-дайындық. I-Бөлім', 'Қазақша', 12000, 'Discount title here', 25, '453730731', '2020-09-14 09:00:00', '4 айлық бағдарламаның 1-бөлімі', 'Әр тақырып бойынша видеосабақтар', 'Әр мысалға жеке видеосабақ', 'Тікелей эфирлер', 'Өздік жұмыстар', 'Тест түріндегі үй жұмыстары', 'Ай соңында ең үздік оқушыға келесі ай тегін оқуға мүмкіндік', '', '', '', 1, '8 (775) 976-08-93', 'c'),
(2, 1, 'Осы бөлімде 6-сыныптың 1 тоқсанында болатын сабақтар және НИШ-қа қатысты тесттер қамтылған.', 6, 'НЗМ-дайындық. II-Бөлім', 'Қазақша', 12000, 'Discount title here', 25, '453730731', '2020-06-01 09:00:00', '4 айлық бағдарламаның 2-бөлімі', 'Әр тақырып бойынша видеосабақтар', 'Әр мысалға жеке видеосабақ', 'Тікелей эфирлер', 'Өздік жұмыстар', 'Тест түріндегі үй жұмыстары', 'Ай соңында ең үздік оқушыға келесі ай тегін оқуға мүмкіндік', '', '', '', 2, '8 (775) 976-08-93', 'c'),
(3, 1, 'Осы бөлімде 6 сыныптың 2-тоқсанында келетін тақырыптар және НИШ-қа тесттер орындалады', 6, 'НЗМ-дайындық. III-Бөлім', 'Қазақша', 12000, 'Discount title here', 25, '453730731', '2020-06-01 09:00:00', '4 айлық бағдарламаның 3-бөлімі', 'Әр тақырып бойынша видеосабақтар', 'Әр мысалға жеке видеосабақ', 'Тікелей эфирлер', 'Өздік жұмыстар', 'Тест түріндегі үй жұмыстары', 'Ай соңында ең үздік оқушыға келесі ай тегін оқуға мүмкіндік', '', '', '', 3, '8 (707) 141-73-88', 'c'),
(4, 1, 'Осы бөлімде негізінен НИШ-қа арналған тесттерге басымдылық беріледі.', 6, 'НЗМ-дайындық. IV-Бөлім', 'Қазақша', 12000, 'Discount title here', 25, '453730731', '2020-06-01 09:00:00', '4 айлық бағдарламаның 4-бөлімі', 'Әр тақырып бойынша видеосабақтар', 'Әр мысалға жеке видеосабақ', 'Тікелей эфирлер', 'Өздік жұмыстар', 'Тест түріндегі үй жұмыстары', 'Ай соңында ең үздік оқушыға келесі ай тегін оқуға мүмкіндік', '', '', '', 4, '8 (707) 141-73-88', 'c'),
(5, 2, 'В этом разделе мы рассмотрим темы, изучаемые в 5-м классе, и пройдем тесты.', 6, 'Подготовка в НИШ. Часть 1', 'Русский', 12000, 'Discount title here', 25, '453731096', '2020-09-15 09:00:00', 'Часть 1 в четырех месячной программы', 'Видео уроки по каждой теме', 'Индивидуальный видеоурок для каждого примера', 'Прямые трансляции', 'Независимая работа', 'Домашнее задание в виде теста', 'В конце месяца лучший студент имеет возможность учиться бесплатно в следующем месяце.', '', '', '', 1, '8 (775) 976-08-93', 'c'),
(6, 2, 'В этом разделе мы рассмотрим темы, изучаемые в 5-м классе, и пройдем тесты.', 6, 'Подготовка к НИШ. Часть 2', 'Русский', 12000, 'Discount title here', 25, '453731096', '2020-10-15 09:00:00', 'Часть 2 в четырех месячной программы', 'Видео уроки по каждой теме', 'Индивидуальный видеоурок для каждого примера', 'Прямые трансляции', 'Независимая работа', 'Домашнее задание в виде теста', 'В конце месяца лучший студент имеет возможность учиться бесплатно в следующем месяце.', '', '', '', 2, '8 (775) 976-08-93', 'c'),
(7, 2, 'В этом разделе мы рассмотрим темы, изучаемые в 5-м классе, и пройдем тесты.', 6, 'Подготовка к НИШ. Часть 2', 'Русский', 12000, 'Discount title here', 25, '453731096', '2020-11-15 09:00:00', 'Часть 3 в четырех месячной программы', 'Видео уроки по каждой теме', 'Индивидуальный видеоурок для каждого примера', 'Прямые трансляции', 'Независимая работа', 'Домашнее задание в виде теста', 'В конце месяца лучший студент имеет возможность учиться бесплатно в следующем месяце.', '', '', '', 3, '8 (775) 976-08-93', 'c'),
(8, 2, 'В этом разделе мы рассмотрим темы, изучаемые в 5-м классе, и пройдем тесты.', 6, 'Подготовка к НИШ. Часть 4', 'Русский', 12000, 'Discount title here', 25, '453731096', '2020-12-15 09:00:00', 'Часть 4 в четырех месячной программы', 'Видео уроки по каждой теме', 'Индивидуальный видеоурок для каждого примера', 'Прямые трансляции', 'Независимая работа', 'Домашнее задание в виде теста', 'В конце месяца лучший студент имеет возможность учиться бесплатно в следующем месяце.', '', '', '', 4, '8 (775) 976-08-93', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

CREATE TABLE `lesson` (
  `id` int(11) NOT NULL COMMENT 'Auto generated lesson id',
  `title` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Lesson''s title',
  `live_url` varchar(128) DEFAULT NULL COMMENT 'Youtube Live URL',
  `summary` longtext CHARACTER SET utf8 COMMENT 'Summary 2048 symbols',
  `priority` double DEFAULT NULL COMMENT 'Priority',
  `start_date_time` datetime DEFAULT NULL COMMENT 'Starting date time',
  `reg_date` date DEFAULT NULL COMMENT 'Recorded date',
  `quiz_allowed` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - not allowed, 1 - allowed',
  `status` char(1) DEFAULT 'c' COMMENT 'c-created, a-active',
  `course_id` int(11) DEFAULT NULL COMMENT 'Course id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`id`, `title`, `live_url`, `summary`, `priority`, `start_date_time`, `reg_date`, `quiz_allowed`, `status`, `course_id`) VALUES
(8, '1 - Сабақ', NULL, '%3Cp%3E%0A%3C/p%3E%0A%3Cdiv%20style=%22text-align:%20center;%22%3E%0A%09%3Ciframe%20style=%22width:%20560px;%20height:%20315px;%22%20allowfullscreen=%22%22%20allow=%22accelerometer;%20autoplay;%20encrypted-media;%20gyroscope;%20picture-in-picture%22%20frameborder=%220%22%20src=%22https://www.youtube.com/embed/7qhvFP0HUKM%22%3E%3C/iframe%3E%20%0A%3C/div%3E%0A%3Cdiv%20style=%22text-align:%20center;%22%3E%0A%09%3Ciframe%20style=%22width:%20560px;%20height:%20315px;%22%20allowfullscreen=%22%22%20allow=%22accelerometer;%20autoplay;%20encrypted-media;%20gyroscope;%20picture-in-picture%22%20frameborder=%220%22%20src=%22https://www.youtube.com/live_chat?v=7qhvFP0HUKM&embed_domain=http://localhost:8080/nishkz/%22%3E%3C/iframe%3E%0A%3C/div%3E', 1, '2020-09-15 09:00:00', '2020-08-30', 1, 's', 1),
(9, 'Test lesson', NULL, 'This is a special test lesson', 0, '2020-08-30 17:04:33', '2020-08-30', 1, 's', 2),
(10, 'Test lesson', NULL, 'This is a special test lesson', 0, '2020-08-30 17:04:50', '2020-08-30', 1, 's', 4),
(11, 'Test lesson', NULL, 'This is a special test lesson', 0, '2020-08-31 15:49:04', '2020-08-31', 1, 's', 3),
(12, 'Test lesson', NULL, 'This is a special test lesson', 0, '2020-09-03 12:28:37', '2020-09-03', 1, 's', 5),
(13, 'Test lesson', NULL, 'This is a special test lesson', 0, '2020-09-06 10:53:12', '2020-09-06', 1, 's', 8),
(14, 'Test lesson', NULL, 'This is a special test lesson', 0, '2020-09-06 11:06:19', '2020-09-06', 1, 's', 7);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL COMMENT 'id',
  `video_id` int(11) DEFAULT NULL COMMENT 'Video id',
  `title` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Video title in 128 symbols',
  `url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Video url',
  `thumbnail_url` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Thumbnail url',
  `duration` int(11) DEFAULT NULL COMMENT 'Duration in seconds',
  `priority` double DEFAULT NULL COMMENT 'Video priority',
  `lesson_id` int(11) DEFAULT NULL COMMENT 'Lesson id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_id`, `title`, `url`, `thumbnail_url`, `duration`, `priority`, `lesson_id`) VALUES
(1, 449353327, 'Кіріспе', 'https://vimeo.com/449353327/e491f9736a', 'https://i.vimeocdn.com/video/942909140_295x166.jpg', 26, 1, 8),
(2, 449353339, 'Сандардың 2 ге, 4 ке, 8 ге бөліну заңы', 'https://vimeo.com/449353339/66c2ec8098', 'https://i.vimeocdn.com/video/942909421_295x166.jpg', 101, 2, 8),
(3, 449353448, 'Сандардың 3 ке және 9 ға бөліну заңы', 'https://vimeo.com/449353448/2053d59dd4', 'https://i.vimeocdn.com/video/942909270_295x166.jpg', 171, 3, 8),
(4, 449353461, 'Ең үлкен ортақ бөлгіш (ЕҮОБ)', 'https://vimeo.com/449353461/c437ed460b', 'https://i.vimeocdn.com/video/942909408_295x166.jpg', 92, 4, 8),
(5, 449353539, 'Ең кіші ортақ еселік (ЕКОЕ)', 'https://vimeo.com/449353539/a22d8efec2', 'https://i.vimeocdn.com/video/942909520_295x166.jpg', 114, 5, 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_COURSE_ID_LESSON` (`course_id`) USING BTREE;

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_LESSON_ID_VIDEO` (`lesson_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto generated course id', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `lesson`
--
ALTER TABLE `lesson`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto generated lesson id', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lesson`
--
ALTER TABLE `lesson`
  ADD CONSTRAINT `CONST_COURSE_ID_LESSON` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `CONST_LESSON_ID_VIDEO` FOREIGN KEY (`lesson_id`) REFERENCES `lesson` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `nishkz-db_forum`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_forum` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_forum`;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `text` text CHARACTER SET utf8 COMMENT 'Forum text',
  `username` varchar(32) DEFAULT NULL COMMENT 'Username',
  `topic_id` int(11) DEFAULT NULL COMMENT 'Topic id',
  `created_date` datetime DEFAULT NULL COMMENT 'Created date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `text` text CHARACTER SET utf8 COMMENT 'Forum text',
  `username` varchar(32) DEFAULT NULL COMMENT 'Username',
  `lesson_id` int(11) DEFAULT NULL COMMENT 'Lesson id',
  `reply_counter` int(11) DEFAULT '0' COMMENT 'Reply counter',
  `created_date` datetime DEFAULT NULL COMMENT 'Created date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_TOPIC_ID_REPLY` (`topic_id`) USING BTREE;

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reply`
--
ALTER TABLE `reply`
  ADD CONSTRAINT `CONST_TOPIC_ID_REPLY` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `nishkz-db_payment`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_payment` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_payment`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `inner_id` int(11) NOT NULL COMMENT 'Account ID',
  `balance` double NOT NULL DEFAULT '0' COMMENT 'Balance'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `product` varchar(32) DEFAULT NULL COMMENT 'Product id',
  `username` varchar(32) DEFAULT NULL COMMENT 'Username',
  `purchased_date` date DEFAULT NULL COMMENT 'Purchased date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase`
--

INSERT INTO `purchase` (`id`, `product`, `username`, `purchased_date`) VALUES
(1, 'C1', '8 (775) 976-08-93', '2020-08-30'),
(2, 'C2', '8 (775) 976-08-93', '2020-08-30'),
(3, 'C5', '8 (775) 976-08-93', '2020-09-03'),
(4, 'D8', '8 (775) 976-08-93', '2020-09-06'),
(5, 'C7', '8 (775) 976-08-94', '2020-09-06'),
(6, 'C6', '8 (775) 976-08-93', '2020-09-16'),
(7, 'C7', '8 (775) 976-08-93', '2020-09-16'),
(8, 'C8', '8 (775) 976-08-93', '2020-09-16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`inner_id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=9;
--
-- Database: `nishkz-db_premium_exam`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_premium_exam` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_premium_exam`;

-- --------------------------------------------------------

--
-- Table structure for table `pexam`
--

CREATE TABLE `pexam` (
  `id` int(11) NOT NULL COMMENT 'id',
  `title` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Exam title',
  `description` text CHARACTER SET utf8 COMMENT 'Exam description',
  `priority` double DEFAULT NULL COMMENT 'Exam priority',
  `status` char(1) DEFAULT 'c' COMMENT 'Exam status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pparticipant`
--

CREATE TABLE `pparticipant` (
  `id` int(11) NOT NULL COMMENT 'id',
  `username` varchar(255) DEFAULT NULL COMMENT 'Contact number',
  `pquiz_id` int(11) DEFAULT NULL COMMENT 'Quiz id',
  `status` tinyint(4) DEFAULT NULL COMMENT '0 - not submitted, 1 - adding responses, 2 -  submitted	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pquestion`
--

CREATE TABLE `pquestion` (
  `id` int(11) NOT NULL COMMENT 'id',
  `priority` double DEFAULT NULL COMMENT 'Question priority',
  `text` text CHARACTER SET utf8 COMMENT 'Question text',
  `answer_a` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer A',
  `answer_b` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer B',
  `answer_c` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer C',
  `answer_d` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer D',
  `answer_e` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer E',
  `correct_answer` varchar(1) DEFAULT NULL COMMENT 'Correct answer',
  `level` smallint(6) DEFAULT NULL COMMENT 'Difficulties level',
  `solution` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Solution',
  `pquiz_id` int(11) DEFAULT NULL COMMENT 'Quiz id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pquiz`
--

CREATE TABLE `pquiz` (
  `id` int(11) NOT NULL COMMENT 'id',
  `duration` int(11) DEFAULT NULL COMMENT 'Premium quiz duration',
  `priority` double DEFAULT NULL COMMENT 'Premium quiz priority',
  `pexam_id` int(11) DEFAULT NULL COMMENT 'Premium exam id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `presponse`
--

CREATE TABLE `presponse` (
  `id` int(11) NOT NULL COMMENT 'id',
  `answer` varchar(1) DEFAULT NULL COMMENT 'Answer 1 char	',
  `pquestion_id` int(11) DEFAULT NULL COMMENT 'PQuestion id',
  `score` int(11) DEFAULT NULL COMMENT 'Score 1 or 0	',
  `pparticipant_id` int(11) DEFAULT NULL COMMENT 'PParticipant id	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pexam`
--
ALTER TABLE `pexam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pparticipant`
--
ALTER TABLE `pparticipant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pquestion`
--
ALTER TABLE `pquestion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_PQUIZ_ID_PQUESTION` (`pquiz_id`) USING BTREE;

--
-- Indexes for table `pquiz`
--
ALTER TABLE `pquiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_PEXAM_ID_PQUIZ` (`pexam_id`) USING BTREE;

--
-- Indexes for table `presponse`
--
ALTER TABLE `presponse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_PPARTICIPANT_ID_PRESPONSE` (`pparticipant_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pexam`
--
ALTER TABLE `pexam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- AUTO_INCREMENT for table `pparticipant`
--
ALTER TABLE `pparticipant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- AUTO_INCREMENT for table `pquestion`
--
ALTER TABLE `pquestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- AUTO_INCREMENT for table `pquiz`
--
ALTER TABLE `pquiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- AUTO_INCREMENT for table `presponse`
--
ALTER TABLE `presponse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pquestion`
--
ALTER TABLE `pquestion`
  ADD CONSTRAINT `CONST_PQUIZ_ID_PQUESTION` FOREIGN KEY (`pquiz_id`) REFERENCES `pquiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pquiz`
--
ALTER TABLE `pquiz`
  ADD CONSTRAINT `CONST_PEXAM_ID_PQUIZ` FOREIGN KEY (`pexam_id`) REFERENCES `pexam` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `presponse`
--
ALTER TABLE `presponse`
  ADD CONSTRAINT `CONST_PPARTICIPANT_ID_PRESPONSE` FOREIGN KEY (`pparticipant_id`) REFERENCES `pparticipant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `nishkz-db_profile`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_profile` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_profile`;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `username` varchar(255) NOT NULL COMMENT 'Contact number',
  `inner_id` int(11) NOT NULL COMMENT 'Used for inner identification',
  `first_name` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT 'First name',
  `last_name` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT 'Last name',
  `gender` tinyint(1) DEFAULT '1' COMMENT 'Gender: (1) male, (0) female',
  `email` varchar(255) DEFAULT '' COMMENT 'Email address',
  `about_me` text CHARACTER SET utf8 COMMENT 'Brief information about user, max 400 words!',
  `birth_date` date DEFAULT NULL COMMENT 'The user birth date',
  `reg_date` date DEFAULT NULL COMMENT 'The user registered date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`username`, `inner_id`, `first_name`, `last_name`, `gender`, `email`, `about_me`, `birth_date`, `reg_date`) VALUES
('8 (222) 222-22-22', 100004, '', '', 0, '', '', '2020-09-14', '2020-09-14'),
('8 (775) 333-33-33', 100003, '', '', 0, '', '', '2020-09-10', '2020-09-10'),
('8 (775) 976-08-93', 100000, 'Orazbek', 'Mukabak', 1, 'omukabak@gmail.com', '', '1993-03-13', '2020-08-30'),
('8 (777) 777-77-77', 100002, '', '', 0, '', '', '2020-08-30', '2020-08-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `INNER_UNIQUE_ID` (`inner_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `inner_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Used for inner identification', AUTO_INCREMENT=100005;
--
-- Database: `nishkz-db_quiz`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_quiz` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_quiz`;

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(11) NOT NULL COMMENT 'id',
  `username` varchar(255) DEFAULT NULL COMMENT 'Contact number',
  `inner_id` int(11) DEFAULT NULL COMMENT 'Inner ID',
  `first_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT 'First name',
  `last_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Last name',
  `quiz_id` int(11) DEFAULT NULL COMMENT 'Quiz id',
  `deadline_allowed` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Deadline: 0 - not allowed, 1 - allowed',
  `deadline_value` datetime DEFAULT NULL COMMENT 'Deadline value',
  `start_date_time` datetime DEFAULT NULL COMMENT 'Start date time',
  `end_date_time` datetime DEFAULT NULL COMMENT 'End date time',
  `course_id` int(11) DEFAULT NULL COMMENT 'Course ID',
  `lesson_id` int(11) DEFAULT NULL COMMENT 'Lesson ID',
  `total_question` int(11) DEFAULT '0' COMMENT 'Total number of questions',
  `total_score` double DEFAULT '0' COMMENT 'Total score',
  `correct_answers` int(11) DEFAULT '0' COMMENT 'Total number of correct answers',
  `status` tinyint(1) DEFAULT '0' COMMENT '0 - not submitted, 1 - submitted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `username`, `inner_id`, `first_name`, `last_name`, `quiz_id`, `deadline_allowed`, `deadline_value`, `start_date_time`, `end_date_time`, `course_id`, `lesson_id`, `total_question`, `total_score`, `correct_answers`, `status`) VALUES
(1, '8 (775) 976-08-93', 100000, 'Orazbek', 'Mukabak', 1, 0, NULL, NULL, NULL, 4, 10, 0, 0, 0, 0),
(2, '8 (775) 976-08-93', 100000, 'Orazbek', 'Mukabak', 2, 0, NULL, '2020-09-06 12:53:00', NULL, 1, 8, 0, 0, 0, 1),
(4, '8 (775) 976-08-93', 100000, 'Orazbek', 'Mukabak', 4, 0, NULL, NULL, NULL, 5, 12, 0, 0, 0, 0),
(5, '8 (775) 976-08-93', 100000, 'Orazbek', 'Mukabak', 5, 0, NULL, NULL, NULL, 8, 13, 0, 0, 0, 0),
(6, '8 (775) 976-08-93', 100000, 'Orazbek', 'Mukabak', 3, 0, NULL, '2020-09-06 14:20:14', '2020-09-06 14:20:17', 2, 9, 1, 100, 1, 2),
(7, '8 (222) 222-22-22', 100004, '', '', 3, 0, NULL, '2020-09-16 18:19:56', NULL, 2, 9, 1, 0, 0, 1),
(8, '8 (222) 222-22-22', 100004, '', '', 2, 0, NULL, NULL, NULL, 1, 8, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL COMMENT 'Question id',
  `level` smallint(6) DEFAULT NULL COMMENT 'Difficulty level',
  `text` text CHARACTER SET utf8 COMMENT 'Question text',
  `answer_a` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer A',
  `answer_b` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer B',
  `answer_c` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer C',
  `answer_d` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer D',
  `answer_e` text CHARACTER SET utf8 NOT NULL COMMENT 'Answer E',
  `correct_answer` varchar(1) DEFAULT NULL COMMENT 'Correct answer',
  `priority` int(11) NOT NULL DEFAULT '1' COMMENT 'Question priority',
  `solution_title` int(11) NOT NULL DEFAULT '0' COMMENT 'Solution title',
  `solution` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT 'Solution',
  `quiz_id` int(11) DEFAULT NULL COMMENT 'Quiz id FK'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `level`, `text`, `answer_a`, `answer_b`, `answer_c`, `answer_d`, `answer_e`, `correct_answer`, `priority`, `solution_title`, `solution`, `quiz_id`) VALUES
(1, 0, '1 + 1 = ?', '1', '2', '3', '4', '5', 'b', 1, 0, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL COMMENT 'Quiz id',
  `duration` decimal(10,0) DEFAULT NULL COMMENT 'Duration in seconds',
  `description` text CHARACTER SET utf8 COMMENT 'Quiz description',
  `deadline_allowed` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - not allowed, 1 - allowed',
  `deadline_value` datetime DEFAULT NULL COMMENT 'Deadline value',
  `lesson_id` int(11) NOT NULL COMMENT 'Appropriate lesson id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `duration`, `description`, `deadline_allowed`, `deadline_value`, `lesson_id`) VALUES
(1, '60', '', 0, NULL, 10),
(2, '60', '', 0, NULL, 8),
(3, '60', '', 0, NULL, 9),
(4, '60', '', 0, NULL, 12),
(5, '60', '', 0, NULL, 13);

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE `response` (
  `id` int(11) NOT NULL COMMENT 'Response id',
  `answer` varchar(1) DEFAULT NULL COMMENT 'Answer 1 char',
  `question_id` int(11) DEFAULT NULL COMMENT 'Question id',
  `score` int(11) DEFAULT NULL COMMENT 'Score 1 or 0',
  `participant_id` int(11) DEFAULT NULL COMMENT 'Participant id'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `response`
--

INSERT INTO `response` (`id`, `answer`, `question_id`, `score`, `participant_id`) VALUES
(2, 'b', 1, 1, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_QUIZ_ID_QUESTION` (`quiz_id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `question_id` (`question_id`,`participant_id`),
  ADD KEY `FK_PARTICIPANT_ID_RESPONSE` (`participant_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Question id', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Quiz id', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Response id', AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `CONST_QUIZ_ID_QUESTION` FOREIGN KEY (`quiz_id`) REFERENCES `quiz` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `response`
--
ALTER TABLE `response`
  ADD CONSTRAINT `CONST_PARTICIPANT_ID_RESPONSE` FOREIGN KEY (`participant_id`) REFERENCES `participant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `nishkz-db_sms`
--
CREATE DATABASE IF NOT EXISTS `nishkz-db_sms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `nishkz-db_sms`;

-- --------------------------------------------------------

--
-- Table structure for table `password_recovery`
--

CREATE TABLE `password_recovery` (
  `username` varchar(255) NOT NULL COMMENT 'Username',
  `code` int(4) NOT NULL COMMENT '4 digits confirmation code'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_recovery`
--

INSERT INTO `password_recovery` (`username`, `code`) VALUES
('8 (775) 976-08-93', 4344);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `password_recovery`
--
ALTER TABLE `password_recovery`
  ADD PRIMARY KEY (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
